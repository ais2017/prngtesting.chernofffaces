#include "QTGUI.h"

void WorkDBSave(IGateWay * dbsql, BO_ResultsExperiment * RE1, BO_ResultsExperiment * RE2, BO_SequenceParameters * SeqPar)
{
	dbsql->SaveData(RE1, RE2, SeqPar);
}

void WorkDBLoad(IGateWay * dbsql, BO_ResultsExperiment * RE1, BO_ResultsExperiment * RE2, BO_SequenceParameters * SeqPar)
{
	dbsql->LoadData(RE1, RE2, SeqPar);
}

void WorkDBLoadSeqPar(IGateWay * dbsql, BO_SequenceParameters * SeqPar)
{
	dbsql->LoadSeqParameters(SeqPar);
}



void QTGUI::mainSignal(QString str)
{
}

Widget::Widget(QWidget *parent) : QWidget(parent)
{

}

void Widget::paintfaces()
{
	QPixmap pixmap(this->size());
	this->render(&pixmap);
	pixmap.save("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\Faces.png", 0, -1);
}

void Widget::paintEvent(QPaintEvent * event)
{
	
	//====================================��������� �� �� ��������� ������������������=================
	/*
	BC_Stub * stub = new BC_Stub;// ���������� ��������
	
	stub->LoadData(&SeqPar);*/
	BO_SequenceParameters SeqPar;

	DB_SQLite *sqlSeqpar = new DB_SQLite;
	WorkDBLoadSeqPar(sqlSeqpar, &SeqPar);
	//====================================����� ��������===============================================
	//====================================��������� �� �� ���������� �������������=====================
	BO_ResultsTest RTforRI[15];
	//��������� ������ ���� ResultsTest
	for (int i = 0; i < 15; i++)
	{
		RTforRI[i].SetResultTest(0.00);
		RTforRI[i].SetTestNumber(0);
	}

	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer1()];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer2()];

	//��������� ������ ���� ResultsIteration
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer1(); i++)
	{
		resultsiterationVer1[i].SetResultIteration(RTforRI);
	}

	//��������� ������ ���� ResultsIteration
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer2(); i++)
	{
		resultsiterationVer2[i].SetResultIteration(RTforRI);
	}

	BO_ResultsExperiment experiment1(resultsiterationVer1, SeqPar.GetNumberOfIterationsVer1());
	BO_ResultsExperiment experiment2(resultsiterationVer2, SeqPar.GetNumberOfIterationsVer2());
	// ���������� ������������
	BO_ResultsExperiment *GetRE1, *GetRE2;
	BO_ResultsIteration *GetRI;
	BO_ResultsTest *GetRT;

	//stub->LoadData(&experiment1, &experiment2, &SeqPar);

	DB_SQLite *sql = new DB_SQLite;
	// ������ ���������� �������� ��������� ���������� ���������� (��� sqlite)
	WorkDBLoad(sql, &experiment1, &experiment2, &SeqPar);
	//====================================����� ��������======================

	GetRI = experiment1.GetResultsExperiment();

	QPainter painter(this);

	//��� �������������
	QPointF points[3] = {
		QPointF(0, 0),
		QPointF(0, 0),
		QPointF(0, 0),
	};

	//��� �������������
	QRectF rectangle(0, 0, 0, 0);
	int startAngle;// = -(30 * 16);
	int spanAngle;// = -(120 * 16);

	//��������
	//�� �����������
	int offsetline = 0;
	int curposface = 50;
	int curposeyesleft = 85;
	int curposeyesright = 145;
	int curpospupilleft = 95;
	int curpospupilright = 155;
	int curposnosetop = 130;
	int curposnoseleft = 120;
	int curposnoseright = 140;
	int curposmouth = 105; //���
	int curposearleft = 30; //���
	int curposearright = 210; //���
	int curposbrowleft = 80; //�����
	int curposbrowright = 140; //�����

	//�� ���������
	int offsetcolumn = 270;
	int columnface = 60;
	int columneyesleft = 120;
	int columneyesright = 120;
	int columnpupilleft = 130;
	int columnpupilright = 130;
	int columnnosetop = 160;
	int columnnoseleft = 190;
	int columnnoseright = 190;
	int columnmouth = 210;//������ ������ ���
	int columnearleft = 150;
	int columnearright = 150;
	int columnbrowleft = 85; //�����
	int columnbrowright = 85; //�����

	//������� ��� ����� ������������� ���� (� ������ �������� 10 ���)

	int countIterline, countItercolumn;
	int countfacesVar1 = SeqPar.GetNumberOfIterationsVer1(); int mod = 0, whole = 0;

	whole = countfacesVar1 / 10;
	mod = countfacesVar1 % 10;
	countItercolumn = whole;

	//������� ����������� ������
	int iterVar1 = 0;

	if (countfacesVar1 < 10)
	{
		countIterline = countfacesVar1;
	}
	else
	{
		countIterline = 10;
	}

	QFont font;
	font.setItalic(true);
	font.setPointSize(50);
	painter.setPen(QPen(Qt::green, Qt::TextColorRole));
	painter.setFont(font);
	painter.drawText(QPoint(825, 50), "FACES: VARIANT 1");

	font.setItalic(false);
	font.setPointSize(50);
	painter.setPen(QPen(Qt::black, Qt::SolidPattern));
	painter.setFont(font);



	for (int i = 0; i < countItercolumn + 1; i++)
	{
		if ((countIterline == 10) && (i == countItercolumn))
		{
			countIterline = 1;
		}
		else
		{
			//���������� �����
			GetRT = GetRI[iterVar1].GetResultIteration();

			offsetline = 0;
			//���� ����
			painter.drawEllipse(curposface = curposface + offsetline, columnface, 160, 220);

			//�����
			painter.drawEllipse(curposeyesleft = curposeyesleft + offsetline, columneyesleft, 30 + GetRT[0].GetResultTest(), 30 + GetRT[1].GetResultTest()); //������ � ������
			painter.drawEllipse(curposeyesright = curposeyesright + offsetline, columneyesright, 30 + GetRT[0].GetResultTest(), 30 + GetRT[1].GetResultTest());

			//������
			painter.setBrush(QBrush(Qt::red, Qt::SolidPattern));
			painter.drawEllipse(curpospupilleft = curpospupilleft + offsetline, columnpupilleft, 10 + GetRT[2].GetResultTest(), 10 + GetRT[3].GetResultTest());
			painter.drawEllipse(curpospupilright = curpospupilright + offsetline, columnpupilright, 10 + GetRT[2].GetResultTest(), 10 + GetRT[3].GetResultTest());

			painter.setBrush(QBrush(Qt::black, Qt::TexturePattern));
			//���
			points[0].setX(curposnosetop = curposnosetop + offsetline);
			points[0].setY(columnnosetop + GetRT[4].GetResultTest());//������ ����
			points[1].setX((curposnoseleft = curposnoseleft + offsetline) - GetRT[5].GetResultTest());//������ ���� -10
			points[1].setY(columnnoseleft);
			points[2].setX((curposnoseright = curposnoseright + offsetline) + GetRT[5].GetResultTest());//������ ���� +10
			points[2].setY(columnnoseright);
			painter.drawPolygon(points, 3);

			//���
			rectangle.setLeft(curposmouth = curposmouth + offsetline);
			rectangle.setTop(columnmouth);
			rectangle.setWidth(50 + GetRT[6].GetResultTest());//������ ���
			rectangle.setHeight(30 + GetRT[7].GetResultTest());//������ ���
			startAngle = -((10 * GetRT[8].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			spanAngle = -((10 * GetRT[9].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			painter.drawArc(rectangle, startAngle, spanAngle);

			//��� ������ �� x, ������ �� y, ������, ������
			painter.drawEllipse(curposearleft = curposearleft + offsetline, columnearleft, 20, 40 + GetRT[10].GetResultTest());
			painter.drawEllipse(curposearright = curposearright + offsetline, columnearright, 20, 40 + GetRT[10].GetResultTest());

			//�����
			//�����
			rectangle.setLeft(curposbrowleft = curposbrowleft + offsetline);
			rectangle.setTop(columnbrowleft);
			rectangle.setWidth(50 + GetRT[11].GetResultTest());//������ ������
			rectangle.setHeight(30 + GetRT[12].GetResultTest());//������ ������
			startAngle = -((10 * GetRT[13].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			spanAngle = -((10 * GetRT[14].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			painter.drawArc(rectangle, startAngle, spanAngle);

			//������
			rectangle.setLeft(curposbrowright = curposbrowright + offsetline);
			rectangle.setTop(columnbrowright);
			rectangle.setWidth(50 + GetRT[11].GetResultTest());//������ ������
			rectangle.setHeight(30 + GetRT[12].GetResultTest());//������ ������
			startAngle = -((10 * GetRT[13].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			spanAngle = -((10 * GetRT[14].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			painter.drawArc(rectangle, startAngle, spanAngle);
			iterVar1++; //����������� ������� ����������� ������
		}
		offsetline = 210;
		for (int j = 0; j < countIterline - 1; j++)
		{
			//���������� �����
			GetRT = GetRI[iterVar1].GetResultIteration();

			//���� ����
			painter.drawEllipse(curposface = curposface + offsetline, columnface, 160, 220);

			//�����
			painter.drawEllipse(curposeyesleft = curposeyesleft + offsetline, columneyesleft, 30 + GetRT[0].GetResultTest(), 30 + GetRT[1].GetResultTest()); //������ � ������
			painter.drawEllipse(curposeyesright = curposeyesright + offsetline, columneyesright, 30 + GetRT[0].GetResultTest(), 30 + GetRT[1].GetResultTest());

			//������
			painter.setBrush(QBrush(Qt::red, Qt::SolidPattern));
			painter.drawEllipse(curpospupilleft = curpospupilleft + offsetline, columnpupilleft, 10 + GetRT[2].GetResultTest(), 10 + GetRT[3].GetResultTest());
			painter.drawEllipse(curpospupilright = curpospupilright + offsetline, columnpupilright, 10 + GetRT[2].GetResultTest(), 10 + GetRT[3].GetResultTest());


			painter.setBrush(QBrush(Qt::red, Qt::TexturePattern));
			//���
			points[0].setX(curposnosetop = curposnosetop + offsetline);
			points[0].setY(columnnosetop + GetRT[4].GetResultTest());//������ ����
			points[1].setX((curposnoseleft = curposnoseleft + offsetline) - GetRT[5].GetResultTest());//������ ���� -10
			points[1].setY(columnnoseleft);
			points[2].setX((curposnoseright = curposnoseright + offsetline) + GetRT[5].GetResultTest());//������ ���� +10
			points[2].setY(columnnoseright);
			painter.drawPolygon(points, 3);

			//���
			rectangle.setLeft(curposmouth = curposmouth + offsetline);
			rectangle.setTop(columnmouth);
			rectangle.setWidth(50 + GetRT[6].GetResultTest());//������ ���
			rectangle.setHeight(30 + GetRT[7].GetResultTest());//������ ���
			startAngle = -((10 * GetRT[8].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			spanAngle = -((10 * GetRT[9].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			painter.drawArc(rectangle, startAngle, spanAngle);

			//��� ������ �� x, ������ �� y, ������, ������
			painter.drawEllipse(curposearleft = curposearleft + offsetline, columnearleft, 20, 40 + GetRT[10].GetResultTest());
			painter.drawEllipse(curposearright = curposearright + offsetline, columnearright, 20, 40 + GetRT[10].GetResultTest());

			//�����
			//�����
			rectangle.setLeft(curposbrowleft = curposbrowleft + offsetline);
			rectangle.setTop(columnbrowleft);
			rectangle.setWidth(50 + GetRT[11].GetResultTest());//������ ������
			rectangle.setHeight(30 + GetRT[12].GetResultTest());//������ ������
			startAngle = -((30 + GetRT[13].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			spanAngle = -((130 + GetRT[14].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			painter.drawArc(rectangle, startAngle, spanAngle);

			//������
			rectangle.setLeft(curposbrowright = curposbrowright + offsetline);
			rectangle.setTop(columnbrowright);
			rectangle.setWidth(50 + GetRT[11].GetResultTest());//������ ������
			rectangle.setHeight(30 + GetRT[12].GetResultTest());//������ ������
			startAngle = -((30 + GetRT[13].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			spanAngle = -((130 + GetRT[14].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			painter.drawArc(rectangle, startAngle, spanAngle);

			iterVar1++; //����������� ������� ����������� ������
		}

		//���� ������ 10 ��� � ������
		if ((i + 1 == whole) && (mod != 0))
		{
			countIterline = mod;
		}

		if ((countIterline == 10) && (i + 1 == countItercolumn))
		{
			//countIterline = 1;
		}
		else
		{
			offsetline = 210;
			curposface = 50;
			curposeyesleft = 85;
			curposeyesright = 145;
			curpospupilleft = 95;
			curpospupilright = 155;
			curposnosetop = 130;
			curposnoseleft = 120;
			curposnoseright = 140;
			curposmouth = 105; //���
			curposearleft = 30; //���
			curposearright = 210; //���
			curposbrowleft = 80; //�����
			curposbrowright = 140; //�����

			columnface = columnface + offsetcolumn;
			columneyesleft = columneyesleft + offsetcolumn;
			columneyesright = columneyesright + offsetcolumn;
			columnpupilleft = columnpupilleft + offsetcolumn;
			columnpupilright = columnpupilright + offsetcolumn;
			columnnosetop = columnnosetop + offsetcolumn;
			columnnoseleft = columnnoseleft + offsetcolumn;
			columnnoseright = columnnoseright + offsetcolumn;
			columnmouth = columnmouth + offsetcolumn;
			columnearleft = columnearleft + offsetcolumn;
			columnearright = columnearright + offsetcolumn;
			columnbrowleft = columnbrowleft + offsetcolumn;
			columnbrowright = columnbrowright + offsetcolumn;
		}
	}

	font.setItalic(true);
	painter.setPen(QPen(Qt::green, Qt::TextColorRole));
	painter.setFont(font);
	painter.drawText(QPoint(825, columnface + 50), "FACES: VARIANT 2");

	font.setItalic(false);
	painter.setPen(QPen(Qt::black, Qt::SolidPattern));
	painter.setFont(font);

	//=================��������� ��� ������� ������� ���====================================
	offsetline = 0;
	curposface = 50;
	curposeyesleft = 85;
	curposeyesright = 145;
	curpospupilleft = 95;
	curpospupilright = 155;
	curposnosetop = 130;
	curposnoseleft = 120;
	curposnoseright = 140;
	curposmouth = 105; //���
	curposearleft = 30; //���
	curposearright = 210; //���
	curposbrowleft = 80; //�����
	curposbrowright = 140; //�����

	int tmpoffset = 150;
	columnface = columnface + offsetcolumn - tmpoffset;
	columneyesleft = columneyesleft + offsetcolumn - tmpoffset;
	columneyesright = columneyesright + offsetcolumn - tmpoffset;
	columnpupilleft = columnpupilleft + offsetcolumn - tmpoffset;
	columnpupilright = columnpupilright + offsetcolumn - tmpoffset;
	columnnosetop = columnnosetop + offsetcolumn - tmpoffset;
	columnnoseleft = columnnoseleft + offsetcolumn - tmpoffset;
	columnnoseright = columnnoseright + offsetcolumn - tmpoffset;
	columnmouth = columnmouth + offsetcolumn - tmpoffset;
	columnearleft = columnearleft + offsetcolumn - tmpoffset;
	columnearright = columnearright + offsetcolumn - tmpoffset;
	columnbrowleft = columnbrowleft + offsetcolumn - tmpoffset;
	columnbrowright = columnbrowright + offsetcolumn - tmpoffset;

	GetRI = experiment2.GetResultsExperiment();
	//������� ����������� ������
	int iterVar2 = 0;
	int countfacesVar2 = SeqPar.GetNumberOfIterationsVer2();
	mod = 0;
	whole = 0;

	whole = countfacesVar2 / 10;
	mod = countfacesVar2 % 10;
	countItercolumn = whole;

	if (countfacesVar2 < 10)
	{
		countIterline = countfacesVar2;
	}
	else
	{
		countIterline = 10;
	}
	//=================������ ������� ���====================================
	for (int i = 0; i < countItercolumn + 1; i++)
	{
		if ((countIterline == 10) && (i == countItercolumn))
		{
			countIterline = 1;
		}
		else
		{
			//���������� �����
			GetRT = GetRI[iterVar2].GetResultIteration();

			offsetline = 0;
			//���� ����
			painter.drawEllipse(curposface = curposface + offsetline, columnface, 160, 220);

			//�����
			painter.drawEllipse(curposeyesleft = curposeyesleft + offsetline, columneyesleft, 30 + GetRT[0].GetResultTest(), 30 + GetRT[1].GetResultTest()); //������ � ������
			painter.drawEllipse(curposeyesright = curposeyesright + offsetline, columneyesright, 30 + GetRT[0].GetResultTest(), 30 + GetRT[1].GetResultTest());

			//������
			painter.setBrush(QBrush(Qt::red, Qt::SolidPattern));
			painter.drawEllipse(curpospupilleft = curpospupilleft + offsetline, columnpupilleft, 10 + GetRT[2].GetResultTest(), 10 + GetRT[3].GetResultTest());
			painter.drawEllipse(curpospupilright = curpospupilright + offsetline, columnpupilright, 10 + GetRT[2].GetResultTest(), 10 + GetRT[3].GetResultTest());

			painter.setBrush(QBrush(Qt::black, Qt::TexturePattern));
			//���
			points[0].setX(curposnosetop = curposnosetop + offsetline);
			points[0].setY(columnnosetop + GetRT[4].GetResultTest());//������ ����
			points[1].setX((curposnoseleft = curposnoseleft + offsetline) - GetRT[5].GetResultTest());//������ ���� -10
			points[1].setY(columnnoseleft);
			points[2].setX((curposnoseright = curposnoseright + offsetline) + GetRT[5].GetResultTest());//������ ���� +10
			points[2].setY(columnnoseright);
			painter.drawPolygon(points, 3);

			//���
			rectangle.setLeft(curposmouth = curposmouth + offsetline);
			rectangle.setTop(columnmouth);
			rectangle.setWidth(50 + GetRT[6].GetResultTest());//������ ���
			rectangle.setHeight(30 + GetRT[7].GetResultTest());//������ ���
			startAngle = -((10 * GetRT[8].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			spanAngle = -((10 * GetRT[9].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			painter.drawArc(rectangle, startAngle, spanAngle);

			//��� ������ �� x, ������ �� y, ������, ������
			painter.drawEllipse(curposearleft = curposearleft + offsetline, columnearleft, 20, 40 + GetRT[10].GetResultTest());
			painter.drawEllipse(curposearright = curposearright + offsetline, columnearright, 20, 40 + GetRT[10].GetResultTest());

			//�����
			//�����
			rectangle.setLeft(curposbrowleft = curposbrowleft + offsetline);
			rectangle.setTop(columnbrowleft);
			rectangle.setWidth(50 + GetRT[11].GetResultTest());//������ ������
			rectangle.setHeight(30 + GetRT[12].GetResultTest());//������ ������
			startAngle = -((10 * GetRT[13].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			spanAngle = -((10 * GetRT[14].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			painter.drawArc(rectangle, startAngle, spanAngle);

			//������
			rectangle.setLeft(curposbrowright = curposbrowright + offsetline);
			rectangle.setTop(columnbrowright);
			rectangle.setWidth(50 + GetRT[11].GetResultTest());//������ ������
			rectangle.setHeight(30 + GetRT[12].GetResultTest());//������ ������
			startAngle = -((10 * GetRT[13].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			spanAngle = -((10 * GetRT[14].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			painter.drawArc(rectangle, startAngle, spanAngle);
			iterVar2++; //����������� ������� ����������� ������
		}
		offsetline = 210;
		for (int j = 0; j < countIterline - 1; j++)
		{
			//���������� �����
			GetRT = GetRI[iterVar2].GetResultIteration();

			//���� ����
			painter.drawEllipse(curposface = curposface + offsetline, columnface, 160, 220);

			//�����
			painter.drawEllipse(curposeyesleft = curposeyesleft + offsetline, columneyesleft, 30 + GetRT[0].GetResultTest(), 30 + GetRT[1].GetResultTest()); //������ � ������
			painter.drawEllipse(curposeyesright = curposeyesright + offsetline, columneyesright, 30 + GetRT[0].GetResultTest(), 30 + GetRT[1].GetResultTest());

			//������
			painter.setBrush(QBrush(Qt::red, Qt::SolidPattern));
			painter.drawEllipse(curpospupilleft = curpospupilleft + offsetline, columnpupilleft, 10 + GetRT[2].GetResultTest(), 10 + GetRT[3].GetResultTest());
			painter.drawEllipse(curpospupilright = curpospupilright + offsetline, columnpupilright, 10 + GetRT[2].GetResultTest(), 10 + GetRT[3].GetResultTest());


			painter.setBrush(QBrush(Qt::red, Qt::TexturePattern));
			//���
			points[0].setX(curposnosetop = curposnosetop + offsetline);
			points[0].setY(columnnosetop + GetRT[4].GetResultTest());//������ ����
			points[1].setX((curposnoseleft = curposnoseleft + offsetline) - GetRT[5].GetResultTest());//������ ���� -10
			points[1].setY(columnnoseleft);
			points[2].setX((curposnoseright = curposnoseright + offsetline) + GetRT[5].GetResultTest());//������ ���� +10
			points[2].setY(columnnoseright);
			painter.drawPolygon(points, 3);

			//���
			rectangle.setLeft(curposmouth = curposmouth + offsetline);
			rectangle.setTop(columnmouth);
			rectangle.setWidth(50 + GetRT[6].GetResultTest());//������ ���
			rectangle.setHeight(30 + GetRT[7].GetResultTest());//������ ���
			startAngle = -((10 * GetRT[8].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			spanAngle = -((10 * GetRT[9].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			painter.drawArc(rectangle, startAngle, spanAngle);

			//��� ������ �� x, ������ �� y, ������, ������
			painter.drawEllipse(curposearleft = curposearleft + offsetline, columnearleft, 20, 40 + GetRT[10].GetResultTest());
			painter.drawEllipse(curposearright = curposearright + offsetline, columnearright, 20, 40 + GetRT[10].GetResultTest());

			//�����
			//�����
			rectangle.setLeft(curposbrowleft = curposbrowleft + offsetline);
			rectangle.setTop(columnbrowleft);
			rectangle.setWidth(50 + GetRT[11].GetResultTest());//������ ������
			rectangle.setHeight(30 + GetRT[12].GetResultTest());//������ ������
			startAngle = -((30 + GetRT[13].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			spanAngle = -((130 + GetRT[14].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			painter.drawArc(rectangle, startAngle, spanAngle);

			//������
			rectangle.setLeft(curposbrowright = curposbrowright + offsetline);
			rectangle.setTop(columnbrowright);
			rectangle.setWidth(50 + GetRT[11].GetResultTest());//������ ������
			rectangle.setHeight(30 + GetRT[12].GetResultTest());//������ ������
			startAngle = -((30 + GetRT[13].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			spanAngle = -((130 + GetRT[14].GetResultTest()) * 16);//���� �� (� ��������) �� ������� �������
			painter.drawArc(rectangle, startAngle, spanAngle);

			iterVar2++; //����������� ������� ����������� ������
		}

		//���� ������ 10 ��� � ������
		if ((i + 1 == whole) && (mod != 0))
		{
			countIterline = mod;
		}

		if ((countIterline == 10) && (i + 1 == countItercolumn))
		{
			//countIterline = 1;
		}
		else
		{
			offsetline = 210;
			curposface = 50;
			curposeyesleft = 85;
			curposeyesright = 145;
			curpospupilleft = 95;
			curpospupilright = 155;
			curposnosetop = 130;
			curposnoseleft = 120;
			curposnoseright = 140;
			curposmouth = 105; //���
			curposearleft = 30; //���
			curposearright = 210; //���
			curposbrowleft = 80; //�����
			curposbrowright = 140; //�����

			columnface = columnface + offsetcolumn;
			columneyesleft = columneyesleft + offsetcolumn;
			columneyesright = columneyesright + offsetcolumn;
			columnpupilleft = columnpupilleft + offsetcolumn;
			columnpupilright = columnpupilright + offsetcolumn;
			columnnosetop = columnnosetop + offsetcolumn;
			columnnoseleft = columnnoseleft + offsetcolumn;
			columnnoseright = columnnoseright + offsetcolumn;
			columnmouth = columnmouth + offsetcolumn;
			columnearleft = columnearleft + offsetcolumn;
			columnearright = columnearright + offsetcolumn;
			columnbrowleft = columnbrowleft + offsetcolumn;
			columnbrowright = columnbrowright + offsetcolumn;
		}
	}
}

/*
void QTGUI::paintEvent(QPaintEvent * event)
{
}*/

QTGUI::QTGUI(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	connect(ui.pushButton, &QPushButton::clicked, this, &QTGUI::on_pushButton_clicked);
	connect(ui.pushButton_2, &QPushButton::clicked, this, &QTGUI::on_pushButton_clicked_2);
	//connect(ui.pushButton, &QPushButton::clicked, this, &QTGUI::on_pushButton_clicked_2);

	//=========�������� ����������� �������� ���������� ������������������=================
	std::ifstream Fin;
	std::string strl, strl2, strl3;
	char ch;
	Fin.open("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\parameters", std::ios::in); //�������� ��� ������
	Fin >> strl >> strl2 >> strl3;
	Fin.close();
	QString stringl, stringl2, stringl3;
	stringl = QString::fromStdString(strl);
	stringl2 = QString::fromStdString(strl2);
	stringl3 = QString::fromStdString(strl3);
	ui.lineEdit->setText(stringl);
	ui.lineEdit_2->setText(stringl2);
	ui.lineEdit_3->setText(stringl3);
	//======================================================================================

	ui.lineEdit_7->setHidden(true);
	ui.label_10->setHidden(true);
	ui.label_11->setHidden(true);
	ui.label_12->setHidden(true);
	ui.graphicsView->setHidden(true);
	ui.tableView->setHidden(true);//������������� �������� �������

	//scroll �������� ����
	QScrollArea *scrollmain = new QScrollArea;
	scrollmain -> setMinimumSize(1200, 800);
	scrollmain->setWidget(this);
	scrollmain->show();
}

void QTGUI::on_pushButton_clicked()
{
	//��������� ��������� ������������������ � ������
	QString stringl, stringl2, stringl3;
	stringl = ui.lineEdit->text();

	int step = ui.lineEdit_2->text().toInt();
	stringl2 = QString::number(step);

	int lengthsequence = ui.lineEdit_3->text().toInt();
	stringl3 = QString::number(lengthsequence);

	//��������������� ������ � ������ ����� � ������ �������� ��� SeqParameters
	QByteArray ba = stringl.toLocal8Bit();
	char *c_str2 = ba.data();
	BO_SequenceParameters SeqPar(c_str2, lengthsequence, 0, 0, step);
	BC_ControlComponent * control = new BC_ControlComponent;
	control->ValidationOfParameters(&SeqPar); //����� ������������ ���-�� �������� ��� 2 ��������� ������
	//ui.lineEdit_4->setText(stringl);
	//ui.lineEdit_5->setText(stringl2);
	//ui.lineEdit_6->setText(stringl3);

	//==============================================================================
	BO_ResultsTest RTforRI[15];
	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer1()];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer2()];

	//��������� ������ ���� ResultsIteration
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer1(); i++)
	{
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(1 + rand() % 10);
			RTforRI[i].SetTestNumber(1);
		}
		resultsiterationVer1[i].SetResultIteration(RTforRI);
	}

	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer2(); i++)
	{
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(1 + rand() % 10);
			RTforRI[i].SetTestNumber(1);
		}
		resultsiterationVer2[i].SetResultIteration(RTforRI);
	}

	BO_ResultsExperiment experiment1(resultsiterationVer1, SeqPar.GetNumberOfIterationsVer1());
	BO_ResultsExperiment experiment2(resultsiterationVer2, SeqPar.GetNumberOfIterationsVer2());
	// ���������� ������������
	BO_ResultsExperiment *GetRE1, *GetRE2;
	BO_ResultsIteration *GetRI;
	BO_ResultsTest *GetRT;

	//================================�������� �� ��===========================================
	//BC_Stub * stub = new BC_Stub;// ���������� ��������
	//stub->LoadData(&experiment1, &experiment2, &SeqPar);
	DB_SQLite *sql = new DB_SQLite;
	WorkDBSave(sql, &experiment1, &experiment2, &SeqPar); // ������ ���������� �������� ��������� ���������� ���������� (��� sqlite)
	WorkDBLoad(sql, &experiment1, &experiment2, &SeqPar);
	//================================����� ��������===========================================
	
	QStandardItemModel *model = new QStandardItemModel;
	QStandardItem *item;

	//��������� ��������
	QStringList horizontalHeader;
	horizontalHeader.append("Name Test");
	horizontalHeader.append("Results Variant 1");
	horizontalHeader.append("Results Variant 2");
	horizontalHeader.append("Iteration Number");

	model->setHorizontalHeaderLabels(horizontalHeader);

	//����������
	//item = new QStandardItem(QString("0"));
	//model->setItem(0, 0, item);

	int horizline = 0;// � ����� ������ ���������� ���������
	// Experiment1
	GetRI = experiment1.GetResultsExperiment();
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer1(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			QString testname = "Test" + QString::number(j + 1);
			item = new QStandardItem(testname); //����� ���� ������������� � ������
			model->setItem(horizline, 0, item); // 1 �������
			item = new QStandardItem(QString::number(GetRT[j].GetResultTest())); //����� ���� ������������� � ������
			model->setItem(horizline, 1, item); // 1 �������
			item = new QStandardItem(QString::number(i + 1)); //����� ���� ������������� � ������?
			model->setItem(horizline, 3, item); // ������� ����� �������� � 3 �������
			horizline++;
		}
		item = new QStandardItem(QString("End results of iteration"));
		model->setItem(horizline, 0, item);
		horizline++;
	}

	horizline = 0;
	// Experiment2
	GetRI = experiment2.GetResultsExperiment();
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer2(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			item = new QStandardItem(QString::number(GetRT[j].GetResultTest())); //����� ���� ������������� � ������
			model->setItem(horizline, 2, item); //2 �������
			horizline++;
		}
		item = new QStandardItem(QString("End results of iteration"));
		model->setItem(horizline, 0, item);
		horizline++;
	}


	//==================================����� ��� �� �����=============================================================
	Widget *wgt = new Widget();
	// ������ ����������� �� 10 ��� = 2400, ����� ������ + 50

	int countfacesV1 = SeqPar.GetNumberOfIterationsVer1();	//���������� ��� CountIterV1
	int countlineV1;//���������� �����

	//���� ���� ������� �� �������, �� + 1 � ���������� �����
	
	countlineV1 = countfacesV1 / 10;

	if ((countfacesV1 % 10) != 0)
	{
		countlineV1 = countlineV1 + 1;
	}

	int countfacesV2 = SeqPar.GetNumberOfIterationsVer2();	//���������� ��� CountIterV2
	int countlineV2;//���������� �����

	//���� ���� ������� �� �������, �� + 1 � ���������� �����
	countlineV2 = countfacesV2 / 10;

	if ((countfacesV2 % 10) != 0)
	{
		countlineV2 = countlineV2 + 1;
	}


	wgt->setMinimumSize(2100 + 50, 270 * (countlineV1 + countlineV2) + 50 + 120);//50 + 120 ��� ������ ������
	QScrollArea *scroll = new QScrollArea;
	scroll->setWidget(wgt);
	//scroll->update();
	//scroll.widgetResizable(false);
	//=============================================����� ���������======================================================
	wgt->paintfaces();
	//==================================================================================================================


	ui.tableView->setModel(model);

	ui.tableView->resizeRowsToContents();
	ui.tableView->resizeColumnsToContents();

	ui.label_10->setHidden(false);
	ui.label_11->setHidden(false);
	ui.label_12->setHidden(false);
	ui.tableView->setHidden(false);//���������� �������
	ui.graphicsView->setHidden(false);
	ui.lineEdit_7->setHidden(false);

	/*
	QGraphicsScene* scene = new QGraphicsScene();
	QGraphicsView* view = new QGraphicsView(scene);
	QGraphicsPixmapItem* item = new QGraphicsPixmapItem(QPixmap::fromImage("F:\\1\\AIS\\sts - 2.1.2\\sts - 2.1.2\\data\\Faces.png"));
	scene->addItem(item);
	view->show();*/

	QGraphicsScene* scene = new QGraphicsScene();
	ui.graphicsView->setScene(scene);
	QPixmap * pixmap = new QPixmap("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\Faces.png");
		
	//������� ���-�����
	std::string stringHashSum;
	control->CalculateHashSum("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\Faces.png", stringHashSum);
	ui.lineEdit_7->setText(QString::fromStdString(stringHashSum));
	
	//pixmap->scaled(1024, 500, Qt::KeepAspectRatio);
	QGraphicsPixmapItem * item1 = new QGraphicsPixmapItem(*pixmap);
	scene->addItem(item1);
	ui.graphicsView->show();
}


void QTGUI::on_pushButton_clicked_2()
{

	//==============================================================================
	/*
	BO_SequenceParameters SeqPar;

	DB_SQLite *sqlSeqpar = new DB_SQLite;
	WorkDBLoadSeqPar(sqlSeqpar, &SeqPar);
	*/
	int lengthfile = 50;
	char * FName = new char[lengthfile];
	memset(FName, 0, sizeof(FName));
	strcpy_s(FName, strlen("F:\\1\\AIS\\sts - 2.1.2\\sts - 2.1.2\\data\\1.bin"), "F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\1.bin");
	BO_SequenceParameters SeqPar(FName, 100, 0, 0, 5);
	BC_ControlComponent * control = new BC_ControlComponent;
	control->ValidationOfParameters(&SeqPar);


	BO_ResultsTest RTforRI[15];
	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer1()];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer2()];

	//��������� ������ ���� ResultsIteration
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer1(); i++)
	{
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(1 + rand() % 10);
			RTforRI[i].SetTestNumber(1);
		}
		resultsiterationVer1[i].SetResultIteration(RTforRI);
	}

	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer2(); i++)
	{
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(1 + rand() % 10);
			RTforRI[i].SetTestNumber(1);
		}
		resultsiterationVer2[i].SetResultIteration(RTforRI);
	}

	BO_ResultsExperiment experiment1(resultsiterationVer1, SeqPar.GetNumberOfIterationsVer1());
	BO_ResultsExperiment experiment2(resultsiterationVer2, SeqPar.GetNumberOfIterationsVer2());
	// ���������� ������������
	BO_ResultsExperiment *GetRE1, *GetRE2;
	BO_ResultsIteration *GetRI;
	BO_ResultsTest *GetRT;

	//================================�������� �� ��===========================================
	//BC_Stub * stub = new BC_Stub;// ���������� ��������
	//stub->LoadData(&experiment1, &experiment2, &SeqPar);
	DB_SQLite *sql = new DB_SQLite;
	WorkDBSave(sql, &experiment1, &experiment2, &SeqPar); // ������ ���������� �������� ��������� ���������� ���������� (��� sqlite)
	//WorkDBLoad(sql, &experiment1, &experiment2, &SeqPar);
	//================================����� ��������===========================================
	QString string1 = QString::fromStdString(SeqPar.GetFileName());
	QString string2 = QString::number(SeqPar.GetLengthSequence());
	QString string3 = QString::number(SeqPar.GetStep());

	ui.lineEdit_4->setText(string1);
	ui.lineEdit_5->setText(string3);
	ui.lineEdit_6->setText(string2);
}


void QTGUI::mainSlot(QString str)
{
}
