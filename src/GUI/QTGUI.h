#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_QTGUI.h"
#include <QStandardItemModel>
#include <QMainWindow>
#include <qpushbutton.h>
#include <qtableview.h>
#include <QTableView>
#include <signal.h>
#include <QPainter>
#include <QMessageBox>
#include <qframe.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qscreen.h>
#include <qstyleditemdelegate.h>
#include <qscrollarea.h>
#include <qgridlayout.h>
#include <qgraphicsscene.h>
#include <QGraphicsView.h>
#include <qgraphicsitem.h>

#include "../chernofffaces/BC_ControlComponent.h"
#include "../chernofffaces/BC_ValidationOfParameters.h"
#include "../chernofffaces/BC_OpeningFileAndParsing.h"
#include "../chernofffaces/BC_Stub.h"
#include "../chernofffaces/IGateWay.h"
#include "../chernofffaces/DB_SQLite.h"
#include "../NISTTestInterface/TestInterface.h"
//#include "../NISTTestInterface/InterfaceImplementations/include/NISTTests.h"


class QTGUI : public QMainWindow
{
	Q_OBJECT

public:
	QTGUI(QWidget *parent = Q_NULLPTR);

	void mainSignal(QString str);

	//void paintEvent(QPaintEvent *event);

	void on_pushButton_clicked();
	void on_pushButton_clicked_2();

	void mainSlot(QString str);
private:
	Ui::QtGuiApplicationClass ui;
	QStandardItemModel *model;
	QTableView * tableView;
};

class QPaintEvent;

class Widget : public QWidget
{
	Q_OBJECT
public:
	Widget(QWidget * parent = 0);
	void paintfaces();
protected:
	void paintEvent(QPaintEvent *);
};
