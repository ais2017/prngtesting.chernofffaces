#include "gtest/gtest.h"
#include "../chernofffaces/BC_ControlComponent.h"
#include "../chernofffaces/BC_ValidationOfParameters.h"
#include "../chernofffaces/BC_OpeningFileAndParsing.h"
#include "../chernofffaces/BC_Stub.h"
#include "../chernofffaces/IGateWay.h"
#include <iostream>

// |���� 1| ������ TestControlComponent
// ���� 1.1 ValidationOfParameters ��� ��������� ����������
TEST(TestControlComponent, ValidationOfParametersIsTrue) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\1.bin", 3, 0, 0, 2);
	BC_ControlComponent * control = new BC_ControlComponent ;
	//std::cout << "control:" << control.ValidationOfParameters(&SeqPar) << std::endl;
	EXPECT_EQ(control->ValidationOfParameters(&SeqPar), 1); 
}

// ���� 1.2 ValidationOfParameters ����� ����� = 0
TEST(TestControlComponent, FileLengthIsNull) {
	BO_SequenceParameters SeqPar("", 5, 0, 0, 2);
	BC_ControlComponent * control = new BC_ControlComponent;
	EXPECT_EQ(control->ValidationOfParameters(&SeqPar), 2); 
}

// ���� 1.3 ValidationOfParameters ���� �� ����������
TEST(TestControlComponent, FileIsNotExists) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\15.bin", 5, 0, 0, 2);
	BC_ControlComponent * control = new BC_ControlComponent;
	EXPECT_EQ(control->ValidationOfParameters(&SeqPar), 3);
}

// ���� 1.4 ValidationOfParameters ��� ������ 0
TEST(TestControlComponent, StepIsNegative) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\1.bin", 5, 0, 0, -2);
	BC_ControlComponent * control = new BC_ControlComponent;
	EXPECT_EQ(control->ValidationOfParameters(&SeqPar), 4);
}

// ���� 1.5 ValidationOfParameters ����� ������������������ ������ 0
TEST(TestControlComponent, LengthSequenceIsNegative) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\1.bin", -100, 0, 0, 2);
	BC_ControlComponent * control = new BC_ControlComponent;
	EXPECT_EQ(control->ValidationOfParameters(&SeqPar), 5);
}

// ���� 1.6 ValidationOfParameters  ����� ������������������ ��������� ����� ����� - �� ����� ���������
TEST(TestControlComponent, LengthSequenceMoreThanSizeOfFile) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\1.bin", 1000, 0, 0, 2);
	BC_ControlComponent * control = new BC_ControlComponent;
	std::cout << "ERROR 1.6" << control->ValidationOfParameters(&SeqPar)  << std::endl;
	EXPECT_EQ(control->ValidationOfParameters(&SeqPar), 6);
}

// ���� 1.7 ValidationOfParameters ��� ������ ����� ������������������ - �� ����� ���������
TEST(TestControlComponent, StepMoreThanLengthSequence) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\1.bin", 5, 0, 0, 6);
	BC_ControlComponent * control = new BC_ControlComponent;
	EXPECT_EQ(control->ValidationOfParameters(&SeqPar), 7);
}

// ���� 1.8 �������� �� ������ ����
TEST(TestControlComponent, FileIsEmpty) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\2.bin", 4, 0, 0, 2);
	BC_ControlComponent * control = new BC_ControlComponent;
	EXPECT_EQ(control->ValidationOfParameters(&SeqPar), 8);
}

// ���� 1.9 �������� �� �������� � �������
TEST(TestControlComponent, OpenAndParsing) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\2.bin", 4, 0, 0, 2);
	BC_ControlComponent * control = new BC_ControlComponent;
	EXPECT_EQ(control->OpeningFileAndParsing(&SeqPar), 1);
}

/*
// ���� 1.10 �������� ���������� �������������� ������ 
TEST(TestControlComponent, ValuesForStatisticalTests) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\tmp2", 4, 0, 0, 3);
	BC_ControlComponent * control = new BC_ControlComponent;
	control->OpeningFileAndParsing(&SeqPar);
	//std::cout << "GetNumberOfIterationsVer1:" << SeqPar.GetNumberOfIterationsVer1() << std::endl;
	//std::cout << "GetNumberOfIterationsVer2:" << SeqPar.GetNumberOfIterationsVer2() << std::endl;

	// ������������� ��� ����������� �������������� ������
	//================================================
	//BO_ResultsTest RTforRI[15];

	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer1()];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer2()];*/
	/*
	//��������� ������ ���� ResultsTest
	for (int i = 0; i < 15; i++)
	{
		RTforRI[i].SetResultTest(0.00);
		RTforRI[i].SetTestNumber(i);
	}
	
	//��������� ������ ���� ResultsIteration1
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer1(); i++)
	{
		resultsiterationVer1[i].SetResultIteration(RTforRI);
	}
	
	//��������� ������ ���� ResultsIteration2
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer2(); i++)
	{
		resultsiterationVer2[i].SetResultIteration(RTforRI);
	}*/
/*
	BO_ResultsExperiment experiment1(resultsiterationVer1, SeqPar.GetNumberOfIterationsVer1());
	BO_ResultsExperiment experiment2(resultsiterationVer2, SeqPar.GetNumberOfIterationsVer2());
	//================================================

	EXPECT_EQ(control.ExecutionStatisticalTests(&SeqPar, &experiment1, &experiment2), 0);

	// ���������� ������������
	BO_ResultsExperiment *GetRE1, *GetRE2;
	BO_ResultsIteration *GetRI;
	BO_ResultsTest *GetRT;
	// Experiment1
	GetRI = experiment1.GetResultsExperiment();
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer1(); i++)
	{
		GetRT= GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			std::cout << "TestNumber[" << j << "]:" << GetRT[j].GetTestNumber() << "\n";
			std::cout << "ResultVer1[" << j << "]:" << GetRT[j].GetResultTest() << "\n";
		}
	}

	std::cout << "===============================================" << std::endl;
	// Experiment2
	GetRI = experiment2.GetResultsExperiment();
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer2(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			std::cout << "TestNumber[" << j << "]:" << GetRT[j].GetTestNumber() << "\n";
			std::cout << "ResultVer2[" << j << "]:" << GetRT[j].GetResultTest() << "\n";
		}
	}
}*/

// ���� 1.11 �������� �� ���������� ����������� ������������� ������ � ��
TEST(TestStub, SaveData) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\tmp2", 4, 0, 0, 3);
	BC_ControlComponent * control = new BC_ControlComponent;
	control->OpeningFileAndParsing(&SeqPar);
	std::cout << "GetNumberOfIterationsVer1:" << SeqPar.GetNumberOfIterationsVer1() << std::endl;
	std::cout << "GetNumberOfIterationsVer2:" << SeqPar.GetNumberOfIterationsVer2() << std::endl;

	// ������������� ��� ����������� �������������� ������
	//================================================

	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer1()];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer2()];

	BO_ResultsExperiment experiment1(resultsiterationVer1, SeqPar.GetNumberOfIterationsVer1());
	BO_ResultsExperiment experiment2(resultsiterationVer2, SeqPar.GetNumberOfIterationsVer2());
	//================================================

	//control.ExecutionStatisticalTests(&SeqPar, &experiment1, &experiment2);
	BC_Stub * stub = new BC_Stub;// ���������� ��������
	stub->SaveData(&experiment1, &experiment2, &SeqPar);
	EXPECT_EQ(stub->SaveData(&experiment1, &experiment2, &SeqPar),0);
}

// ���� 1.12 �������� �������� �������� ��� ��������� ���
TEST(TestControlComponent, GenerationFace) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\tmp2", 4, 0, 0, 3);
	BC_ControlComponent * control = new BC_ControlComponent;
	control->OpeningFileAndParsing(&SeqPar);
	std::cout << "GetNumberOfIterationsVer1:" << SeqPar.GetNumberOfIterationsVer1() << std::endl;
	std::cout << "GetNumberOfIterationsVer2:" << SeqPar.GetNumberOfIterationsVer2() << std::endl;

	// ������������� ��� ����������� �������������� ������
	//================================================

	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer1()];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer2()];

	BO_ResultsExperiment experiment1(resultsiterationVer1, SeqPar.GetNumberOfIterationsVer1());
	BO_ResultsExperiment experiment2(resultsiterationVer2, SeqPar.GetNumberOfIterationsVer2());
	//================================================
	//control->ExecutionStatisticalTests(&SeqPar, &experiment1, &experiment2); // ���������� ������

	BO_FaceParametr * Faceparametr = new BO_FaceParametr[15];// ������ FaceParametr for Face ������ 15
	
	//������� ������� ��� ��� ������� ��������
	BO_Face * faceVer1 = new BO_Face[SeqPar.GetNumberOfIterationsVer1()];
	BO_Face * faceVer2 = new BO_Face[SeqPar.GetNumberOfIterationsVer2()];

	// ���������� ������������
	BO_ResultsExperiment *GetRE1, *GetRE2;
	BO_ResultsIteration *GetRI;
	BO_ResultsTest *GetRT;
	// Experiment1
	GetRI = experiment1.GetResultsExperiment();
	/*
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer1(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			
			Faceparametr[i].SetParametr(GetRT[j]);
			Faceparametr[i].SetNameParametr("Parametr1");
			//std::cout << "TestNumber[" << j << "]:" << GetRT[j].GetTestNumber() << "\n";
			//std::cout << "ResultVer1[" << j << "]:" << GetRT[j].GetResultTest() << "\n";
		}
		faceVer1[i].SetFaceParameters(Faceparametr);
		faceVer1[i].SetFaceName("Face1");
	}

	std::cout << "===============================================" << std::endl;
	// Experiment2
	GetRI = experiment2.GetResultsExperiment();
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer2(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			Faceparametr[i].SetParametr(GetRT[j]);
			Faceparametr[i].SetNameParametr("Parametr2");
			//std::cout << "TestNumber[" << j << "]:" << GetRT[j].GetTestNumber() << "\n";
			//std::cout << "ResultVer2[" << j << "]:" << GetRT[j].GetResultTest() << "\n";
		}
		faceVer2[i].SetFaceParameters(Faceparametr);
		faceVer2[i].SetFaceName("Face2");
	}*/

	EXPECT_EQ(control->GenerationFace(faceVer1, faceVer2), 0);
	delete[] Faceparametr;
}

// ���� 1.13 �������� ���������� � �� ���
TEST(TestStub, SaveDataFaces) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\tmp2", 4, 0, 0, 3);
	BC_ControlComponent * control = new BC_ControlComponent;
	control->OpeningFileAndParsing(&SeqPar);
	//std::cout << "GetNumberOfIterationsVer1:" << SeqPar.GetNumberOfIterationsVer1() << std::endl;
	//std::cout << "GetNumberOfIterationsVer2:" << SeqPar.GetNumberOfIterationsVer2() << std::endl;

	// ������������� ��� ����������� �������������� ������
	//================================================

	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer1()];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer2()];

	BO_ResultsExperiment experiment1(resultsiterationVer1, SeqPar.GetNumberOfIterationsVer1());
	BO_ResultsExperiment experiment2(resultsiterationVer2, SeqPar.GetNumberOfIterationsVer2());
	//================================================
	//control.ExecutionStatisticalTests(&SeqPar, &experiment1, &experiment2); // ���������� ������

	BO_FaceParametr * Faceparametr = new BO_FaceParametr[15];// ������ FaceParametr for Face ������ 15

	//������� ������� ��� ��� ������� ��������
	BO_Face * faceVer1 = new BO_Face[SeqPar.GetNumberOfIterationsVer1()];
	BO_Face * faceVer2 = new BO_Face[SeqPar.GetNumberOfIterationsVer2()];

	// ���������� ������������
	BO_ResultsExperiment *GetRE1, *GetRE2;
	BO_ResultsIteration *GetRI;
	BO_ResultsTest *GetRT;
	// Experiment1
	GetRI = experiment1.GetResultsExperiment();
	/*
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer1(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{

			Faceparametr[i].SetParametr(GetRT[j]);
			Faceparametr[i].SetNameParametr("Parametr1");
			//std::cout << "TestNumber[" << j << "]:" << GetRT[j].GetTestNumber() << "\n";
			//std::cout << "ResultVer1[" << j << "]:" << GetRT[j].GetResultTest() << "\n";
		}
		faceVer1[i].SetFaceParameters(Faceparametr);
		faceVer1[i].SetFaceName("Face1");
	}*/

	std::cout << "===============================================" << std::endl;
	// Experiment2
	GetRI = experiment2.GetResultsExperiment();
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer2(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			Faceparametr[i].SetParametr(GetRT[j]);
			Faceparametr[i].SetNameParametr("Parametr2");
			std::cout << "TestNumber[" << j << "]:" << GetRT[j].GetTestNumber() << "\n";
			std::cout << "ResultVer2[" << j << "]:" << GetRT[j].GetResultTest() << "\n";
		}
		faceVer2[i].SetFaceParameters(Faceparametr);
		faceVer2[i].SetFaceName("Face2");
	}

	BC_Stub * stub = new BC_Stub;// ���������� ��������
	stub->SaveDataFace(faceVer1, faceVer1);
	EXPECT_EQ(stub->SaveDataFace(faceVer1, faceVer2), 0);
	delete[] Faceparametr;
}

// ���� 1.14 �������� ������ ������
TEST(TestControlComponent, FindReport) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\2.bin", 4, 0, 0, 2);
	BC_ControlComponent * control = new BC_ControlComponent;
	EXPECT_EQ(control->FindReport(&SeqPar), 0);
}

// ���� 1.15 �������� ��������� ������
TEST(TestControlComponent, GenerationReport) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\tmp2", 4, 0, 0, 3);
	BC_ControlComponent * control = new BC_ControlComponent;
	control->OpeningFileAndParsing(&SeqPar);

	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer1()];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer2()];

	BO_ResultsExperiment experiment1(resultsiterationVer1, SeqPar.GetNumberOfIterationsVer1());
	BO_ResultsExperiment experiment2(resultsiterationVer2, SeqPar.GetNumberOfIterationsVer2());
	//================================================
	//control.ExecutionStatisticalTests(&SeqPar, &experiment1, &experiment2); // ���������� ������

	BO_FaceParametr * Faceparametr = new BO_FaceParametr[15];// ������ FaceParametr for Face ������ 15

	//������� ������� ��� ��� ������� ��������
	BO_Face *faceVer1 = new BO_Face[SeqPar.GetNumberOfIterationsVer1()];
	BO_Face *faceVer2 = new BO_Face[SeqPar.GetNumberOfIterationsVer2()];

	// ���������� ������������
	BO_ResultsExperiment *GetRE1, *GetRE2;
	BO_ResultsIteration *GetRI;
	BO_ResultsTest *GetRT;
	// Experiment1
	GetRI = experiment1.GetResultsExperiment();
	/*for (int i = 0; i < SeqPar.GetNumberOfIterationsVer1(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{

			Faceparametr[i].SetParametr(GetRT[j]);
			Faceparametr[i].SetNameParametr("Parametr1");
			//std::cout << "TestNumber[" << j << "]:" << GetRT[j].GetTestNumber() << "\n";
			//std::cout << "ResultVer1[" << j << "]:" << GetRT[j].GetResultTest() << "\n";
		}
		faceVer1[i].SetFaceParameters(Faceparametr);
		faceVer1[i].SetFaceName("Face1");
	}

	std::cout << "===============================================" << std::endl;
	// Experiment2
	GetRI = experiment2.GetResultsExperiment();
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer2(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			Faceparametr[i].SetParametr(GetRT[j]);
			Faceparametr[i].SetNameParametr("Parametr2");
			std::cout << "TestNumber[" << j << "]:" << GetRT[j].GetTestNumber() << "\n";
			std::cout << "ResultVer2[" << j << "]:" << GetRT[j].GetResultTest() << "\n";
		}
		faceVer2[i].SetFaceParameters(Faceparametr);
		faceVer2[i].SetFaceName("Face2");
	}*/

	EXPECT_EQ(control->ReportGeneration(&SeqPar, &experiment1, &experiment2, faceVer1, faceVer2), 0);

	delete[] faceVer1;
	delete[] faceVer2;
}

// ���� 1.16 �������� �������� ���������� ������������������ �� ��������
TEST(TestStub, SaveDataSequenceParameters) {
	//��������� seqparameters
	BO_SequenceParameters Seq("C:\\ABC", 50, 10, 10, 2);
	BC_Stub * stub = new BC_Stub;// ���������� ��������
	EXPECT_EQ(stub->SaveSeqParameters(&Seq), 0);
}

// ���� 1.17 �������� �������� ���������� ������������������ �� ��������
TEST(TestStub, LoadDataSequenceParameters) {
	//�������� ����������� SequenceParameters
	BO_SequenceParameters Seq;
	BC_Stub * stub = new BC_Stub;// ���������� ��������
	stub->LoadSeqParameters(&Seq);
	EXPECT_GT(strlen(Seq.GetFileName()), 0);
	EXPECT_GT(Seq.GetLengthSequence(), 0);
	EXPECT_GT(Seq.GetNumberOfIterationsVer1(), 0);
	EXPECT_GT(Seq.GetNumberOfIterationsVer2(), 0);
	EXPECT_GT(Seq.GetStep(), 0);
}

// ���� 1.18 �������� �������� ����������� ������������� �� ��������
TEST(TestStub, LoadDataResultsExperiment) {
	BO_ResultsTest RTforRI[15];
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\tmp2", 4, 0, 0, 3);
	BC_ControlComponent * control = new BC_ControlComponent;
	control->ValidationOfParameters(&SeqPar);
	SeqPar.GetNumberOfIterationsVer1();
	SeqPar.GetNumberOfIterationsVer2();

	//��������� ������ ���� ResultsTest
	for (int i = 0; i < 15; i++)
	{
		RTforRI[i].SetResultTest(0.00);
		RTforRI[i].SetTestNumber(1);
	}

	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer1()];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer2()];

	//��������� ������ ���� ResultsIteration
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer1(); i++)
	{
		resultsiterationVer1[i].SetResultIteration(RTforRI);
	}

	//��������� ������ ���� ResultsIteration
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer2(); i++)
	{
		resultsiterationVer2[i].SetResultIteration(RTforRI);
	}

	BO_ResultsExperiment experiment1(resultsiterationVer1, SeqPar.GetNumberOfIterationsVer1());
	BO_ResultsExperiment experiment2(resultsiterationVer2, SeqPar.GetNumberOfIterationsVer1());
	// ���������� ������������
	BO_ResultsExperiment *GetRE1, *GetRE2;
	BO_ResultsIteration *GetRI;
	BO_ResultsTest *GetRT;

	BC_Stub * stub = new BC_Stub;// ���������� ��������
	stub->LoadData(&experiment1, &experiment2, &SeqPar);

	// Experiment1
	GetRI = experiment1.GetResultsExperiment();
	GetRT = GetRI[0].GetResultIteration();
	EXPECT_GT(GetRT[0].GetTestNumber(), 0);
	EXPECT_GT(GetRT[0].GetResultTest(), 0);
	// Experiment2
	GetRI = experiment2.GetResultsExperiment();
	GetRT = GetRI[0].GetResultIteration();
	EXPECT_GT(GetRT[0].GetTestNumber(), 0);
	EXPECT_GT(GetRT[0].GetResultTest(), 0);

}

// ���� 1.19 �������� �������� ���������� ��� ���������� ��� �� ��������
TEST(TestStub, LoadDataFaces) {
	BO_Face faceVer1;
	BO_Face faceVer2;

	BC_Stub * stub = new BC_Stub;// ���������� ��������
	stub->SaveDataFace(&faceVer1, &faceVer2);
	EXPECT_EQ(stub->LoadDataFace(&faceVer1, &faceVer2), 0);
	
	//��������� face
	BO_ResultsTest RTforFaceParametr(0, 0.00); // ResultTest for Faceparametr
	BO_FaceParametr Faceparametr[15];// ������ FaceParametr for Face
	BO_FaceParametr * GetFP;
	// ���������� ����
	EXPECT_GT(strlen(faceVer1.GetFaceName()), 0);
	GetFP = faceVer1.GetFaceParameters();
	RTforFaceParametr = GetFP[0].GetParametr();

	EXPECT_GT(RTforFaceParametr.GetTestNumber(), 0);
	EXPECT_GT(RTforFaceParametr.GetResultTest(), 0);

	EXPECT_GT(strlen(GetFP[0].GetNameParametr()), 0);
}

// ���� 1.20 �������� ���������� ����� � ��������
TEST(TestStub, SaveDataReport) {
	BO_ResultsTest RTforRI[15];

	int sizearray = 10;

	//��������� ������ ���� ResultsTest
	for (int i = 0; i < 15; i++)
	{
		RTforRI[i].SetResultTest(-10.55);
		RTforRI[i].SetTestNumber(-5);
	}

	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizearray];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[sizearray];

	//��������� ������ ���� ResultsIteration
	for (int i = 0; i < sizearray; i++)
	{
		resultsiterationVer1[i].SetResultIteration(RTforRI);
		resultsiterationVer2[i].SetResultIteration(RTforRI);
	}

	BO_ResultsExperiment experiment1(resultsiterationVer1, sizearray);
	BO_ResultsExperiment experiment2(resultsiterationVer2, sizearray);
	//��������� seqparameters
	BO_SequenceParameters Seq("", -50, -10, -10, -2);

	//��������� face
	BO_ResultsTest RTforFaceParametr(-2, -2.55); // ResultTest for Faceparametr
	BO_FaceParametr Faceparametr[15];// ������ FaceParametr for Face
	BO_FaceParametr * GetFP;

	// ��������� ������  �������� Faceparametr 
	for (int i = 0; i < 15; i++) {
		Faceparametr[i].SetParametr(RTforFaceParametr);
		Faceparametr[i].SetNameParametr("");
	}
	BO_Face face("", Faceparametr);
	BO_Report report("", -5, &Seq, &experiment1, &experiment2, &face, &face);

	BC_Stub * stub = new BC_Stub;// ���������� ��������
	EXPECT_EQ(stub->SaveDataReport(&report), 0);
}

// ���� 1.21 �������� �������� ������ �� ��������
TEST(TestStub, LoadDataReport) {
	BO_ResultsTest RTforRI[15];

	int sizearray = 10;

	//��������� ������ ���� ResultsTest
	for (int i = 0; i < 15; i++)
	{
		RTforRI[i].SetResultTest(10.55);
		RTforRI[i].SetTestNumber(5);
	}

	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizearray];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[sizearray];

	//��������� ������ ���� ResultsIteration
	for (int i = 0; i < sizearray; i++)
	{
		resultsiterationVer1[i].SetResultIteration(RTforRI);
		resultsiterationVer2[i].SetResultIteration(RTforRI);
	}

	BO_ResultsExperiment experiment1(resultsiterationVer1, sizearray);
	BO_ResultsExperiment experiment2(resultsiterationVer2, sizearray);
	//��������� seqparameters
	BO_SequenceParameters Seq("C:\\ABC", 50, 10, 10, 2);

	//��������� face
	BO_ResultsTest RTforFaceParametr(2, 2.55); // ResultTest for Faceparametr
	BO_FaceParametr Faceparametr[15];// ������ FaceParametr for Face
	BO_FaceParametr * GetFP;

	// ��������� ������  �������� Faceparametr 
	for (int i = 0; i < 15; i++) {
		Faceparametr[i].SetParametr(RTforFaceParametr);
		Faceparametr[i].SetNameParametr("Par");
	}
	BO_Face face("face1", Faceparametr);
	BO_Report report("asd", 5, &Seq, &experiment1, &experiment2, &face, &face);
	report.GetHushSum();

	BC_Stub * stub = new BC_Stub;// ���������� ��������
	EXPECT_EQ(stub->LoadDataReport(&report), 0);

	// �������� ����� ������
	EXPECT_GT(strlen(report.GetReportName()), 0);

	// ���������� ����
	EXPECT_GT(strlen(face.GetFaceName()), 0);
	GetFP = face.GetFaceParameters();
	RTforFaceParametr = GetFP[0].GetParametr();

	EXPECT_GT(RTforFaceParametr.GetTestNumber(), 0);
	EXPECT_GT(RTforFaceParametr.GetResultTest(), 0);

	EXPECT_GT(strlen(GetFP[0].GetNameParametr()), 0);
	// �������� ���-�����
	EXPECT_GT(report.GetHushSum(), 0);
	// ���������� ������������
	BO_ResultsExperiment *GetRE1, *GetRE2;
	BO_ResultsIteration *GetRI;
	BO_ResultsTest *GetRT;
	// Experiment1
	GetRE1 = report.GetResultSequenceVer1();
	GetRI = GetRE1[0].GetResultsExperiment();
	GetRT = GetRI[0].GetResultIteration();
	EXPECT_GT(GetRT[0].GetTestNumber(), 0);
	EXPECT_GT(GetRT[0].GetResultTest(), 0);
	// Experiment2
	GetRE2 = report.GetResultSequenceVer2();
	GetRI = GetRE2[0].GetResultsExperiment();
	GetRT = GetRI[0].GetResultIteration();
	EXPECT_GT(GetRT[0].GetTestNumber(), 0);
	EXPECT_GT(GetRT[0].GetResultTest(), 0);

	//�������� ����������� SequenceParameters
	EXPECT_GT(strlen(Seq.GetFileName()), 0);
	EXPECT_GT(Seq.GetLengthSequence(), 0);
	EXPECT_GT(Seq.GetNumberOfIterationsVer1(), 0);
	EXPECT_GT(Seq.GetNumberOfIterationsVer2(), 0);
	EXPECT_GT(Seq.GetStep(), 0);
}