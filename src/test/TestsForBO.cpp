#include "gtest/gtest.h"
#include "../chernofffaces/BO_ResultsTest.h"
#include "../chernofffaces/BO_ResultsIteration.h"
#include "../chernofffaces/BO_ResultsExperiment.h"
#include "../chernofffaces/BO_SequenceParameters.h"
#include "../chernofffaces/BO_FaceParametr.h"
#include "../chernofffaces/BO_Face.h"
#include "../chernofffaces/BO_Report.h"
#include <iostream>
#include <string>

	// |���� 1| ������ ResultsTest
	// ���� 1.1 ������ �����������
	TEST(TestResultsTest, ConstructorIsEmpty) {
		BO_ResultsTest RT;
		EXPECT_EQ(RT.GetResultTest(), 0); // EXPECT_EQ: val1 == val2
		EXPECT_EQ(RT.GetTestNumber(), 0);
	}

	// ���� 1.2 ��������� �����������
	TEST(TestResultsTest, ConstructorIsNotEmpty) {
		BO_ResultsTest RT(0, 00.00);
		EXPECT_GE(RT.GetResultTest(), 0); // EXPECT_GT: val1 >= val2
		EXPECT_GE(RT.GetTestNumber(), 0);

		BO_ResultsTest RT1(1, 25.43);
		EXPECT_GE(RT1.GetResultTest(), 0); // EXPECT_GT: val1 >= val2
		EXPECT_GE(RT1.GetTestNumber(), 0);
	}

	// ���� 1.3 �������� ������������� �������� ������������, ��� �������������� ��������� ���������� ����� ������
	TEST(TestResultsTest, ConstructorIsNegative) {
		BO_ResultsTest RT(-1, -25.43);
		EXPECT_EQ(RT.GetResultTest(), 0);
		EXPECT_EQ(RT.GetTestNumber(), 0);
	}

	// ���� 1.4 �������� SetResultTest  �� ������������� ����� (����������)
	TEST(TestResultsTest, SetResultTestIsNegative) {
		BO_ResultsTest RT;
		RT.SetResultTest(-1.24);
		EXPECT_EQ(RT.GetResultTest(), 0);
	}

	// ���� 1.5 �������� SetResultTest �� ������� ��������
	TEST(TestResultsTest, SetResultTestIsEmpty) {
		BO_ResultsTest RT;
		RT.SetResultTest(0.00);
		EXPECT_EQ(RT.GetResultTest(), 0);
	}

	// ���� 1.6 �������� SetResultTest �� ����������
	TEST(TestResultsTest, SetResultTestANDTestNumberIsNotEmpty) {
		BO_ResultsTest RT;
		RT.SetResultTest(5.50);
		EXPECT_GT(RT.GetResultTest(), 0);
	}

	// ���� 1.7 �������� SetTestNumber �� ������������� ����� (����������)
	TEST(TestResultsTest, SetTestNumberIsNegative) {
		BO_ResultsTest RT;
		RT.SetTestNumber(-5);
		EXPECT_EQ(RT.GetTestNumber(), 0);
	}

	// ���� 1.8 �������� SetTestNumber �� ������� ��������
	TEST(TestResultsTest, SetTestNumberIsEmpty) {
		BO_ResultsTest RT;
		RT.SetTestNumber(0);
		EXPECT_EQ(RT.GetTestNumber(), 0);
	}

	// ���� 1.9 �������� SetTestNumber �� ����������
	TEST(TestResultsTest, SetTestNumberIsNotEmpty) {
		BO_ResultsTest RT;
		RT.SetTestNumber(4);
		EXPECT_GT(RT.GetTestNumber(), 0);
	}
	// |���� 2| ������ ResultsIteration
	// ���� 2.1 ������ ����������� �� ��������� (�������� ResultTest, TestNumber)
	TEST(TestResultsIteration, ConstructorIsEmpty) {
		BO_ResultsIteration RI;
		BO_ResultsTest * RTforRI;

		RTforRI = RI.GetResultIteration();
		for (int i = 0; i < 15; i++)
		{
			EXPECT_EQ(RTforRI[i].GetResultTest(), 0); // EXPECT_EQ: val1 == val2
			EXPECT_EQ(RTforRI[i].GetTestNumber(), 0);
		}
	}

	// ���� 2.2 ��������� �����������
	TEST(TestResultsIteration, ConstructorIsNotEmpty) {
		BO_ResultsTest RTforRI[15]; // �������������� ������ ����������� ����� ��� ResultsIteration
		BO_ResultsTest * GetRT;

		//��������� ������ ��� ������������ � �����������
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(i);
			RTforRI[i].SetTestNumber(i);
		}
		
		BO_ResultsIteration RI(RTforRI); // �������� ����������� � ����������� ������ ResultsIteration
		GetRT = RI.GetResultIteration();
		//��������� ��� �� ������ ������������� �����
		for (int i = 0; i < 15; i++)
		{
			EXPECT_GE(GetRT[i].GetResultTest(), 0); // EXPECT_GE: val1 >= val2
			EXPECT_GE(GetRT[i].GetTestNumber(), 0);
		}
	}

	// ���� 2.3 �������� ������������� �������� ������������ (����������)
	TEST(TestResultsIteration, ConstructorIsNegative) {
		BO_ResultsTest RTforRI[15]; // �������������� ������ ����������� ����� ��� ResultsIteration
		BO_ResultsTest * GetRT;

		//��������� ������ ��� ������������ � �����������
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(-1);
			RTforRI[i].SetTestNumber(-1);
		}

		BO_ResultsIteration RI(RTforRI); // �������� ����������� � ����������� ������ ResultsIteration
		GetRT = RI.GetResultIteration();
		//���������
		for (int i = 0; i < 15; i++)
		{
			EXPECT_EQ(GetRT[i].GetResultTest(), 0); // EXPECT_EQ: val1 = val2
			EXPECT_EQ(GetRT[i].GetTestNumber(), 0);
		}
	}

	// ���� 2.4 �������� SetResultsIteration �� ������������� �����
	TEST(TestResultsIteration, SetResultsIterationIsNegative) {
		BO_ResultsTest RTforRI[15]; // �������������� ������ ����������� ����� ��� ResultsIteration
		BO_ResultsTest * GetRT;

		//��������� ������
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(-1);
			RTforRI[i].SetTestNumber(-1);
		}

		BO_ResultsIteration RI; // �������� ����������� � ����������� ������ ResultsIteration
		RI.SetResultIteration(RTforRI);
		GetRT = RI.GetResultIteration();
		//��������� ��� �� ������ ������������� �����
		for (int i = 0; i < 15; i++)
		{
			EXPECT_EQ(GetRT[i].GetResultTest(), 0); // EXPECT_EQ: val1 = val2
			EXPECT_EQ(GetRT[i].GetTestNumber(), 0);
		}
	}

	// ���� 2.5 �������� SetResultsIteration �� ����������
	TEST(TestResultsIteration, SetResultsIterationIsNotEmpty) {
		BO_ResultsTest RTforRI[15]; // �������������� ������ ����������� ����� ��� ResultsIteration
		BO_ResultsTest * GetRT;

		//��������� ������
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(5.55);
			RTforRI[i].SetTestNumber(2);
		}

		BO_ResultsIteration RI; // �������� ����������� � ����������� ������ ResultsIteration
		RI.SetResultIteration(RTforRI);
		GetRT = RI.GetResultIteration();
		//���������
		for (int i = 0; i < 15; i++)
		{
			EXPECT_GT(GetRT[i].GetResultTest(), 0); // EXPECT_EQ: val1 = val2
			EXPECT_GT(GetRT[i].GetTestNumber(), 0);
		}
	}

	// ���� 2.6 �������� SetResultsIteration �� ������� ��������
	TEST(TestResultsIteration, SetResultsIterationIsEmpty) {
		BO_ResultsTest RTforRI[15]; // �������������� ������ ����������� ����� ��� ResultsIteration
		BO_ResultsTest * GetRT;

		//��������� ������
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(0.00);
			RTforRI[i].SetTestNumber(0);
		}

		BO_ResultsIteration RI; // �������� ����������� � ����������� ������ ResultsIteration
		RI.SetResultIteration(RTforRI);
		GetRT = RI.GetResultIteration();
		//���������
		for (int i = 0; i < 15; i++)
		{
			EXPECT_EQ(GetRT[i].GetResultTest(), 0); // EXPECT_EQ: val1 = val2
			EXPECT_EQ(GetRT[i].GetTestNumber(), 0);
		}
	}
	// |���� 3| ������ ResultsExperiment
	// ���� 3.1 ������ �����������
	TEST(TestResultsExperiment, ConstructorIsEmpty) {
		int sizearray = 10;

		BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizearray];

		BO_ResultsExperiment RE(resultsiterationVer1, sizearray); // �������� ����������� � �����������, ��� ������������ �� ��������� �.�. ������������ ������
		BO_ResultsIteration  * RIforRE; // ��������� ���� ResultsIteration ��� ��������� �������
		RIforRE = RE.GetResultsExperiment(); // �������� ��������� �� ������ ResultsExperiment

		BO_ResultsTest * GetRI;

		for (int j = 0; j < sizearray; j++)
		{
			GetRI = RIforRE[j].GetResultIteration(); // �������� ��������� �� ������ ResultIteration
			for (int i = 0; i < 15; i++)
			{
				EXPECT_EQ(GetRI[i].GetResultTest(), 0); // EXPECT_EQ: val1 = val2
				EXPECT_EQ(GetRI[i].GetTestNumber(), 0);
			}
		}
	}

	// ���� 3.2 ��������� �����������
	TEST(TestResultsExperiment, ConstructorIsNotEmpty) {
		BO_ResultsTest RTforRI[15];

		int sizearray = 10;

		//��������� ������ ResultsIteration
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(i);
			RTforRI[i].SetTestNumber(i);
		}

		BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizearray];

		for (int i = 0; i < sizearray; i++)
		{
			resultsiterationVer1[i].SetResultIteration(RTforRI);
		}

		BO_ResultsExperiment RE(resultsiterationVer1, sizearray); // �������� ����������� � �����������, ��� ������������ �� ��������� �.�. ������������ ������
		BO_ResultsIteration  * RIforRE; // ��������� ���� ResultsIteration ��� ��������� �������
		RIforRE = RE.GetResultsExperiment(); // �������� ��������� �� ������ ResultsExperiment

		BO_ResultsTest * GetRI;

		for (int j = 0; j < sizearray; j++)
		{
			GetRI = RIforRE[j].GetResultIteration(); // �������� ��������� �� ������ ResultIteration
			for (int i = 0; i < 15; i++)
			{
				EXPECT_GE(GetRI[i].GetResultTest(), 0); // EXPECT_GE: val1 >= val2
				EXPECT_GE(GetRI[i].GetTestNumber(), 0);
			}
		}
	}

	// ���� 3.3 �������� ������������� �������� ������������ (����������)
	TEST(TestResultsExperiment, ConstructorIsNegative) {
		BO_ResultsTest RTforRI[15];

		int sizearray = 10;

		//��������� ������ ���� ResultsTest
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(-1);
			RTforRI[i].SetTestNumber(-1);
		}

		BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizearray];

		//��������� ������ ���� ResultsIteration
		for (int i = 0; i < sizearray; i++)
		{
			resultsiterationVer1[i].SetResultIteration(RTforRI);
		}

		BO_ResultsExperiment RE(resultsiterationVer1, sizearray); // �������� ����������� � �����������, ��� ������������ �� ��������� �.�. ������������ ������
		BO_ResultsIteration  * RIforRE; // ��������� ���� ResultsIteration ��� ��������� �������
		RIforRE = RE.GetResultsExperiment(); // �������� ��������� �� ������ ResultsExperiment

		BO_ResultsTest * GetRI;

		for (int j = 0; j < sizearray; j++)
		{
			GetRI = RIforRE[j].GetResultIteration(); // �������� ��������� �� ������ ResultIteration
			for (int i = 0; i < 15; i++)
			{
				EXPECT_EQ(GetRI[i].GetResultTest(), 0); // EXPECT_EQ: val1 = val2
				EXPECT_EQ(GetRI[i].GetTestNumber(), 0);
			}
		}
	}

	// ���� 3.4 �������� Set �� ������������� ��������(����������)
	TEST(TestResultsExperiment, SetIsNegative) {
		BO_ResultsTest RTforRI[15];

		int sizearray = 10;

		//��������� ������ ���� ResultsTest
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(-1);
			RTforRI[i].SetTestNumber(-1);
		}

		BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizearray];

		//��������� ������ ���� ResultsIteration
		for (int i = 0; i < sizearray; i++)
		{
			resultsiterationVer1[i].SetResultIteration(RTforRI);
		}

		BO_ResultsExperiment RE(resultsiterationVer1, sizearray); // �������� ����������� � �����������, ��� ������������ �� ��������� �.�. ������������ ������
		RE.SetResultsExperiment(resultsiterationVer1, sizearray);
		BO_ResultsIteration  * RIforRE; // ��������� ���� ResultsIteration ��� ��������� �������
		RIforRE = RE.GetResultsExperiment(); // �������� ��������� �� ������ ResultsExperiment

		BO_ResultsTest * GetRI;

		for (int j = 0; j < sizearray; j++)
		{
			GetRI = RIforRE[j].GetResultIteration(); // �������� ��������� �� ������ ResultIteration
			for (int i = 0; i < 15; i++)
			{
				EXPECT_EQ(GetRI[i].GetResultTest(), 0); // EXPECT_EQ: val1 = val2
				EXPECT_EQ(GetRI[i].GetTestNumber(), 0);
			}
		}
	}

	// ���� 3.5 �������� Set �� ���������� 
	TEST(TestResultsExperiment, SetIsNotEmpty) {
		BO_ResultsTest RTforRI[15];

		int sizearray = 10;

		//��������� ������ ���� ResultsTest
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(5.55);
			RTforRI[i].SetTestNumber(7);
		}

		BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizearray];

		//��������� ������ ���� ResultsIteration
		for (int i = 0; i < sizearray; i++)
		{
			resultsiterationVer1[i].SetResultIteration(RTforRI);
		}

		BO_ResultsExperiment RE(resultsiterationVer1, sizearray); // �������� ����������� � �����������, ��� ������������ �� ��������� �.�. ������������ ������
		RE.SetResultsExperiment(resultsiterationVer1, sizearray);
		BO_ResultsIteration  * RIforRE; // ��������� ���� ResultsIteration ��� ��������� �������
		RIforRE = RE.GetResultsExperiment(); // �������� ��������� �� ������ ResultsExperiment

		BO_ResultsTest * GetRI;

		for (int j = 0; j < sizearray; j++)
		{
			GetRI = RIforRE[j].GetResultIteration(); // �������� ��������� �� ������ ResultIteration
			for (int i = 0; i < 15; i++)
			{
				EXPECT_GT(GetRI[i].GetResultTest(), 0); // EXPECT_EQ: val1 = val2
				EXPECT_GT(GetRI[i].GetTestNumber(), 0);
			}
		}
	}

	// ���� 3.6 �������� Set �� ������� �������� 
	TEST(TestResultsExperiment, SetIsEmpty) {
		BO_ResultsTest RTforRI[15];

		int sizearray = 10;

		//��������� ������ ���� ResultsTest
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(0.00);
			RTforRI[i].SetTestNumber(0);
		}

		BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizearray];

		//��������� ������ ���� ResultsIteration
		for (int i = 0; i < sizearray; i++)
		{
			resultsiterationVer1[i].SetResultIteration(RTforRI);
		}

		BO_ResultsExperiment RE(resultsiterationVer1, sizearray); // �������� ����������� � �����������, ��� ������������ �� ��������� �.�. ������������ ������
		RE.SetResultsExperiment(resultsiterationVer1, sizearray);
		BO_ResultsIteration  * RIforRE; // ��������� ���� ResultsIteration ��� ��������� �������
		RIforRE = RE.GetResultsExperiment(); // �������� ��������� �� ������ ResultsExperiment

		BO_ResultsTest * GetRI;

		for (int j = 0; j < sizearray; j++)
		{
			GetRI = RIforRE[j].GetResultIteration(); // �������� ��������� �� ������ ResultIteration
			for (int i = 0; i < 15; i++)
			{
				EXPECT_EQ(GetRI[i].GetResultTest(), 0); // EXPECT_EQ: val1 = val2
				EXPECT_EQ(GetRI[i].GetTestNumber(), 0);
			}
		}
	}

	// |���� 4| ������ Report
	// ���� 4.1 ���������� ������������
	TEST(TestReport, ConstructorIsNotEmpty) {
		BO_ResultsTest RTforRI[15];

		int sizearray = 10;

		//��������� ������ ���� ResultsTest
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(10.55);
			RTforRI[i].SetTestNumber(5);
		}

		BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizearray];
		BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[sizearray];

		//��������� ������ ���� ResultsIteration
		for (int i = 0; i < sizearray; i++)
		{
			resultsiterationVer1[i].SetResultIteration(RTforRI);
			resultsiterationVer2[i].SetResultIteration(RTforRI);
		}

		BO_ResultsExperiment experiment1(resultsiterationVer1, sizearray);
		BO_ResultsExperiment experiment2(resultsiterationVer2, sizearray);
		//��������� seqparameters
		BO_SequenceParameters Seq("C:\\ABC", 50, 10, 10, 2);

		//��������� face
		BO_ResultsTest RTforFaceParametr(2, 2.55); // ResultTest for Faceparametr
		BO_FaceParametr Faceparametr[15];// ������ FaceParametr for Face
		BO_FaceParametr * GetFP;

		// ��������� ������  �������� Faceparametr 
		for (int i = 0; i < 15; i++) {
			Faceparametr[i].SetParametr(RTforFaceParametr);
			Faceparametr[i].SetNameParametr("Par");
		}
		BO_Face face("face1", Faceparametr);
		BO_Report report("asd", 5, &Seq, &experiment1, &experiment2, &face, &face);
		report.GetHushSum();

		// �������� ����� ������
		EXPECT_GT(strlen(report.GetReportName()),0);

		// ���������� ����
		EXPECT_GT(strlen(face.GetFaceName()), 0);
		GetFP = face.GetFaceParameters();
		RTforFaceParametr = GetFP[0].GetParametr();

		EXPECT_GT(RTforFaceParametr.GetTestNumber(), 0);
		EXPECT_GT(RTforFaceParametr.GetResultTest(), 0);

		EXPECT_GT(strlen(GetFP[0].GetNameParametr()), 0);
		// �������� ���-�����
		EXPECT_GT(report.GetHushSum(), 0);
		// ���������� ������������
		BO_ResultsExperiment *GetRE1, *GetRE2;
		BO_ResultsIteration *GetRI;
		BO_ResultsTest *GetRT;
		// Experiment1
		GetRE1 = report.GetResultSequenceVer1();
		GetRI=GetRE1[0].GetResultsExperiment();
		GetRT=GetRI[0].GetResultIteration();
		EXPECT_GT(GetRT[0].GetTestNumber(), 0);
		EXPECT_GT(GetRT[0].GetResultTest(), 0);
		// Experiment2
		GetRE2 = report.GetResultSequenceVer2();
		GetRI = GetRE2[0].GetResultsExperiment();
		GetRT = GetRI[0].GetResultIteration();
		EXPECT_GT(GetRT[0].GetTestNumber(), 0);
		EXPECT_GT(GetRT[0].GetResultTest(), 0);

		//�������� ����������� SequenceParameters
		EXPECT_GT(strlen(Seq.GetFileName()), 0);
		EXPECT_GT(Seq.GetLengthSequence(), 0);
		EXPECT_GT(Seq.GetNumberOfIterationsVer1(), 0);
		EXPECT_GT(Seq.GetNumberOfIterationsVer2(), 0);
		EXPECT_GT(Seq.GetStep(), 0);
	}

	// ���� 4.2 �������� �� ������������� ��������
	TEST(TestReport, ConstructorIsNegative) {
		BO_ResultsTest RTforRI[15];

		int sizearray = 10;

		//��������� ������ ���� ResultsTest
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(-10.55);
			RTforRI[i].SetTestNumber(-5);
		}

		BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizearray];
		BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[sizearray];

		//��������� ������ ���� ResultsIteration
		for (int i = 0; i < sizearray; i++)
		{
			resultsiterationVer1[i].SetResultIteration(RTforRI);
			resultsiterationVer2[i].SetResultIteration(RTforRI);
		}

		BO_ResultsExperiment experiment1(resultsiterationVer1, sizearray);
		BO_ResultsExperiment experiment2(resultsiterationVer2, sizearray);
		//��������� seqparameters
		BO_SequenceParameters Seq("", -50, -10, -10, -2);

		//��������� face
		BO_ResultsTest RTforFaceParametr(-2, -2.55); // ResultTest for Faceparametr
		BO_FaceParametr Faceparametr[15];// ������ FaceParametr for Face
		BO_FaceParametr * GetFP;

		// ��������� ������  �������� Faceparametr 
		for (int i = 0; i < 15; i++) {
			Faceparametr[i].SetParametr(RTforFaceParametr);
			Faceparametr[i].SetNameParametr("");
		}
		BO_Face face("", Faceparametr);
		BO_Report report("", -5, &Seq, &experiment1, &experiment2, &face, &face);
		
		// �������� ����� ������
		EXPECT_EQ(strlen(report.GetReportName()), 0);

		// ���������� ����
		EXPECT_EQ(strlen(face.GetFaceName()), 0);
		GetFP = face.GetFaceParameters();
		RTforFaceParametr = GetFP[0].GetParametr();

		EXPECT_EQ(RTforFaceParametr.GetTestNumber(), 0);
		EXPECT_EQ(RTforFaceParametr.GetResultTest(), 0);

		EXPECT_EQ(strlen(GetFP[0].GetNameParametr()), 0);
		// �������� ���-�����
		EXPECT_EQ(report.GetHushSum(), 0);
		// ���������� ������������
		BO_ResultsExperiment *GetRE1, *GetRE2;
		BO_ResultsIteration *GetRI;
		BO_ResultsTest *GetRT;
		// Experiment1
		GetRE1 = report.GetResultSequenceVer1();
		GetRI = GetRE1[0].GetResultsExperiment();
		GetRT = GetRI[0].GetResultIteration();
		EXPECT_EQ(GetRT[0].GetTestNumber(), 0);
		EXPECT_EQ(GetRT[0].GetResultTest(), 0);
		// Experiment2
		GetRE2 = report.GetResultSequenceVer2();
		GetRI = GetRE2[0].GetResultsExperiment();
		GetRT = GetRI[0].GetResultIteration();
		EXPECT_EQ(GetRT[0].GetTestNumber(), 0);
		EXPECT_EQ(GetRT[0].GetResultTest(), 0);

		//�������� ����������� SequenceParameters
		EXPECT_EQ(strlen(Seq.GetFileName()), 0);
		EXPECT_EQ(Seq.GetLengthSequence(), 0);
		EXPECT_EQ(Seq.GetNumberOfIterationsVer1(), 0);
		EXPECT_EQ(Seq.GetNumberOfIterationsVer2(), 0);
		EXPECT_EQ(Seq.GetStep(), 0);
	}

	// ���� 4.3 ���������� ������������ ��� Report ������
	TEST(TestReport, ConstructorReportIsEmpty) {
		BO_ResultsTest RTforRI[15];

		int sizearray = 10;

		//��������� ������ ���� ResultsTest
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(-1);
			RTforRI[i].SetTestNumber(-1);
		}

		BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizearray];
		BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[sizearray];

		//��������� ������ ���� ResultsIteration
		for (int i = 0; i < sizearray; i++)
		{
			resultsiterationVer1[i].SetResultIteration(RTforRI);
			resultsiterationVer2[i].SetResultIteration(RTforRI);
		}

		BO_ResultsExperiment experiment1(resultsiterationVer1, sizearray);
		BO_ResultsExperiment experiment2(resultsiterationVer2, sizearray);
		//��������� seqparameters
		BO_SequenceParameters Seq("C:\\ABC", 50, 10, 10, 2);

		//��������� face
		BO_ResultsTest RTforFaceParametr(2, 2.55); // ResultTest for Faceparametr
		BO_FaceParametr Faceparametr[15];// ������ FaceParametr for Face
		BO_FaceParametr * GetFP;

		// ��������� ������  �������� Faceparametr 
		for (int i = 0; i < 15; i++) {
			Faceparametr[i].SetParametr(RTforFaceParametr);
			Faceparametr[i].SetNameParametr("Par");
		}
		BO_Face face("face1", Faceparametr);
		BO_Report report("", 5, &Seq, &experiment1, &experiment2, &face, &face);
		report.GetHushSum();

		// ���������� ����
		EXPECT_EQ(strlen(report.GetReportName()), 0); 
	}

	// ���� 4.4 ���������� ������������ ��� Report �� ������
	TEST(TestReport, ConstructorReportIsNotEmpty) {
		BO_ResultsTest RTforRI[15];

		int sizearray = 10;

		//��������� ������ ���� ResultsTest
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(10.57);
			RTforRI[i].SetTestNumber(23);
		}

		BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizearray];
		BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[sizearray];

		//��������� ������ ���� ResultsIteration
		for (int i = 0; i < sizearray; i++)
		{
			resultsiterationVer1[i].SetResultIteration(RTforRI);
			resultsiterationVer2[i].SetResultIteration(RTforRI);
		}

		BO_ResultsExperiment experiment1(resultsiterationVer1, sizearray);
		BO_ResultsExperiment experiment2(resultsiterationVer2, sizearray);
		//��������� seqparameters
		BO_SequenceParameters Seq("C:\\ABC", 50, 10, 10, 2);

		//��������� face
		BO_ResultsTest RTforFaceParametr(2, 2.55); // ResultTest for Faceparametr
		BO_FaceParametr Faceparametr[15];// ������ FaceParametr for Face
		BO_FaceParametr * GetFP;

		// ��������� ������  �������� Faceparametr 
		for (int i = 0; i < 15; i++) {
			Faceparametr[i].SetParametr(RTforFaceParametr);
			Faceparametr[i].SetNameParametr("Par");
		}
		BO_Face face("face1", Faceparametr);
		BO_Report report("Report1", 5, &Seq, &experiment1, &experiment2, &face, &face);
		report.GetHushSum();

		// ���������� ����
		EXPECT_GT(strlen(report.GetReportName()), 0);
	}

	// ���� 4.5 �������� SetReport �� ����������
	TEST(TestReport, SetReportIsNotEmpty) {
	
		BO_SequenceParameters Seq;
		BO_Report report;
		report.SetReportName("Report1");
		EXPECT_GT(strlen(report.GetReportName()), 0);
	}

	// ���� 4.6 �������� SetReport �� ������� �����
	TEST(TestReport, SetReportIsEmpty) {

		BO_SequenceParameters Seq;
		BO_Report report;
		report.SetReportName("");
		EXPECT_EQ(strlen(report.GetReportName()), 0);
	}


	// ���� 4.7 �������� �� ������������� �������� ���-�����, ��� ������������� ��������� ���� ���������
	TEST(TestReport, SethushsumIsNegative) {
		BO_Report report;
		report.SetHushSum(-1);
		EXPECT_EQ(report.GetHushSum(), 0);
	}

	// ���� 4.8 �������� �� ������������� �������� ���-�����
	TEST(TestReport, SethushsumIsNotNegative) {
		BO_Report report;
		report.SetHushSum(5);
		EXPECT_GE(report.GetHushSum(), 0);
		report.SetHushSum(0);
		EXPECT_GE(report.GetHushSum(), 0);
	}

	// ���� 4.9 �������� SetParameters ����������
	TEST(TestReport, SetParametersIsNotEmpty) {
		//��������� seqparameters
		BO_SequenceParameters Seq("C:\\ABC", 50, 10, 10, 2);

		BO_Report report;
		report.SetParameters(&Seq);
		//�������� ����������� SequenceParameters
		EXPECT_GT(strlen(Seq.GetFileName()), 0);
		EXPECT_GT(Seq.GetLengthSequence(), 0);
		EXPECT_GT(Seq.GetNumberOfIterationsVer1(), 0);
		EXPECT_GT(Seq.GetNumberOfIterationsVer2(), 0);
		EXPECT_GT(Seq.GetStep(), 0);
	}

	// ���� 4.10 �������� SetParameters �� ������� ��������
	TEST(TestReport, SetParametersIsEmpty) {
		//��������� seqparameters
		BO_SequenceParameters Seq("", 0, 0, 0, 0);

		BO_Report report;
		report.SetParameters(&Seq);
		//�������� ����������� SequenceParameters
		EXPECT_EQ(strlen(Seq.GetFileName()), 0);
		EXPECT_EQ(Seq.GetLengthSequence(), 0);
		EXPECT_EQ(Seq.GetNumberOfIterationsVer1(), 0);
		EXPECT_EQ(Seq.GetNumberOfIterationsVer2(), 0);
		EXPECT_EQ(Seq.GetStep(), 0);
	}

	// ���� 4.11 �������� SetParameters �� ������������� ��������
	TEST(TestReport, SetParametersIsNegative) {
		//��������� seqparameters
		BO_SequenceParameters Seq("", -5, -10, -50, -9);

		BO_Report report;
		report.SetParameters(&Seq);
		//�������� ����������� SequenceParameters
		EXPECT_EQ(strlen(Seq.GetFileName()), 0);
		EXPECT_EQ(Seq.GetLengthSequence(), 0);
		EXPECT_EQ(Seq.GetNumberOfIterationsVer1(), 0);
		EXPECT_EQ(Seq.GetNumberOfIterationsVer2(), 0);
		EXPECT_EQ(Seq.GetStep(), 0);
	}

	// ���� 4.12 �������� SetResultSequenceVer1 �� ����������

	TEST(TestReport, SetResultSequenceVer1IsNotEmpty) {
		BO_ResultsTest RTforRI[15];

		int sizearray = 10;

		//��������� ������ ���� ResultsTest
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(10.55);
			RTforRI[i].SetTestNumber(5);
		}

		BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizearray];

		//��������� ������ ���� ResultsIteration
		for (int i = 0; i < sizearray; i++)
		{
			resultsiterationVer1[i].SetResultIteration(RTforRI);
		}

		BO_ResultsExperiment experiment1(resultsiterationVer1, sizearray);
		
		BO_Report report;
		report.SetResultSequenceVer1(&experiment1);
	
		// ���������� ������������
		BO_ResultsExperiment *GetRE1;
		BO_ResultsIteration *GetRI;
		BO_ResultsTest *GetRT;
		// Experiment1
		GetRE1 = report.GetResultSequenceVer1();
		GetRI = GetRE1[0].GetResultsExperiment();
		GetRT = GetRI[0].GetResultIteration();
		EXPECT_GT(GetRT[0].GetTestNumber(), 0);
		EXPECT_GT(GetRT[0].GetResultTest(), 0);
	}

	// ���� 4.13 �������� SetResultSequenceVer1 �� ������� ��������

	TEST(TestReport, SetResultSequenceVer1IsEmpty) {
		BO_ResultsTest RTforRI[15];

		int sizearray = 10;

		//��������� ������ ���� ResultsTest
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(0);
			RTforRI[i].SetTestNumber(0);
		}

		BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizearray];

		//��������� ������ ���� ResultsIteration
		for (int i = 0; i < sizearray; i++)
		{
			resultsiterationVer1[i].SetResultIteration(RTforRI);
		}

		BO_ResultsExperiment experiment1(resultsiterationVer1, sizearray);

		BO_Report report;
		report.SetResultSequenceVer1(&experiment1);

		// ���������� ������������
		BO_ResultsExperiment *GetRE1;
		BO_ResultsIteration *GetRI;
		BO_ResultsTest *GetRT;
		// Experiment1
		GetRE1 = report.GetResultSequenceVer1();
		GetRI = GetRE1[0].GetResultsExperiment();
		GetRT = GetRI[0].GetResultIteration();
		EXPECT_EQ(GetRT[0].GetTestNumber(), 0);
		EXPECT_EQ(GetRT[0].GetResultTest(), 0);
	}

	// ���� 4.14 �������� SetResultSequenceVer1 �� ������������� ��������

	TEST(TestReport, SetResultSequenceVer1IsNegative) {
		BO_ResultsTest RTforRI[15];

		int sizearray = 10;

		//��������� ������ ���� ResultsTest
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(-524.55);
			RTforRI[i].SetTestNumber(-98);
		}

		BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizearray];
		BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[sizearray];

		//��������� ������ ���� ResultsIteration
		for (int i = 0; i < sizearray; i++)
		{
			resultsiterationVer1[i].SetResultIteration(RTforRI);
		}

		BO_ResultsExperiment experiment1(resultsiterationVer1, sizearray);

		BO_Report report;
		report.SetResultSequenceVer1(&experiment1);

		// ���������� ������������
		BO_ResultsExperiment *GetRE1;
		BO_ResultsIteration *GetRI;
		BO_ResultsTest *GetRT;
		// Experiment1
		GetRE1 = report.GetResultSequenceVer1();
		GetRI = GetRE1[0].GetResultsExperiment();
		GetRT = GetRI[0].GetResultIteration();
		EXPECT_EQ(GetRT[0].GetTestNumber(), 0);
		EXPECT_EQ(GetRT[0].GetResultTest(), 0);
	}

	// ���� 4.15 �������� SetResultSequenceVer2 �� ����������

	TEST(TestReport, SetResultSequenceVer2IsNotEmpty) {
		BO_ResultsTest RTforRI[15];

		int sizearray = 10;

		//��������� ������ ���� ResultsTest
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(10.55);
			RTforRI[i].SetTestNumber(5);
		}

		BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[sizearray];

		//��������� ������ ���� ResultsIteration
		for (int i = 0; i < sizearray; i++)
		{
			resultsiterationVer2[i].SetResultIteration(RTforRI);
		}

		BO_ResultsExperiment experiment2(resultsiterationVer2, sizearray);

		BO_Report report;
		report.SetResultSequenceVer2(&experiment2);

		// ���������� ������������
		BO_ResultsExperiment *GetRE2;
		BO_ResultsIteration *GetRI;
		BO_ResultsTest *GetRT;

		// Experiment2
		GetRE2 = report.GetResultSequenceVer2();
		GetRI = GetRE2[0].GetResultsExperiment();
		GetRT = GetRI[0].GetResultIteration();
		EXPECT_GT(GetRT[0].GetTestNumber(), 0);
		EXPECT_GT(GetRT[0].GetResultTest(), 0);
	}

	// ���� 4.16 �������� SetResultSequenceVer2 �� ������� ��������

	TEST(TestReport, SetResultSequenceVer2IsEmpty) {
		BO_ResultsTest RTforRI[15];

		int sizearray = 10;

		//��������� ������ ���� ResultsTest
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(0);
			RTforRI[i].SetTestNumber(0);
		}

		BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[sizearray];

		//��������� ������ ���� ResultsIteration
		for (int i = 0; i < sizearray; i++)
		{
			resultsiterationVer2[i].SetResultIteration(RTforRI);
		}

		BO_ResultsExperiment experiment2(resultsiterationVer2, sizearray);

		BO_Report report;
		report.SetResultSequenceVer2(&experiment2);

		// ���������� ������������
		BO_ResultsExperiment *GetRE2;
		BO_ResultsIteration *GetRI;
		BO_ResultsTest *GetRT;

		// Experiment2
		GetRE2 = report.GetResultSequenceVer2();
		GetRI = GetRE2[0].GetResultsExperiment();
		GetRT = GetRI[0].GetResultIteration();
		EXPECT_EQ(GetRT[0].GetTestNumber(), 0);
		EXPECT_EQ(GetRT[0].GetResultTest(), 0);
	}

	// ���� 4.17 �������� SetResultSequenceVer2 �� ������������� ��������

	TEST(TestReport, SetResultSequenceVer2IsNegative) {
		BO_ResultsTest RTforRI[15];

		int sizearray = 10;

		//��������� ������ ���� ResultsTest
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(-524.55);
			RTforRI[i].SetTestNumber(-98);
		}

		BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[sizearray];

		//��������� ������ ���� ResultsIteration
		for (int i = 0; i < sizearray; i++)
		{
			resultsiterationVer2[i].SetResultIteration(RTforRI);
		}

		BO_ResultsExperiment experiment2(resultsiterationVer2, sizearray);

		BO_Report report;
		report.SetResultSequenceVer2(&experiment2);


		// ���������� ������������
		BO_ResultsExperiment *GetRE2;
		BO_ResultsIteration *GetRI;
		BO_ResultsTest *GetRT;
		// Experiment2
		GetRE2 = report.GetResultSequenceVer2();
		GetRI = GetRE2[0].GetResultsExperiment();
		GetRT = GetRI[0].GetResultIteration();
		EXPECT_EQ(GetRT[0].GetTestNumber(), 0);
		EXPECT_EQ(GetRT[0].GetResultTest(), 0);
	}

	// ���� 4.18 �������� SetFaces ����������
	TEST(TestReport, SetFacesIsNotEmpty) {
		//��������� face
		BO_ResultsTest RTforFaceParametr(14, 97.55); // ResultTest for Faceparametr
		BO_FaceParametr Faceparametr[15];// ������ FaceParametr for Face
		BO_FaceParametr * GetFP;

		// ��������� ������  �������� Faceparametr 
		for (int i = 0; i < 15; i++) {
			Faceparametr[i].SetParametr(RTforFaceParametr);
			Faceparametr[i].SetNameParametr("Parametrface");
		}
		BO_Face face("Face87", Faceparametr);
		BO_Report report;
		report.SetFacesVer1(&face);

		// ���������� ����
		EXPECT_GT(strlen(face.GetFaceName()), 0);
		GetFP = face.GetFaceParameters();
		RTforFaceParametr = GetFP[0].GetParametr();

		EXPECT_GT(RTforFaceParametr.GetTestNumber(), 0);
		EXPECT_GT(RTforFaceParametr.GetResultTest(), 0);

		EXPECT_GT(strlen(GetFP[0].GetNameParametr()), 0);
	}

	// ���� 4.19 �������� SetFaces �� ������� ��������
	TEST(TestReport, SetFacesIsEmpty) {
		//��������� face
		BO_ResultsTest RTforFaceParametr(0, 0.00); // ResultTest for Faceparametr
		BO_FaceParametr Faceparametr[15];// ������ FaceParametr for Face
		BO_FaceParametr * GetFP;

		// ��������� ������  �������� Faceparametr 
		for (int i = 0; i < 15; i++) {
			Faceparametr[i].SetParametr(RTforFaceParametr);
			Faceparametr[i].SetNameParametr("");
		}
		BO_Face face("", Faceparametr);
		BO_Report report;
		report.SetFacesVer1(&face);

		// ���������� ����
		EXPECT_EQ(strlen(face.GetFaceName()), 0);
		GetFP = face.GetFaceParameters();
		RTforFaceParametr = GetFP[0].GetParametr();

		EXPECT_EQ(RTforFaceParametr.GetTestNumber(), 0);
		EXPECT_EQ(RTforFaceParametr.GetResultTest(), 0);

		EXPECT_EQ(strlen(GetFP[0].GetNameParametr()), 0);
	}

	// ���� 4.20 �������� SetFaces �� ������������� ��������
	TEST(TestReport, SetFacesIsNegative) {
		//��������� face
		BO_ResultsTest RTforFaceParametr(-50, -23.09); // ResultTest for Faceparametr
		BO_FaceParametr Faceparametr[15];// ������ FaceParametr for Face
		BO_FaceParametr * GetFP;

		// ��������� ������  �������� Faceparametr 
		for (int i = 0; i < 15; i++) {
			Faceparametr[i].SetParametr(RTforFaceParametr);
			Faceparametr[i].SetNameParametr("");
		}
		BO_Face face("", Faceparametr);
		BO_Report report;
		report.SetFacesVer1(&face);

		// ���������� ����
		EXPECT_EQ(strlen(face.GetFaceName()), 0);
		GetFP = face.GetFaceParameters();
		RTforFaceParametr = GetFP[0].GetParametr();

		EXPECT_EQ(RTforFaceParametr.GetTestNumber(), 0);
		EXPECT_EQ(RTforFaceParametr.GetResultTest(), 0);

		EXPECT_EQ(strlen(GetFP[0].GetNameParametr()), 0);
	}

	// |���� 5| ������ FaceParametr
	// ���� 5.1 ������������� ������������ � �����������
	TEST(TestFaceParametr, ConstructorWhithParametersIsEmpty) {
		BO_ResultsTest RTforFaceParametr(0, 0);
		BO_ResultsTest GetResTest;

		//�������� ������������ ������� ��������
		BO_FaceParametr FP("", RTforFaceParametr);
		GetResTest = FP.GetParametr();

		EXPECT_EQ(strlen(FP.GetNameParametr()), 0);	// EXPECT_EQ: val1 = val2
		EXPECT_EQ(GetResTest.GetTestNumber(), 0);
		EXPECT_EQ(GetResTest.GetResultTest(), 0);
	}
	
	// ���� 5.2 ������������� ������������ �� ���������, �������� ��� ���������
	TEST(TestFaceParametr, ConstructorIsEmpty) {
		BO_ResultsTest GetResTest;

		BO_FaceParametr FP;
		GetResTest = FP.GetParametr();

		EXPECT_EQ(strlen(FP.GetNameParametr()), 0);	// EXPECT_EQ: val1 = val2 
		//std::cout << GetResTest.GetTestNumber();
		//std::cout << GetResTest.GetResultTest();
		EXPECT_EQ(GetResTest.GetTestNumber(), 0);
		EXPECT_EQ(GetResTest.GetResultTest(), 0);
	}

	// ���� 5.3 ���������� ������������
	TEST(TestFaceParametr, ConstructorIsNotEmpty) {
		BO_ResultsTest RTforFaceParametr(1, 2.423);
		BO_ResultsTest GetResTest;
		BO_FaceParametr FP("parametr1", RTforFaceParametr);
		GetResTest = FP.GetParametr();

		EXPECT_GE(strlen(FP.GetNameParametr()), 0);	// EXPECT_GE: val1 >= val2
		EXPECT_GE(GetResTest.GetTestNumber(), 0);
		EXPECT_GE(GetResTest.GetResultTest(), 0);
	}

	// ���� 5.4 �������� �� ������������� ������������ (����������)
	TEST(TestFaceParametr, ConstructorIsNegative) {
		BO_ResultsTest RTforFaceParametr(-1, -2.423);
		BO_ResultsTest GetResTest;
		BO_FaceParametr FP("parametr1", RTforFaceParametr);
		GetResTest = FP.GetParametr();

		EXPECT_GE(strlen(FP.GetNameParametr()), 0);	// EXPECT_GE: val1 >= val2
		EXPECT_EQ(GetResTest.GetTestNumber(), 0);
		EXPECT_EQ(GetResTest.GetResultTest(), 0);
	}

	// ���� 5.5 �������� �� ������������� �������� SetTestNumber � SetResultTest (����������)
	TEST(TestFaceParametr, SetTestNumberAndResultTestIsNegative) {
		BO_ResultsTest RTforFaceParametr(-1, -2.55);
		BO_ResultsTest GetResTest;
		BO_FaceParametr FP;
		FP.SetParametr(RTforFaceParametr);

		GetResTest = FP.GetParametr();

		EXPECT_EQ(GetResTest.GetTestNumber(), 0);// EXPECT_GE: val1 >= val2
		EXPECT_EQ(GetResTest.GetResultTest(), 0);
	}

	// ���� 5.6 �������� �� ���������� SetTestNumber � SetResultTest
	TEST(TestFaceParametr, SetTestNumberAndResultTestIsNotNull) {
		BO_ResultsTest RTforFaceParametr(9, 7.87);
		BO_ResultsTest GetResTest;
		BO_FaceParametr FP;
		FP.SetParametr(RTforFaceParametr);

		GetResTest = FP.GetParametr();

		EXPECT_GT(GetResTest.GetTestNumber(), 0);// EXPECT_GE: val1 >= val2
		EXPECT_GT(GetResTest.GetResultTest(), 0);
	}

	// ���� 5.7 �������� �� ������� �������� SetTestNumber � SetResultTest
	TEST(TestFaceParametr, SetTestNumberAndResultTestIs0) {
		BO_ResultsTest RTforFaceParametr(0, 0.00);
		BO_ResultsTest GetResTest;
		BO_FaceParametr FP;
		FP.SetParametr(RTforFaceParametr);

		GetResTest = FP.GetParametr();

		EXPECT_EQ(GetResTest.GetTestNumber(), 0);// EXPECT_GE: val1 >= val2
		EXPECT_EQ(GetResTest.GetResultTest(), 0);
	}

	// ���� 5.8 �������� SetNameparametr �� ����������
	TEST(TestFaceParametr, SetNameparametrIsNotEmpty) {
		BO_FaceParametr FP;

		FP.SetNameParametr("abc");
		EXPECT_GT(strlen(FP.GetNameParametr()), 0);	// EXPECT_GE: val1 > val2
	}

	// ���� 5.9 �������� SetNameparametr ����� = 0
	TEST(TestFaceParametr, SetNameparametrIsEmpty) {
		BO_FaceParametr FP;

		FP.SetNameParametr("");
		EXPECT_EQ(strlen(FP.GetNameParametr()), 0);	// EXPECT_GE: val1 > val2
	}

	// |���� 6| ������ Face
	// ���� 6.1 �������� ������������ �� ������� ��������
	TEST(TestFace, ConstructorIsEmpty) {
		int sizearrayface = 5;
		BO_ResultsTest RTforFaceParametr(0, 0); // ResultTest for Faceparametr
		BO_FaceParametr * Faceparametr = new BO_FaceParametr[sizearrayface];// ������ FaceParametr for Face

		// ��������� ������  �������� Faceparametr 
		for (int i = 0; i < sizearrayface; i++) {
			Faceparametr[i].SetParametr(RTforFaceParametr);
			Faceparametr[i].SetNameParametr("");
		}
		
		//������� ��������� ����
		BO_Face face("", Faceparametr);
		//�������� �� ������ ��� ����
		EXPECT_EQ(strlen(face.GetFaceName()), 0);
		BO_FaceParametr * FP = face.GetFaceParameters();
		
		//�������� �� ������� �������� NameParametr, ResultTest � TestNumber
		//std::cout << "GetNameParametr:" << strlen(FP[1].GetNameParametr()) << std::endl;
		EXPECT_EQ(strlen(FP[1].GetNameParametr()) , 0);
		BO_ResultsTest ForFaceparametr = FP[1].GetParametr();
		//std::cout << "GetResultTest:" << ForFaceparametr.GetResultTest() << std::endl;
		EXPECT_EQ(ForFaceparametr.GetResultTest(), 0);
		//std::cout << "GetTestNumber:" << ForFaceparametr.GetTestNumber() << std::endl;
		EXPECT_EQ(ForFaceparametr.GetTestNumber(), 0);
	}

	// ���� 6.2 �������� ������������ �� ����������
	TEST(TestFace, ConstructorIsNotEmpty) {
		int sizearrayface = 5;
		BO_ResultsTest RTforFaceParametr(2, 2.55); // ResultTest for Faceparametr
		BO_FaceParametr * Faceparametr = new BO_FaceParametr[sizearrayface];// ������ FaceParametr for Face

		// ��������� ������  �������� Faceparametr 
		for (int i = 0; i < sizearrayface; i++) {
			Faceparametr[i].SetParametr(RTforFaceParametr);
			Faceparametr[i].SetNameParametr("Par");
		}

		//������� ��������� ����
		BO_Face face("Face", Faceparametr);

		//�������� �� ��� ����
		EXPECT_GT(strlen(face.GetFaceName()), 0); // EXPECT_GE: val1 > val2
		BO_FaceParametr * FP = face.GetFaceParameters();

		//�������� �� ���������� NameParametr, ResultTest � TestNumber
		EXPECT_GT(strlen(FP[1].GetNameParametr()), 0);
		BO_ResultsTest ForFaceparametr = FP[1].GetParametr();
		EXPECT_GT(ForFaceparametr.GetResultTest(), 0);
		EXPECT_GT(ForFaceparametr.GetTestNumber(), 0);
	}

	// ���� 6.3 �������� ������������ �� ������������� ������������  (�������� ResultTest � TestNumber ����������)
	TEST(TestFace, ConstructorIsNegative) {
		int sizearrayface = 5;
		BO_ResultsTest RTforFaceParametr(-1, -2.55); // ResultTest for Faceparametr
		BO_FaceParametr * Faceparametr = new BO_FaceParametr[sizearrayface];// ������ FaceParametr for Face

		// ��������� ������  �������� Faceparametr 
		for (int i = 0; i < sizearrayface; i++) {
			Faceparametr[i].SetParametr(RTforFaceParametr);
		}

		//������� ��������� ����
		BO_Face face("Face", Faceparametr);

		BO_FaceParametr * FP = face.GetFaceParameters();

		//�������� ResultTest � TestNumber
		BO_ResultsTest ForFaceparametr = FP[1].GetParametr();
		EXPECT_EQ(ForFaceparametr.GetResultTest(), 0);
		EXPECT_EQ(ForFaceparametr.GetTestNumber(), 0);
	}

	// ���� 6.4 �������� SetFaceParameters �� ������������� ��������
	TEST(TestFace, SetFaceParametersIsNegative) {
		int sizearrayface = 5;
		BO_ResultsTest RTforFaceParametr(-1, -2.55); // ResultTest for Faceparametr
		BO_FaceParametr * Faceparametr = new BO_FaceParametr[sizearrayface];// ������ FaceParametr for Face

		// ��������� ������  �������� Faceparametr 
		for (int i = 0; i < sizearrayface; i++) {
			Faceparametr[i].SetParametr(RTforFaceParametr);
			Faceparametr[i].SetNameParametr("");
		}

		//������� ��������� ����
		BO_Face face;
		face.SetFaceParameters(Faceparametr);

		BO_FaceParametr * FP = face.GetFaceParameters();
		//�������� �� ������� ����� ����� ���������
		EXPECT_EQ(strlen(FP[1].GetNameParametr()), 0);
		//�������� ResultTest � TestNumber
		BO_ResultsTest ForFaceparametr = FP[1].GetParametr();
		EXPECT_EQ(ForFaceparametr.GetResultTest(), 0);
		EXPECT_EQ(ForFaceparametr.GetTestNumber(), 0);
	}

	// ���� 6.5 �������� SetFaceParameters �� ����������
	TEST(TestFace, SetFaceParametersIsNotEmpty) {
		int sizearrayface = 5;
		BO_ResultsTest RTforFaceParametr(4, 32.55); // ResultTest for Faceparametr
		BO_FaceParametr * Faceparametr = new BO_FaceParametr[sizearrayface];// ������ FaceParametr for Face

		// ��������� ������  �������� Faceparametr 
		for (int i = 0; i < sizearrayface; i++) {
			Faceparametr[i].SetParametr(RTforFaceParametr);
			Faceparametr[i].SetNameParametr("Par");
		}

		//������� ��������� ����
		BO_Face face;
		face.SetFaceParameters(Faceparametr);

		BO_FaceParametr * FP = face.GetFaceParameters();
		//�������� �� ������� ����� ����� ���������
		EXPECT_GT(strlen(FP[1].GetNameParametr()), 0);
		//�������� ResultTest � TestNumber
		BO_ResultsTest ForFaceparametr = FP[1].GetParametr();
		EXPECT_GT(ForFaceparametr.GetResultTest(), 0);
		EXPECT_GT(ForFaceparametr.GetTestNumber(), 0);
	}

	// ���� 6.6 �������� SetFaceParameters �� ������� ��������
	TEST(TestFace, SetFaceParametersIsEmpty) {
		int sizearrayface = 5;
		BO_ResultsTest RTforFaceParametr(0, 0.00); // ResultTest for Faceparametr
		BO_FaceParametr * Faceparametr = new BO_FaceParametr[sizearrayface];// ������ FaceParametr for Face

		// ��������� ������  �������� Faceparametr 
		for (int i = 0; i < sizearrayface; i++) {
			Faceparametr[i].SetParametr(RTforFaceParametr);
			Faceparametr[i].SetNameParametr("");
		}

		//������� ��������� ����
		BO_Face face;
		face.SetFaceParameters(Faceparametr);

		BO_FaceParametr * FP = face.GetFaceParameters();
		//�������� �� ������� ����� ����� ���������
		EXPECT_EQ(strlen(FP[1].GetNameParametr()), 0);
		//�������� ResultTest � TestNumber
		BO_ResultsTest ForFaceparametr = FP[1].GetParametr();
		EXPECT_EQ(ForFaceparametr.GetResultTest(), 0);
		EXPECT_EQ(ForFaceparametr.GetTestNumber(), 0);
	}

	// ���� 6.7 �������� SetFaceName �� ����������
	TEST(TestFace, SetFaceNameIsNotEmpty) {
		//������� ��������� ����
		BO_Face face;
		face.SetFaceName("Face1");
		//�������� ResultTest � TestNumber
		EXPECT_GE(strlen(face.GetFaceName()), 0);
	}

	// ���� 6.8 �������� SetFaceName �� ������� �����
	TEST(TestFace, SetFaceNameIsEmpty) {
		//������� ��������� ����
		BO_Face face;
		face.SetFaceName("");
		//�������� ResultTest � TestNumber
		EXPECT_EQ(strlen(face.GetFaceName()), 0);
	}

	// |���� 7| ������ SequenceParameters
	// ���� 7.1 �������� ������� ������������ �� ���������
	TEST(TestSequenceParameters, ConstructorIsEmpty) {
		BO_SequenceParameters Seq;
		EXPECT_EQ(strlen(Seq.GetFileName()), 0);
		EXPECT_EQ(Seq.GetLengthSequence(), 0);
		EXPECT_EQ(Seq.GetNumberOfIterationsVer1(), 0);
		EXPECT_EQ(Seq.GetNumberOfIterationsVer2(), 0);
		EXPECT_EQ(Seq.GetStep(), 0);
	}

	// ���� 7.2 �������� ������������ � ����������� �� ����������
	TEST(TestSequenceParameters, ConstructorIsNotEmpty) {
		BO_SequenceParameters Seq("C:\\ABC", 50, 10, 10, 2);
		EXPECT_GT(strlen(Seq.GetFileName()), 0);
		EXPECT_GT(Seq.GetLengthSequence(), 0);
		EXPECT_GT(Seq.GetNumberOfIterationsVer1(), 0);
		EXPECT_GT(Seq.GetNumberOfIterationsVer2(), 0);
		EXPECT_GT(Seq.GetStep(), 0);
	}

	// ���� 7.3 �������� ������������ � ����������� �� ������������� �������� (����������)
	TEST(TestSequenceParameters, ConstructorIsNegative) {
		BO_SequenceParameters Seq("", -1, -5, -5, -7);
		EXPECT_EQ(strlen(Seq.GetFileName()), 0);
		EXPECT_EQ(Seq.GetLengthSequence(), 0);
		EXPECT_EQ(Seq.GetNumberOfIterationsVer1(), 0);
		EXPECT_EQ(Seq.GetNumberOfIterationsVer2(), 0);
		EXPECT_EQ(Seq.GetStep(), 0);
	}

	// ���� 7.4 �������� �� ������������� �������� ��� SetLengthSequence (����������)
	TEST(TestSequenceParameters, SetLengthSequenceIsNegative) {
		BO_SequenceParameters Seq;
		Seq.SetLengthSequence(-1);

		EXPECT_EQ(Seq.GetLengthSequence(), 0);
	}

	// ���� 7.5 �������� �� ���������� ��� SetLengthSequence
	TEST(TestSequenceParameters, SetLengthSequenceIsNotEmpty) {
		BO_SequenceParameters Seq;
		Seq.SetLengthSequence(25);

		EXPECT_GT(Seq.GetLengthSequence(), 0);
	}

	// ���� 7.6 �������� �� ������� �������� ��� SetLengthSequence
	TEST(TestSequenceParameters, SetLengthSequenceIsEmpty) {
		BO_SequenceParameters Seq;
		Seq.SetLengthSequence(0);

		EXPECT_EQ(Seq.GetLengthSequence(), 0);
	}

	// ���� 7.7 �������� �� ������������� �������� ��� SetNumberOfIterationsVer1 (����������)
	TEST(TestSequenceParameters, SetNumberOfIterationsVer1IsNegative) {
		BO_SequenceParameters Seq;

		Seq.SetNumberOfIterationsVer1(-54);

		EXPECT_EQ(Seq.GetNumberOfIterationsVer1(), 0);
	}

	// ���� 7.8  �������� �� ���������� ��� SetNumberOfIterationsVer1
	TEST(TestSequenceParameters, SetNumberOfIterationsVer1IsNotEmpty) {
		BO_SequenceParameters Seq;

		Seq.SetNumberOfIterationsVer1(844);

		EXPECT_GT(Seq.GetNumberOfIterationsVer1(), 0);
	}

	// ���� 7.9 �������� �� ������� �������� ��� SetNumberOfIterationsVer1
	TEST(TestSequenceParameters, SetNumberOfIterationsVer1IsEmpty) {
		BO_SequenceParameters Seq;

		Seq.SetNumberOfIterationsVer1(0);

		EXPECT_EQ(Seq.GetNumberOfIterationsVer1(), 0);
	}
	// ���� 7.10 �������� �� ������������� �������� ��� SetNumberOfIterationsVer2 (����������)
	TEST(TestSequenceParameters, SetNumberOfIterationsVer2IsNegative) {
		BO_SequenceParameters Seq;

		Seq.SetNumberOfIterationsVer2(-87);

		EXPECT_EQ(Seq.GetNumberOfIterationsVer2(), 0);
	}

	// ���� 7.11 �������� �� ���������� ��� SetNumberOfIterationsVer2
	TEST(TestSequenceParameters, SetNumberOfIterationsVer2IsNotEmpty) {
		BO_SequenceParameters Seq;

		Seq.SetNumberOfIterationsVer2(64);

		EXPECT_GT(Seq.GetNumberOfIterationsVer2(), 0);
	}

	// ���� 7.12 �������� �� ������� �������� ��� SetNumberOfIterationsVer2
	TEST(TestSequenceParameters, SetNumberOfIterationsVer2IsEmpty) {
		BO_SequenceParameters Seq;

		Seq.SetNumberOfIterationsVer2(0);

		EXPECT_EQ(Seq.GetNumberOfIterationsVer2(), 0);
	}

	// ���� 7.13 �������� �� ������������� �������� ��� SetStep (����������)
	TEST(TestSequenceParameters, SetStepIsNegative) {
		BO_SequenceParameters Seq;

		Seq.SetStep(-16);

		EXPECT_EQ(Seq.GetStep(), 0);
	}

	// ���� 7.14 �������� �� ���������� ��� SetStep
	TEST(TestSequenceParameters, SetStepIsNotEmpty) {
		BO_SequenceParameters Seq;

		Seq.SetStep(77);

		EXPECT_GT(Seq.GetStep(), 0);
	}

	// ���� 7.15 �������� �� ������� �������� ��� SetStep
	TEST(TestSequenceParameters, SetStepIsEmpty) {
		BO_SequenceParameters Seq;

		Seq.SetStep(0);

		EXPECT_EQ(Seq.GetStep(), 0);
	}

	// ���� 7.16 �������� SetFileName �� ����������
	TEST(TestSequenceParameters, SetFileNameIsNotEmpty) {
		BO_SequenceParameters Seq;
		Seq.SetFileName("c:\\users");

		EXPECT_GT(strlen(Seq.GetFileName()), 0);
	}

	// ���� 7.17 �������� SetFileName �� ������� �����
	TEST(TestSequenceParameters, SetFileNameIsEmpty) {
		BO_SequenceParameters Seq;
		Seq.SetFileName("");

		EXPECT_EQ(strlen(Seq.GetFileName()), 0);
	}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
