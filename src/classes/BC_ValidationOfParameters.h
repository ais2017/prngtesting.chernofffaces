#pragma once
#include "BO_SequenceParameters.h"

class BC_ValidationOfParameters
{
	BO_SequenceParameters * SeqParameters;
public:
	BC_ValidationOfParameters(BO_SequenceParameters *);
	~BC_ValidationOfParameters();
	int GetValidationOfFileName();
	int GetValidationOfStep();
	int GetValidationOfLengthSequence();
};

