#include "BC_Stub.h"


BC_Stub::BC_Stub() 
{
}


BC_Stub::~BC_Stub()
{
}


int BC_Stub::SaveSeqParameters(BO_SequenceParameters * SeqPar)
{
	return 0;
}

int BC_Stub::SaveData(BO_ResultsExperiment *RE1, BO_ResultsExperiment *RE2, BO_SequenceParameters * SeqPar)
{
	return 0;
}

int BC_Stub::SaveDataFace(BO_Face *face1, BO_Face *face2)
{
	return 0;
}


int BC_Stub::SaveDataReport(BO_Report *report)
{
	return 0;
}


int BC_Stub::LoadSeqParameters(BO_SequenceParameters *SeqPar)
{
	int lengthfile = 40;
	char * FName = new char[lengthfile];
	memset(FName, 0, sizeof(FName));
	strcpy_s(FName, 7, "C:\\ABC");
	//��������� seqparameters
	SeqPar->SetFileName(FName);
	SeqPar->SetLengthSequence(100);
	SeqPar->SetNumberOfIterationsVer1(37);
	SeqPar->SetNumberOfIterationsVer2(20);
	SeqPar->SetStep(2);
	return 0;
}

int BC_Stub::LoadData(BO_ResultsExperiment *RE1, BO_ResultsExperiment *RE2, BO_SequenceParameters * SeqPar)
{
	BO_ResultsTest RTforRI[15];
	//��������� ������ ���� ResultsTest

	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[SeqPar->GetNumberOfIterationsVer1()];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[SeqPar->GetNumberOfIterationsVer2()];

	//��������� ������ ���� ResultsIteration
	for (int i = 0; i < SeqPar->GetNumberOfIterationsVer1(); i++)
	{
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(1 + rand() % 10);
			RTforRI[i].SetTestNumber(5);
		}
		resultsiterationVer1[i].SetResultIteration(RTforRI);
	}

	for (int i = 0; i < SeqPar->GetNumberOfIterationsVer2(); i++)
	{
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(1 + rand() % 10);
			RTforRI[i].SetTestNumber(5);
		}
		resultsiterationVer2[i].SetResultIteration(RTforRI);
	}

	RE1->SetResultsExperiment(resultsiterationVer1, SeqPar->GetNumberOfIterationsVer1());
	RE2->SetResultsExperiment(resultsiterationVer2, SeqPar->GetNumberOfIterationsVer2());
	return 0;
}


int BC_Stub::LoadDataFace(BO_Face *face1, BO_Face *face2)
{
	int lengthfile = 40;
	char * NamePar = new char[lengthfile];
	memset(NamePar, 0, sizeof(NamePar));
	strcpy_s(NamePar, 5, "Par1");

	char * facename = new char[lengthfile];
	memset(facename, 0, sizeof(facename));
	strcpy_s(facename, 5, "Face");
	//��������� face
	BO_ResultsTest RTforFaceParametr(2, 2.55); // ResultTest for Faceparametr
	BO_FaceParametr Faceparametr[15];// ������ FaceParametr for Face
	BO_FaceParametr * GetFP;

	// ��������� ������  �������� Faceparametr 
	for (int i = 0; i < 15; i++) {
		Faceparametr[i].SetParametr(RTforFaceParametr);
		Faceparametr[i].SetNameParametr(NamePar);
	}
	face1->SetFaceParameters(Faceparametr);
	face1->SetFaceName(facename);
	face2->SetFaceParameters(Faceparametr);
	face2->SetFaceName(facename);
	return 0;
}


int BC_Stub::LoadDataReport(BO_Report *report)
{

	BO_ResultsTest RTforRI[15];

	int sizearray = 10;
	int lengthfile = 40;
	char * FName = new char[lengthfile];
	memset(FName, 0, sizeof(FName));
	strcpy_s(FName, 7, "C:\\ABC");

	lengthfile = 40;
	char * NamePar = new char[lengthfile];
	memset(NamePar, 0, sizeof(NamePar));
	strcpy_s(NamePar, 5, "Par1");

	char * facename = new char[lengthfile];
	memset(facename, 0, sizeof(facename));
	strcpy_s(facename, 5, "Face");
	//��������� ������ ���� ResultsTest
	for (int i = 0; i < 15; i++)
	{
		RTforRI[i].SetResultTest(10.55);
		RTforRI[i].SetTestNumber(5);
	}

	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizearray];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[sizearray];

	//��������� ������ ���� ResultsIteration
	for (int i = 0; i < sizearray; i++)
	{
		resultsiterationVer1[i].SetResultIteration(RTforRI);
		resultsiterationVer2[i].SetResultIteration(RTforRI);
	}

	BO_ResultsExperiment experiment1(resultsiterationVer1, sizearray);
	BO_ResultsExperiment experiment2(resultsiterationVer2, sizearray);
	//��������� seqparameters
	BO_SequenceParameters Seq(FName, 50, 10, 10, 2);

	//��������� face
	BO_ResultsTest RTforFaceParametr(2, 2.55); // ResultTest for Faceparametr
	BO_FaceParametr Faceparametr[15];// ������ FaceParametr for Face
	BO_FaceParametr * GetFP;

	// ��������� ������  �������� Faceparametr 
	for (int i = 0; i < 15; i++) {
		Faceparametr[i].SetParametr(RTforFaceParametr);
		Faceparametr[i].SetNameParametr(NamePar);
	}
	BO_Face face(facename, Faceparametr);
	report->SetFacesVer1(&face);
	report->SetFacesVer2(&face);
	report->SetHushSum(5);
	report->SetParameters(&Seq);
	report->SetReportName(facename);
	report->SetResultSequenceVer1(&experiment1);
	report->SetResultSequenceVer2(&experiment2);
	return 0;
}
