#pragma once
class IGUI
{
public:
	virtual ~IGUI() {};

	virtual int request() = 0;
	virtual int answer() = 0;
};

