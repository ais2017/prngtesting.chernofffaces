#pragma once
#include "BO_ResultsTest.h"

class BO_ResultsIteration
{
	BO_ResultsTest ResultIteration[15];
public:
	BO_ResultsIteration();
	BO_ResultsIteration(BO_ResultsTest *);
	~BO_ResultsIteration();
	void SetResultIteration(BO_ResultsTest *);
	BO_ResultsTest * GetResultIteration();
	//void CallResultsTest(int, float);
};

