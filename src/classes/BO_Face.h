#pragma once
#include "BO_FaceParametr.h"

class BO_Face
{	
	char * FaceName;
	BO_FaceParametr FaceParameters[15];

public:
	BO_Face();
	BO_Face(char *, BO_FaceParametr *);
	void SetFaceParameters(BO_FaceParametr *);
	BO_FaceParametr * GetFaceParameters();
	void SetFaceName(char *);
	char * GetFaceName();
	~BO_Face();
};

