// chernofffaces.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "BO_Face.h"
#include "BO_FaceParametr.h"
#include "BO_Report.h"
#include "BO_ResultsExperiment.h"
#include "BO_ResultsIteration.h"
#include "BO_ResultsTest.h"
#include "BO_SequenceParameters.h"
#include <iostream>
#include <string>
#include "md5.h"
#include <fstream>
#include "DB_SQLite.h"
#include "BC_ControlComponent.h"

void WorkDBSave(IGateWay * dbsql, BO_ResultsExperiment * RE1, BO_ResultsExperiment * RE2, BO_SequenceParameters * SeqPar)
{
	dbsql->SaveData(RE1, RE2, SeqPar);
}

void WorkDBLoad(IGateWay * dbsql, BO_ResultsExperiment * RE1, BO_ResultsExperiment * RE2, BO_SequenceParameters * SeqPar)
{
	dbsql->LoadData(RE1, RE2, SeqPar);
}

void WorkDBLoadSeqPar(IGateWay * dbsql, BO_SequenceParameters * SeqPar)
{
	dbsql->LoadSeqParameters(SeqPar);
}

int main()
{	
	BO_SequenceParameters SeqPar;

	DB_SQLite *sqlSeqpar = new DB_SQLite;
	WorkDBLoadSeqPar(sqlSeqpar, &SeqPar);
	//==============================================================================
	//SequenceParameters

	int lengthfile = 50;
	char * FName = new char[lengthfile];
	memset(FName, 0, sizeof(FName));
	strcpy_s(FName, strlen("F:\\1\\AIS\\sts - 2.1.2\\sts - 2.1.2\\data\\1.bin"), "F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\1.bin");
	//BO_SequenceParameters SeqPar(FName, 100, 20, 2, 5);

	std::cout << "FIRST:" << SeqPar.GetFileName() << std::endl;
	std::cout << "FIRST:" << SeqPar.GetLengthSequence() << std::endl;
	std::cout << "FIRST:" << SeqPar.GetNumberOfIterationsVer1() << std::endl;
	std::cout << "FIRST:" << SeqPar.GetNumberOfIterationsVer2() << std::endl;
	std::cout << "FIRST:" << SeqPar.GetStep() << std::endl;
	/*
	BC_ControlComponent * control = new BC_ControlComponent;
	std::cout << "ControlComponent:" << control->ValidationOfParameters(&SeqPar) << std::endl; //после подсчитывает кол-во итераций для 2 вариантов тестов
	*/
	BO_ResultsTest RTforRI[15];
	
	std::cout << "Kol1:" << SeqPar.GetNumberOfIterationsVer1()  <<std::endl;
	std::cout << "Kol2:" << SeqPar.GetNumberOfIterationsVer2() << std::endl;

	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer1()];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer2()];

	//заполняем массив типа ResultsIteration
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer1(); i++)
	{
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(1 + rand() % 10);
			RTforRI[i].SetTestNumber(5);
		}
		resultsiterationVer1[i].SetResultIteration(RTforRI);
	}

	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer2(); i++)
	{
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(1 + rand() % 10);
			RTforRI[i].SetTestNumber(5);
		}
		resultsiterationVer2[i].SetResultIteration(RTforRI);
	}

	BO_ResultsExperiment experiment1(resultsiterationVer1, SeqPar.GetNumberOfIterationsVer1());
	BO_ResultsExperiment experiment2(resultsiterationVer2, SeqPar.GetNumberOfIterationsVer2());
	// результаты эксперимента
	BO_ResultsExperiment *GetRE1, *GetRE2;
	BO_ResultsIteration *GetRI;
	BO_ResultsTest *GetRT;
	
	//вывод результатов
	/*
	GetRI = experiment1.GetResultsExperiment();
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer1(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //Вывод массива результатов
		{
			// делаем запрос
			std::cout << "RI[" << i + 1 << "]" << "RT[" << j + 1 << "]:" << GetRT[j].GetResultTest() << std::endl;
			//sqlite3_finalize(stmt);//сбрасываем инструкцию
		}
	}*/
	//конец вывода

	DB_SQLite sql;
	std::cout << "Cod:"<<sql.SaveData(&experiment1, &experiment1, &SeqPar) << std::endl;
	//std::cout << sql.LoadData(&experiment1, &experiment1, &SeqPar) << std::endl;
	//std::cout << sql.LoadSeqParameters(&SeqPar) << std::endl;

	std::cout << "SELECT2:" << SeqPar.GetFileName() << std::endl;
	std::cout << "SELECT2:" << SeqPar.GetLengthSequence() << std::endl;
	std::cout << "SELECT2:" << SeqPar.GetNumberOfIterationsVer1() << std::endl;
	std::cout << "SELECT2:" << SeqPar.GetNumberOfIterationsVer2() << std::endl;
	std::cout << "SELECT2:" << SeqPar.GetStep() << std::endl;
	
	/*
	BO_ResultsTest RT(1, 5.55);//Инициализация результата теста

	std::cout << "Res:" << RT.GetResultTest() << "\n";
	std::cout << "TestNumber:" << RT.GetTestNumber() << "\n";

	BO_ResultsTest RTforRIVer1[15], RTforRIVer2[15];// инициализируем результат теста для результатов итерации
	for (i = 0; i < 15; i++)
	{
		std::cout << "Testnum[" << i << "]:";
		std::cout << "Result[" << i << "]:";
		std::cin >> resultVer1;
		std::cout << "\n";
		resultVer2 = i+1;

		RTforRIVer1[i].SetResultTest(resultVer1);
		RTforRIVer1[i].SetTestNumber(i);

		RTforRIVer2[i].SetResultTest(resultVer2);
		RTforRIVer2[i].SetTestNumber(i);
	}
	// на выходе массив результатов итерации

	//BO_ResultsIteration RI(RTforRI);//Инициализация результатов итерации (передаем массив результатов всех тестов в результаты итерации)
	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizeVer1];
	BO_ResultsIteration *resultsiterarionVer2 = new BO_ResultsIteration[sizeVer2];
	BO_ResultsTest *RI1; //указатель типа ResultTest для получения массива

	resultsiterationVer1[0].SetResultIteration(RTforRIVer1);//Заполняемя массив результатов итерации для 1 варианта
	resultsiterarionVer2[0].SetResultIteration(RTforRIVer2);//Заполняемя массив результатов итерации для 2 варианта
	*/
	//RI1=resultsiterationVer1[0].GetResultIteration();// Получаем указатель на массив ResultIteration
	
	/*for (i = 0; i < 15; i++) //Вывод массива результатов
	{
		std::cout << "ResultVer1[" << i << "]:" << RI1[i].GetResultTest() << "\n";
		std::cout << "TestNumber[" << i << "]:" << RI1[i].GetTestNumber() << "\n";
	}

	RI1 = resultsiterarionVer2[0].GetResultIteration();// Получаем указатель на массив ResultIteration

	for (i = 0; i < 15; i++) //Вывод массива результатов
	{
		std::cout << "ResultVer2[" << i << "]:" << RI1[i].GetResultTest() << "\n";
		std::cout << "TestNumber[" << i << "]:" << RI1[i].GetTestNumber() << "\n";
	}*/
	/*
	BO_ResultsExperiment RE(resultsiterationVer1, sizeVer1); //инициализация
	BO_ResultsIteration  * RIforRE; // указатель типа ResultsIteration для получения массива
	RIforRE = RE.GetResultsExperiment();

	RI1 = RIforRE[0].GetResultIteration();
	for (i = 0; i < 15; i++) //Вывод массива результатов
	{
		std::cout << "TestNumber[" << i << "]:" << RI1[i].GetTestNumber() << "\n";
		std::cout << "ResultVer1[" << i << "]:" << RI1[i].GetResultTest() << "\n";
	}

	//SequenceParameters
	int lengthfile = 40;
	char * FName = new char[lengthfile];
	memset(FName, 0, sizeof(FName));
	strcpy_s(FName, 6, "File1");

	BO_SequenceParameters SP(FName, 100, 10, 8, 3);
	std::cout << "FileName" << SP.GetFileName() <<"\n";
	SP.SetFileName(FName);
	std::cout << "FileName2:" << SP.GetFileName() << "\n";

	int b = 0;
	std::cout << "GetResultTest:" << b << "\n";
	//report

	BO_ResultsTest RTforFaceParametr(2, 2.55); // ResultTest for Faceparametr
	BO_FaceParametr Faceparametr[15];// массив FaceParametr for Face
	BO_FaceParametr * GetFP;

	// заполняем массив  объектов Faceparametr 
	for (int i = 0; i < 15; i++) {
		Faceparametr[i].SetParametr(RTforFaceParametr);
		Faceparametr[i].SetNameParametr(FName);
	}
	BO_Face face(FName, Faceparametr);

	BO_Report report;
	report.SetParameters(&SP);
	*/
	
	//=====================================================================================
	/*
	std::string stringHashSum;
	control->CalculateHashSum("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\Faces.png", stringHashSum);
	std::cout << "FILEHASH" << stringHashSum << std::endl;

	//======================== Подсчет хеш-суммы MD5 изображений ==========================
	std::string line;
	std::string outputline;
	std::ifstream Fin;

	Fin.open("F:\\1\\Desert.jpg", std::ios::binary | std::ios::in); //открытие для чтение

	if (Fin)
	{
		std::cout << "Successful\n";
	}
	else
	{
		std::cout << "Error\n";
		Fin.close();
		return 0;
	}

	if (Fin.peek() == EOF)
	{
		//std::cout << "File is empty\n";
		Fin.close();
		return(2);
	}

	std::getline(Fin, line);
	while (!Fin.eof())
	{
		std::getline(Fin, line);
		outputline = outputline + line;
		//std::cout << "LINEFIRST:" << line << std::endl; // строка
	}

	Fin.close();
	std::cout << std::endl;
	std::cout << "Calculate Hash Sum 'MD5' of image: " << md5(outputline) << std::endl;*/
	//=====================================================================================
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
