#include "pch.h"
#include "BO_ResultsTest.h"
#include <iostream>

BO_ResultsTest::BO_ResultsTest()
{
	TestNumber = 0;
	ResultTest = 0;
}

BO_ResultsTest::BO_ResultsTest(int TestNum, double Result)
{
	//�������� �� ������������� ��������
	if (TestNum < 0)
	{
		//std::cout << "Values will can not is negative \n";
		TestNumber = 0;
	}
	else
	{
		TestNumber = TestNum;
	}

	if (Result < 0)
	{
		//std::cout << "Values will can not is negative \n";
		ResultTest = 0.00;
	}
	else
	{
		ResultTest = Result;
	}
}

BO_ResultsTest::~BO_ResultsTest()
{
}

void BO_ResultsTest::SetResultTest(double Result)
{
	if (Result < 0)
	{
		//std::cout << "Values will can not is negative \n";
		ResultTest = 0.00;
	}
	else
	{
		ResultTest = Result;
	}
}

double BO_ResultsTest::GetResultTest()
{
	return ResultTest;
}

void BO_ResultsTest::SetTestNumber(int TestNum)
{	
	if (TestNum < 0)
	{
		//std::cout << "Values will can not is negative \n";
		TestNumber = 0;
	}
	else
	{
		TestNumber = TestNum;
	}
}

int BO_ResultsTest::GetTestNumber()
{
	return TestNumber;
}
