#include "pch.h"
#include "BO_Face.h"
#include <iostream>
#include <string>

BO_Face::BO_Face()
{
}

BO_Face::BO_Face(char *FName, BO_FaceParametr * FParameters)
{
	int lengthname = strlen(FName) + 1;
	FaceName = new char[lengthname];
	memset(FaceName, 0, sizeof(FaceName)); //очистка
	for (int i = 0; i < lengthname; i++)
	{
		FaceName[i] = FName[i];
	}

	for (int i = 0; i < 15; i++)
	{
		FaceParameters[i] = FParameters[i];
	}
}

void BO_Face::SetFaceParameters(BO_FaceParametr * FParameters)
{
	for (int i = 0; i < 15; i++)
	{
		FaceParameters[i] = FParameters[i];
	}
}

BO_FaceParametr * BO_Face::GetFaceParameters()
{
	return FaceParameters;
}

void BO_Face::SetFaceName(char * FName)
{
	int lengthname = strlen(FName) + 1;
	FaceName = new char[lengthname];
	memset(FaceName, 0, sizeof(FaceName)); //очистка
	for (int i = 0; i < lengthname; i++)
	{
		FaceName[i] = FName[i];
	}
}

char * BO_Face::GetFaceName()
{
	return FaceName;
}


BO_Face::~BO_Face()
{
	if (FaceName == NULL)
	{
		delete[] FaceName; //освобождение памяти
	}
}
