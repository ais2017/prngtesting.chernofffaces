#pragma once
#include "IGUI.h"
#include "BO_SequenceParameters.h"
#include "BO_ResultsExperiment.h"
#include "BO_Face.h"
#include "BO_Report.h"
#include <string>
#include <string.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <math.h>
#include <errno.h>
#include <cstring>
#include "md5.h"


class BC_ControlComponent : IGUI
{
public:
	BC_ControlComponent();
	~BC_ControlComponent();

	int ValidationOfParameters(BO_SequenceParameters *); //�������� ���������� 
	int OpeningFileAndParsing(BO_SequenceParameters * ); //�������� ����� � ������� ������������������
	//int ExecutionStatisticalTests(BO_SequenceParameters *, BO_ResultsExperiment *, BO_ResultsExperiment *); // ���������� ������ ��� �������������� ������
	//double * StatisticalTests(char *); //���������� �������������� ������
	int ReportGeneration(BO_SequenceParameters *, BO_ResultsExperiment *, BO_ResultsExperiment *, BO_Face *, BO_Face *); //��������� ������
	int FindReport(BO_SequenceParameters *); // ������ ������
	int GenerationFace(BO_Face *, BO_Face *); // ��������� ���
	int request();
	int answer();
	int CalculateHashSum(std::string, std::string &);
};

