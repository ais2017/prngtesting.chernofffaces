#pragma once
#include "IGateWay.h"
//#include <winsqlite/winsqlite3.h>
#include "BO_Face.h"
#include "BO_Report.h"
#include "BO_ResultsExperiment.h"
#include "BO_SequenceParameters.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <string>
#include "/1/AIS/sqlite-amalgamation-3210000/sqlite3.h"

class DB_SQLite :
	public IGateWay
{
public:
	DB_SQLite();
	~DB_SQLite();
	int SaveData(BO_ResultsExperiment *, BO_ResultsExperiment *, BO_SequenceParameters *) override; //οεπεξοπεδελενθε
	int LoadData(BO_ResultsExperiment *, BO_ResultsExperiment *, BO_SequenceParameters *) override;
	int LoadSeqParameters(BO_SequenceParameters *) override;

	int SaveDataFace(BO_Face *, BO_Face *) override;
	int LoadDataFace(BO_Face *, BO_Face *) override;
	int SaveSeqParameters(BO_SequenceParameters *) override;
	int LoadDataReport(BO_Report *) override;
	int SaveDataReport(BO_Report *) override;
};

