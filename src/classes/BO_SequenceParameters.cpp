#include "pch.h"
#include "BO_SequenceParameters.h"
#include <string>
#include <iostream>


BO_SequenceParameters::BO_SequenceParameters()
{
	FileName = new char[0];
	memset(FileName, 0, sizeof(FileName)); //�������
	LengthSequence = 0; //����� ������������������
	NumberOfIterationsVer1 = 0; //���������� �������� ��� 1 ��������
	NumberOfIterationsVer2 = 0; //���������� �������� ��� 2 ��������
	Step = 0;
}

BO_SequenceParameters::BO_SequenceParameters(char * FName, int LenSequence, int NumIterVer1, int NumIterVer2, int step)
{
	int lengthfile = strlen(FName) + 1;
	FileName = new char[lengthfile];
	memset(FileName, 0, sizeof(FileName)); //�������

	strcpy_s(FileName, lengthfile, FName);

	/*for (int i = 0; i < lengthfile; i++)
	{
		FileName[i] = FName[i];
	}*/

	// �������� �� ������������� ��������
	if (LenSequence < 0)
	{
		LengthSequence = 0;
	}
	else
	{
		LengthSequence = LenSequence;
	}

	if (NumIterVer1 < 0)
	{
		NumberOfIterationsVer1 = 0;
	}
	else
	{
		NumberOfIterationsVer1 = NumIterVer1;
	}

	if (NumIterVer2 < 0)
	{
		NumberOfIterationsVer2 = 0;
	}
	else
	{
		NumberOfIterationsVer2 = NumIterVer2;
	}

	if (step < 0)
	{
		Step = 0;
	}
	else
	{
		Step = step;
	}

}

BO_SequenceParameters::~BO_SequenceParameters()
{
	if (FileName == NULL)
	{
		delete[] FileName; //������������ ������ ��� FileName
	}
}

void BO_SequenceParameters::SetFileName(char * FName)
{

	int lengthfile = strlen(FName) + 1;
	FileName = new char[lengthfile];
	memset(FileName, 0, sizeof(FileName)); //�������
	for (int i = 0; i < lengthfile; i++)
	{
		FileName[i] = FName[i];
	}
}

char * BO_SequenceParameters::GetFileName()
{
	return FileName;
}

void BO_SequenceParameters::SetLengthSequence(int LenSequence)
{
	if (LenSequence < 0 )
	{
		LengthSequence = 0;
	}
	else
	{
		LengthSequence = LenSequence;
	}
}

int BO_SequenceParameters::GetLengthSequence()
{
	return LengthSequence;
}

void BO_SequenceParameters::SetNumberOfIterationsVer1(int NumIterVer1)
{
	if (NumIterVer1 < 0)
	{
		NumberOfIterationsVer1 = 0;
	}
	else
	{
		NumberOfIterationsVer1 = NumIterVer1;
	}
}

int BO_SequenceParameters::GetNumberOfIterationsVer1()
{
	return NumberOfIterationsVer1;
}

void BO_SequenceParameters::SetNumberOfIterationsVer2(int NumIterVer2)
{
	if (NumIterVer2 < 0)
	{
		NumberOfIterationsVer2 = 0;
	}
	else
	{
		NumberOfIterationsVer2 = NumIterVer2;
	}
}

int BO_SequenceParameters::GetNumberOfIterationsVer2()
{
	return NumberOfIterationsVer2;
}

void BO_SequenceParameters::SetStep(int step)
{
	if (step < 0)
	{
		Step = 0;
	}
	else
	{
		Step = step;
	}
}

int BO_SequenceParameters::GetStep()
{
	return Step;
}
