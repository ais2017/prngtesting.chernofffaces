#include "pch.h"
#include "BO_ResultsExperiment.h"
#include <string>


BO_ResultsExperiment::BO_ResultsExperiment(BO_ResultsIteration * resultsiteration, int sizearray)
{
	ResultsExperiment = new BO_ResultsIteration[sizearray];
	
	for (int i = 0; i < sizearray; i++)
	{
		ResultsExperiment[i] = resultsiteration[i];
	}
}

BO_ResultsExperiment::~BO_ResultsExperiment()
{
	if (ResultsExperiment == NULL)
	{
		delete[] ResultsExperiment; //освобождение памяти
	}
}

void BO_ResultsExperiment::SetResultsExperiment(BO_ResultsIteration * resultsiteration, int sizearray)
{
	for (int i = 0; i < sizearray; i++)
	{
		ResultsExperiment[i] = resultsiteration[i];
	}
}

BO_ResultsIteration * BO_ResultsExperiment::GetResultsExperiment()
{
	return ResultsExperiment;
}

/*void BO_ResultsExperiment::CallResultsIteration(BO_ResultsTest)
{
}*/
