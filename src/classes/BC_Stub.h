#pragma once
#include "IGateWay.h"
#include "BO_Face.h"
#include "BO_Report.h"
#include "BO_ResultsExperiment.h"
#include "BO_SequenceParameters.h"
#include <iostream>
#include <random>

class BC_Stub :
	public IGateWay
{
public:
	BC_Stub();
	~BC_Stub();
	int SaveSeqParameters(BO_SequenceParameters *) override;
	int SaveData(BO_ResultsExperiment *, BO_ResultsExperiment *, BO_SequenceParameters *) override;
	int SaveDataFace(BO_Face *, BO_Face *) override;
	int SaveDataReport(BO_Report *) override;
	//int LoadData(BO_SequenceParameters *) override;
	int LoadData(BO_ResultsExperiment *, BO_ResultsExperiment *, BO_SequenceParameters *) override;
	int LoadDataFace(BO_Face *, BO_Face *) override;
	int LoadDataReport(BO_Report *) override;
	int LoadSeqParameters(BO_SequenceParameters *) override;
	//int LoadSeqParameters2(BO_SequenceParameters *) override;

};

