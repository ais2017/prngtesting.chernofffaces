#include "BC_ControlComponent.h"

BC_ControlComponent::BC_ControlComponent()
{
}


BC_ControlComponent::~BC_ControlComponent()
{
}

int BC_ControlComponent::ValidationOfParameters(BO_SequenceParameters * SP)
{
	std::ifstream Fin;
	std::ofstream Fout;
	int end;
	Fin.open(SP->GetFileName(), std::ios::binary);
	
	if (strlen(SP->GetFileName()) == 0)
	{
		return 2;
	}

	if (Fin)
	{
		//std::cout << "Successful\n";
	}
	else
	{
		//std::cout << "Error\n";
		return 3;
	}
	Fin.close();

	if (SP->GetStep() == 0)
	{
		return 4;
	}

	if (SP->GetLengthSequence() > 0)
	{
		//������ ������ �����
		Fin.open(SP->GetFileName(), std::ios::binary); //�������� ��� ������
		Fin.seekg(0, std::ios::end);
		end = Fin.tellg();
		Fin.close();
		std::cout << "Size is: " << end << " bytes.\n";
		if (end == 0)
		{
			return 8;
		}

		if (SP->GetLengthSequence() > end) 
		{
			return 6;
		}
	}
	else
	{
		return 5;
	}

	if (SP->GetLengthSequence() < SP->GetStep())
	{
		return 7;
	}

	// ���������� ���������� � ����
	Fout.open("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\parameters", std::ios::binary | std::ios::out);
	Fout << SP->GetFileName() << "\n" <<  SP->GetStep() << "\n" << SP->GetLengthSequence();
	Fout.close();

	SP->SetNumberOfIterationsVer1(SP->GetLengthSequence() / SP->GetStep()); // ������� ���������� �������� ��� 1 ��������
	SP->SetNumberOfIterationsVer2(log(SP->GetLengthSequence()) / log(SP->GetStep())); // ������� ���������� �������� ��� 2 ��������

	return 1;
}

int BC_ControlComponent::OpeningFileAndParsing(BO_SequenceParameters * SP)
{
	
	std::string line;
	int length, tmp = 0;
	/*
	//��� �������������� � �������� �����
	int tmpbit, j, decimalnumber; int tmpintbit[16];

	char tmpstr[1];
	int tmpx;
	*/
	std::ifstream Fin;
	std::ofstream Fout;
	/*
	Fin.open(SP->GetFileName(), std::ios::binary | std::ios::in); //�������� ��� ������
	
	if (Fin)
	{
		//std::cout << "Successful\n";
	}
	else
	{
		//std::cout << "Error\n";
		Fin.close();
		return 0;
	}

	if (Fin.peek() == EOF)
	{
		//std::cout << "File is empty\n";
		Fin.close();
		return(2);
	}

	//�������� ��� ������
	Fout.open("G:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\tmp", std::ios::binary | std::ios::out);
	
	//������
	while (!Fin.eof())
	{
		std::getline(Fin, line);
		length = line.length();
		for (int i = 0; i < length; i++)
		{
			tmpstr[0] = line[i];
			tmpx = atoi(tmpstr);

			j = 0;
			decimalnumber = int(line[i]); // �������� ���������� ��� �������
			//std::cout << "decimalnumber" << decimalnumber << std::endl;

			// �������������� �� 10 � 2 ������� ���������
			while (decimalnumber > 0)
			{
				tmpbit = decimalnumber % 2;
				decimalnumber = decimalnumber / 2;
				tmpintbit[j] = tmpbit;
				j++;
			}
			for (int k = j-1; k+1 > 0; k--)
			{
				//std::cout << "tmpintbit" << tmpintbit[k] << std::endl;
				if (tmpintbit[k] == 1)
				{
					Fout << '1';
				}
				else
				{
					if (tmpintbit[k] == 0)
					{
						Fout << '0';
					}
				}
			}
			Fout << '|'; // �����������
			
		}
		std::cout << line << "\n"; // ������
		std::cout << "length:" << length << "\n"; // �����
	}
	
	Fin.close();
	Fout.close();
	*/
	
	//������ ������ �����
	int sizefile;

	Fin.open(SP->GetFileName(), std::ios::binary); //�������� ��� ������
	Fin.seekg(0, std::ios::end);
	sizefile = Fin.tellg();
	Fin.close();
	//std::cout << "size is: " << end  << " bytes.\n";

	//SP->SetNumberOfIterationsVer1(SP->GetLengthSequence / SP->GetStep()); // ������� ���������� �������� ��� 1 ��������
	//SP->SetNumberOfIterationsVer2(log(SP->GetLengthSequence) / log(SP->GetStep())); // ������� ���������� �������� ��� 2 ��������
	return 1;
}
/*
int BC_ControlComponent::ExecutionStatisticalTests(BO_SequenceParameters * SP, BO_ResultsExperiment * RE1, BO_ResultsExperiment * RE2)
{
	
	//��������� �� ������ ����������� �������������� ������
	double * ptrrestest;

	// ��� �������� ����������� ������
	// ======================================================================================
	BO_ResultsTest RTforRI[15];

	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[SP->GetNumberOfIterationsVer1()];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[SP->GetNumberOfIterationsVer2()];

	// ======================================================================================
	
	std::ifstream Fin;
	std::ofstream Fout;
	int sizefile; //������ �����

	//������ ������ �����
	Fin.open("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\tmp2", std::ios::binary);
	Fin.seekg(0, std::ios::end);
	sizefile = Fin.tellg();
	Fin.close();
	//std::cout << "size is: " << end << " bytes.\n";

	int tmpstep = SP->GetStep();

	SP->SetNumberOfIterationsVer1(sizefile / tmpstep);
	SP->SetNumberOfIterationsVer2(log(sizefile) / log(tmpstep));

	char * valuesteplength = new char[tmpstep];
	
	//��� �������������� ������
	Fin.open("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\tmp2", std::ios::binary); // �������� ����� �� ������
	
	//��� 1 �������
	for (int i = 0; i < (SP->GetNumberOfIterationsVer1()); i++) {
		Fin.seekg(tmpstep*i);
		Fin.read(valuesteplength, tmpstep);
		valuesteplength[tmpstep] = '\0';

		//�������� ���������� �������������� ������ ��� ����� �������� ������ � ���
		ptrrestest = StatisticalTests(valuesteplength);
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(ptrrestest[i]);
			RTforRI[i].SetTestNumber(i+1);
		}
		// ���������� ������� ���� ResultIteration ��� 1 �������� �������������� ������
		resultsiterationVer1[i].SetResultIteration(RTforRI);

		//std::cout << "ValueOfLengthStep1:" << valuesteplength << std::endl;
	}

	//���������� ���������� ������� ���� ResultIteration ��� 1 �������� � ResultsExperiment1
	RE1->SetResultsExperiment(resultsiterationVer1, SP->GetNumberOfIterationsVer1());

	Fin.seekg(0, std::ios::beg); // �������� � ������ �����

	int tmpNumberOfIterationsVer2 = SP->GetNumberOfIterationsVer2();
	//��� 2 ��������
	for (int i = 1; i < tmpNumberOfIterationsVer2+1; i++) {
		Fin.seekg(0, std::ios::beg);
		tmpstep = pow(SP->GetStep(), i);//���������� � �������
		char * valuesteplength2 = new char[tmpstep + 1];
		//std::cout << "stepen:" << tmpstep << std::endl;
		Fin.read(valuesteplength2, tmpstep);
		valuesteplength2[tmpstep] = '\0';
		
		//�������� ���������� �������������� ������ ��� ����� �������� ������ � ���
		ptrrestest = StatisticalTests(valuesteplength2);
		for (int i = 0; i < 15; i++)
		{
			RTforRI[i].SetResultTest(ptrrestest[i]);
			RTforRI[i].SetTestNumber(i+1);
		}
		// ���������� ������� ���� ResultIteration ��� 2 �������� �������������� ������
		resultsiterationVer2[i-1].SetResultIteration(RTforRI);
		//std::cout << "ValueOfLengthStep2:" << valuesteplength2 << std::endl;

		if (valuesteplength2 == NULL)
		{
			delete[] valuesteplength2; //������������ ������
		}
	}

	//���������� ���������� ������� ���� ResultIteration ��� 1 �������� � ResultsExperiment1
	RE2->SetResultsExperiment(resultsiterationVer2, SP->GetNumberOfIterationsVer2());

	Fin.close(); //�������� ����� �� ������

	if (valuesteplength == NULL)
	{
		delete[] valuesteplength; //������������ ������
	}

	delete[] resultsiterationVer1; //������������ ������
	delete[] resultsiterationVer2; //������������ ������

	if (&ptrrestest == NULL)
	{
		delete[] &ptrrestest; //������������ ������
	}
	return 0;
}*/
/*
double * BC_ControlComponent::StatisticalTests(char *)
{
	double * resultstests = new double[15];
	for (int i = 0; i < 15; i++)
	{
		resultstests[i] = i + 0.755;
	}
	return resultstests;
}
*/
int BC_ControlComponent::ReportGeneration(BO_SequenceParameters * SP, BO_ResultsExperiment * RE1, BO_ResultsExperiment * RE2, BO_Face * FacesVer1, BO_Face * FacesVer2)
{
	return 0;
}

int BC_ControlComponent::FindReport(BO_SequenceParameters * SP)
{
	return 0;
}

int BC_ControlComponent::GenerationFace(BO_Face * FacesVer1, BO_Face * FacesVer2)
{
	return 0;
}
int BC_ControlComponent::request()
{
	return 0;
}
int BC_ControlComponent::answer()
{
	return 0;
}

int BC_ControlComponent::CalculateHashSum(std::string filename, std::string &Hushsum)
{
	//======================== ������� ���-����� MD5 ����������� ==========================
	std::string line;
	std::string outputline;
	std::ifstream Fin;

	Fin.open(filename, std::ios::binary | std::ios::in); //�������� ��� ������

	if (Fin)
	{
		//std::cout << "Successful\n";
	}
	else
	{
		//std::cout << "Error\n";
		Fin.close();
		return 0;
	}

	if (Fin.peek() == EOF)
	{
		//std::cout << "File is empty\n";
		Fin.close();
		return(2);
	}

	std::getline(Fin, line);
	while (!Fin.eof())
	{
		std::getline(Fin, line);
		outputline = outputline + line;
		//std::cout << "LINEFIRST:" << line << std::endl; // ������
	}

	Fin.close();
	std::cout << std::endl;
	Hushsum = md5(outputline);
	return 1;
	//=====================================================================================
}
