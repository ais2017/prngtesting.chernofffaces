#include "pch.h"
#include "BO_Report.h"
#include <string>


BO_Report::BO_Report()
{
}

BO_Report::BO_Report(char * RName, double hush, BO_SequenceParameters * parameters, BO_ResultsExperiment * ResSeqVer1,BO_ResultsExperiment * ResSeqVer2, BO_Face *face1, BO_Face *face2)
{
	int lengthname = strlen(RName) + 1;
	ReportName = new char[lengthname]; // ����������� ������ ��������� char
	memset(ReportName, 0, sizeof(ReportName)); //�������
	for (int i=0; i < lengthname; i++)
	{
		ReportName[i] = RName[i];
	}

	if (hush < 0)
	{ 	
		HushSum = 0;
	}
	else
	{
		HushSum = hush;
	}

	Parameters = parameters;
	ResultSequenceVer1 = ResSeqVer1;
	ResultSequenceVer2 = ResSeqVer2;
	FacesVer1 = face1;
	FacesVer2 = face2;
}

void BO_Report::SetReportName(char * RName)
{
	int lengthname = strlen(RName) + 1;
	ReportName = new char[lengthname]; // ����������� ������ ��������� char
	memset(ReportName, 0, sizeof(ReportName)); //�������
	for (int i = 0; i < lengthname; i++)
	{
		ReportName[i] = RName[i];
	}
}

char * BO_Report::GetReportName()
{
	return ReportName;
}

void BO_Report::SetHushSum(int hush)
{
	if (hush < 0)
	{
		HushSum = 0;
	}
	else
	{
		HushSum = hush;
	}
}

int BO_Report::GetHushSum()
{
	return HushSum;
}

void BO_Report::SetParameters(BO_SequenceParameters *parameters)
{
	Parameters = parameters;
}

BO_SequenceParameters * BO_Report::GetParameters()
{
	return Parameters;
}

void BO_Report::SetResultSequenceVer1(BO_ResultsExperiment * ResSeqVer1)
{
	ResultSequenceVer1 = ResSeqVer1;
}

BO_ResultsExperiment * BO_Report::GetResultSequenceVer1()
{
	return ResultSequenceVer1;
}

void BO_Report::SetResultSequenceVer2(BO_ResultsExperiment * ResSeqVer2)
{
	ResultSequenceVer2 = ResSeqVer2;
}

BO_ResultsExperiment * BO_Report::GetResultSequenceVer2()
{
	return ResultSequenceVer2;
}

void BO_Report::SetFacesVer1(BO_Face * face1)
{
	FacesVer1 = face1;
}

void BO_Report::SetFacesVer2(BO_Face * face2)
{
	FacesVer2 = face2;
}

BO_Face * BO_Report::GetFacesVer1()
{
	return FacesVer1;
}

BO_Face * BO_Report::GetFacesVer2()
{
	return FacesVer2;
}


BO_Report::~BO_Report()
{
	if (ReportName == NULL)
	{
		delete[] ReportName; //������������ ������
	}
}
