#pragma once
#include "BO_Face.h"
#include "BO_Report.h"
#include "BO_ResultsExperiment.h"
#include "BO_SequenceParameters.h"

class IGateWay
{
public:
	//IGateWay();
	virtual ~IGateWay() {};
	virtual int SaveDataFace(BO_Face *, BO_Face *) = 0;
	virtual int LoadDataFace(BO_Face *, BO_Face *) = 0;
	virtual int SaveData(BO_ResultsExperiment *, BO_ResultsExperiment *, BO_SequenceParameters *) = 0;
	virtual int LoadData(BO_ResultsExperiment *, BO_ResultsExperiment *, BO_SequenceParameters *) = 0;
	virtual int LoadSeqParameters(BO_SequenceParameters *)=0;
	virtual int SaveSeqParameters(BO_SequenceParameters *) = 0;
	virtual int LoadDataReport(BO_Report *) = 0;
	virtual int SaveDataReport(BO_Report *) = 0;
};

