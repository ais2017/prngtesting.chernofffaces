#include "DB_SQLite.h"
#include <string.h>

DB_SQLite::DB_SQLite()
{
}


DB_SQLite::~DB_SQLite()
{
}

int DB_SQLite::SaveData(BO_ResultsExperiment * RE1, BO_ResultsExperiment * RE2, BO_SequenceParameters *SeqPar)
{
	// ���������� ������������
	BO_ResultsIteration *GetRI;
	BO_ResultsTest *GetRT;
	int REID = 1; //ID ResultsExperiment

	sqlite3* conn;
	// ��������� ������������ ���������
	int rc;
	//� ������ ��������� ���� ����.
	rc = sqlite3_open16(L"F:\\1\\BDFaces.db", &conn);
	//���� ����� ���, �� �� ����� ������.

	sqlite3_stmt *stmt;
	stmt = NULL;
	//============================================================================================================
	std::string tmpstr = "";
	std::string statistic_query = "INSERT INTO `ResultsExperiments` VALUES ";
	GetRI = RE1->GetResultsExperiment();
	for (int i = 0; i < SeqPar->GetNumberOfIterationsVer1(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			// ������ ������
			tmpstr = "(NULL," + std::to_string(REID) + ", " + std::to_string(i + 1) + ", " + std::to_string(j + 1) + ", " + std::to_string(GetRT[j].GetResultTest())+ ")";
			statistic_query = statistic_query + tmpstr;
			if ((i+1 == SeqPar->GetNumberOfIterationsVer1()) && (j+1 == 15))
			{
				statistic_query = statistic_query + ";";
			}
			else
			{
				statistic_query = statistic_query + ",";
			}
			rc = sqlite3_prepare(conn, statistic_query.c_str(), -1, &stmt, 0);
		}
	}
	statistic_query = statistic_query + " COMMIT;";
	//std::cout << "INSERT:"<< statistic_query << std::endl;
	sqlite3_step(stmt);//����������

	//============================================================================================================
	REID = 2;
	//============================================================================================================
	tmpstr = "";
	statistic_query = "INSERT INTO ResultsExperiments VALUES ";
	GetRI = RE2->GetResultsExperiment();
	for (int i = 0; i < SeqPar->GetNumberOfIterationsVer2(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			// ������ ������
			tmpstr = "(NULL," + std::to_string(REID) + ", " + std::to_string(i + 1) + ", " + std::to_string(j + 1) + ", " + std::to_string(GetRT[j].GetResultTest()) + ")";
			statistic_query = statistic_query + tmpstr;
			if ((i + 1 == SeqPar->GetNumberOfIterationsVer2()) && (j + 1 == 15))
			{
				statistic_query = statistic_query + ";";
			}
			else
			{
				statistic_query = statistic_query + ",";
			}
			rc = sqlite3_prepare(conn, statistic_query.c_str(), -1, &stmt, 0);
		}
	}
	statistic_query = statistic_query + " COMMIT;";
	//std::cout << "INSERT2:"<< statistic_query << std::endl;
	sqlite3_step(stmt);//����������
	//============================================================================================================

	
	//SequenceParameters
	//============================================================================================================
	std::string stringforseqpar = "INSERT INTO `SequenceParameters` VALUES (NULL, " + std::string("'") + std::string(SeqPar->GetFileName()) + std::string("'") + ", " + std::to_string(SeqPar->GetLengthSequence()) + ", " + std::to_string(SeqPar->GetNumberOfIterationsVer1()) + ", " + std::to_string(SeqPar->GetNumberOfIterationsVer2()) + ", " + std::to_string(SeqPar->GetStep()) + " );";
	rc = sqlite3_prepare(conn, stringforseqpar.c_str(), -1, &stmt, 0);
	//std::cout << "INSERT:" << stringforseqpar << std::endl;

	sqlite3_step(stmt);//����������

	//============================================================================================================


	//�� ���������� ������ � �����, ���� ���� ��������� ������, � ��� ����� ������ ��������, ���� ����� �������.
	sqlite3_close(conn);
	return rc;
}

int DB_SQLite::LoadData(BO_ResultsExperiment *RE1, BO_ResultsExperiment *RE2, BO_SequenceParameters *SeqPar)
{
	// ���������� ������������
	BO_ResultsIteration *GetRI;
	BO_ResultsTest *GetRT;
	int REID = 1; //ID ResultsExperiment

	sqlite3* conn;
	// ��������� ������������ ���������
	int rc;
	//� ������ ��������� ���� ����.
	rc = sqlite3_open16(L"F:\\1\\BDFaces.db", &conn);
	//���� ����� ���, �� �� ����� ������.

	sqlite3_stmt *stmt;
	//============================================================================================================
	GetRI = RE1->GetResultsExperiment();
	for (int i = 0; i < SeqPar->GetNumberOfIterationsVer1(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			// ������ ������
			std::string statistic_query = "SELECT * FROM ResultsExperiments WHERE ResultsExperimentID = 1 AND ResultsIterationID = " + std::to_string(i+1) + " AND ResultsTest = " +std::to_string(j+1);
			rc = sqlite3_prepare(conn, statistic_query.c_str(), -1, &stmt, 0);
			sqlite3_step(stmt);//����������
			GetRT[j].SetResultTest (sqlite3_column_int(stmt, 4));
			//sqlite3_finalize(stmt);//���������� ����������
		}
	}
	
	//============================================================================================================
	REID = 2;
	//============================================================================================================
	//==============������ �� �������====================
	GetRI = RE1->GetResultsExperiment();
	for (int i = 0; i < SeqPar->GetNumberOfIterationsVer1(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			// ������ ������
			std::cout << "RI[" << i+1 << "]" << "RT[" << j+1 << "]:" << GetRT[j].GetResultTest() << std::endl;
			//sqlite3_finalize(stmt);//���������� ����������
		}
	}
	
	//�������� ����������� ������������� ��� 2 ��������
	GetRI = RE2->GetResultsExperiment();
	for (int i = 0; i < SeqPar->GetNumberOfIterationsVer2(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			// ������ ������
			std::string statistic_query = "SELECT * FROM ResultsExperiments WHERE ResultsExperimentID = 2 AND ResultsIterationID = " + std::to_string(i + 1) + " AND ResultsTest = " + std::to_string(j + 1);
			rc = sqlite3_prepare(conn, statistic_query.c_str(), -1, &stmt, 0);
			sqlite3_step(stmt);//����������
			GetRT[j].SetResultTest(sqlite3_column_int(stmt, 4));
			//sqlite3_finalize(stmt);//���������� ����������
		}
	}
	//�� ���������� ������ � �����, ���� ���� ��������� ������, � ��� ����� ������ ��������, ���� ����� �������.
	sqlite3_close(conn);
	return rc;
}

int DB_SQLite::LoadSeqParameters(BO_SequenceParameters * SeqPar)
{
	sqlite3* conn;
	// ��������� ������������ ���������
	int rc;
	//� ������ ��������� ���� ����.
	rc = sqlite3_open16(L"F:\\1\\BDFaces.db", &conn);
	//���� ����� ���, �� �� ����� ������.

	sqlite3_stmt *stmt;
	stmt = NULL;

	//SequenceParameters
	//============================================================================================================
	std::string stringforseqpar = "SELECT * FROM  SequenceParameters ORDER BY id desc";
	rc = sqlite3_prepare(conn, stringforseqpar.c_str(), -1, &stmt, 0);
	sqlite3_step(stmt);//����������
	std::string strtmp = std::string(reinterpret_cast <const char *>(sqlite3_column_text(stmt, 1)));

	int lenfilename = strlen((char *)(sqlite3_column_text(stmt, 1)))+1;
	char * forfilename = new char[lenfilename];
	for (int i=0; i < lenfilename; i++)
	{
		forfilename[i] = strtmp[i];
	}
	SeqPar->SetFileName(forfilename);
	SeqPar->SetLengthSequence(sqlite3_column_int(stmt, 2));
	SeqPar->SetNumberOfIterationsVer1(sqlite3_column_int(stmt, 3));
	SeqPar->SetNumberOfIterationsVer2(sqlite3_column_int(stmt, 4));
	SeqPar->SetStep(sqlite3_column_int(stmt, 5));
	//sqlite3_step(stmt);//����������
	//============================================================================================================
	std::cout << "SELECT:" << SeqPar->GetFileName() << std::endl;
	std::cout << "SELECT:" << SeqPar->GetLengthSequence() << std::endl;
	std::cout << "SELECT:" << SeqPar->GetNumberOfIterationsVer1() << std::endl;
	std::cout << "SELECT:" << SeqPar->GetNumberOfIterationsVer2() << std::endl;
	std::cout << "SELECT:" << SeqPar->GetStep() << std::endl;

	//�� ���������� ������ � �����, ���� ���� ��������� ������, � ��� ����� ������ ��������, ���� ����� �������.
	sqlite3_close(conn);
	return rc;
}

int DB_SQLite::SaveDataFace(BO_Face *, BO_Face *)
{
	return 0;
}

int DB_SQLite::LoadDataFace(BO_Face *, BO_Face *)
{
	return 0;
}

int DB_SQLite::SaveSeqParameters(BO_SequenceParameters *)
{
	return 0;
}

int DB_SQLite::LoadDataReport(BO_Report *)
{
	return 0;
}

int DB_SQLite::SaveDataReport(BO_Report *)
{
	return 0;
}
