#include "pch.h"
#include "BO_FaceParametr.h"
#include <string>
#include <iostream>

BO_FaceParametr::BO_FaceParametr()
{
	NameParametr = new char[0];
	memset(NameParametr, 0, sizeof(char)); //�������
	Parametr.SetResultTest(0.00);
	Parametr.SetTestNumber(0);
}

BO_FaceParametr::BO_FaceParametr(char * NamePar, BO_ResultsTest FP)
{
	Parametr = FP;
	int lengthname = strlen(NamePar)+1;
	NameParametr = new char[lengthname];
	memset(NameParametr, 0, lengthname * sizeof(char)); //�������
	for (int i = 0; i < lengthname; i++)
	{
		NameParametr[i] = NamePar[i];
	}
}


BO_FaceParametr::~BO_FaceParametr()
{
	if (NameParametr == NULL)
	{
		delete[] NameParametr; //������������ ������
	}
}

void BO_FaceParametr::SetParametr(BO_ResultsTest FP)
{
	Parametr = FP;
}

BO_ResultsTest BO_FaceParametr::GetParametr()
{
	return Parametr;
}

void BO_FaceParametr::SetNameParametr(char *NamePar)
{
	int lengthname = strlen(NamePar) + 1;
	NameParametr = new char[lengthname];
	memset(NameParametr, 0, sizeof(NameParametr)); //�������
	for (int i = 0; i < lengthname; i++)
	{
		NameParametr[i] = NamePar[i];
	}
}

char * BO_FaceParametr::GetNameParametr()
{
	return NameParametr;
}
