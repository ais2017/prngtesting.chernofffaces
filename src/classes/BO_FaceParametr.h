#pragma once
#include "BO_ResultsTest.h"

class BO_FaceParametr
{
	char * NameParametr;
	BO_ResultsTest Parametr;
public:
	BO_FaceParametr();
	BO_FaceParametr(char *, BO_ResultsTest);
	virtual ~BO_FaceParametr();
	void SetParametr(BO_ResultsTest);
	BO_ResultsTest GetParametr();
	void SetNameParametr(char *);
	char * GetNameParametr();
};

