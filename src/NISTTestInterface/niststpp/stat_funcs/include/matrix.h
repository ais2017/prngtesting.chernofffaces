#include <cstddef>

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
       R A N K  A L G O R I T H M  F U N C T I O N  P R O T O T Y P E S 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

class matrix
{
    std::vector<std::vector<bool>> _seq;
    size_t _M;
    size_t _Q;
public:
    matrix          (size_t M, size_t Q);
    size_t			computeRank();
    void			perform_elementary_row_operations(int flag, size_t i);
    void			find_unit_element_and_swap(int flag, size_t i);
    void			swap_rows(size_t i, size_t index);
    size_t			determine_rank(size_t m);
    void			def_matrix(const std::vector<bool> & seq);
};