#include <vector>
#include <array>

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     S T A T I S T I C A L  T E S T  F U N C T I O N  P R O T O T Y P E S 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

double	Frequency(const std::vector<bool>& sequence);
double	BlockFrequency(int M, const std::vector<bool>& sequence);
double	CumulativeSums(const std::vector<bool>& sequence, bool backwards = false);
double	Runs(const std::vector<bool>& sequence);
double	LongestRunOfOnes(const std::vector<bool>& sequence);
double	Rank(const std::vector<bool>& sequence);
double	DiscreteFourierTransform(const std::vector<bool>& sequence);
double	NonOverlappingTemplateMatchings(const std::vector<bool>& seqtest,
                                        const std::vector<bool>& seqtempl,
                                        size_t blockSize);
double	OverlappingTemplateMatchings(const std::vector<bool>& seqtest,
                                     const std::vector<bool>& seqtempl,
                                     size_t blockSize);
double	Universal(const std::vector<bool>& seqence);

double	ApproximateEntropy(const std::vector<bool>& sequence, size_t blockLength);
std::array<double, 8>	RandomExcursions(const std::vector<bool>& seqence);
std::array<double, 18>	RandomExcursionsVariant(const std::vector<bool>& seqence);
double	LinearComplexity(const std::vector<bool>& seqence, size_t blockLength);
std::pair<double, double>	Serial(const std::vector<bool>& seqence, size_t blockLength);
