#include <cmath>

#include "cephes.h"
#include "stat_fncs.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                          F R E Q U E N C Y  T E S T
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

double
Frequency(const std::vector<bool>& sequence)
{
    static const double sqrt2 = 1.41421356237309504880;
	double	sum = 0.0;
	for ( auto i : sequence)
		sum += 2* (i ? 1 : 0) - 1;
    return erfc((fabs(sum)/sqrt(sequence.size()))/sqrt2);
}
