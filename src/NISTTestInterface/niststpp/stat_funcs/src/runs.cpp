#include <cmath>

#include "cephes.h"
#include "stat_fncs.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                              R U N S  T E S T 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

double
Runs(const std::vector<bool>& seq)
{
	int S = 0;
	for (auto k : seq)
		if ( k )
			S++;
	double pi = static_cast<double >(S) / static_cast<double >(seq.size());
    double V = 1;
    if(!seq.empty())
    {
        for (auto k = seq.cbegin() + 1; k != seq.cend(); ++k)
            if (*k != *(k-1))
                V++;
    }
    return erfc(fabs(V - 2.0 * seq.size() * pi * (1-pi)) / (2.0 * pi * (1-pi) * sqrt(2*seq.size())));
}
