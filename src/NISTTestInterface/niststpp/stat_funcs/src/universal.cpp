#include <cmath>

#include "cephes.h"
#include "stat_fncs.h"
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                         U N I V E R S A L  T E S T
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

double
Universal(const std::vector<bool>& seq)
{
	double	expected_value[17] = { 0, 0, 0, 0, 0, 0, 5.2177052, 6.1962507, 7.1836656,
				8.1764248, 9.1723243, 10.170032, 11.168765,
				12.168070, 13.167693, 14.167488, 15.167379 };
	double   variance[17] = { 0, 0, 0, 0, 0, 0, 2.954, 3.125, 3.238, 3.311, 3.356, 3.384,
				3.401, 3.410, 3.416, 3.419, 3.421 };
	
	/* * * * * * * * * ** * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * THE FOLLOWING REDEFINES L, SHOULD THE CONDITION:     n >= 1010*2^L*L       *
	 * NOT BE MET, FOR THE BLOCK LENGTH L.                                        *
	 * * * * * * * * * * ** * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	size_t L = 5;
	if ( seq.size() >= 387840 )     L = 6;
	if ( seq.size() >= 904960 )     L = 7;
	if ( seq.size() >= 2068480 )    L = 8;
	if ( seq.size() >= 4654080 )    L = 9;
	if ( seq.size() >= 10342400 )   L = 10;
	if ( seq.size() >= 22753280 )   L = 11;
	if ( seq.size() >= 49643520 )   L = 12;
	if ( seq.size() >= 107560960 )  L = 13;
	if ( seq.size() >= 231669760 )  L = 14;
	if ( seq.size() >= 496435200 )  L = 15;
	if ( seq.size() >= 1059061760 ) L = 16;

    auto Q = static_cast<size_t>(10 * pow(2, L));
	size_t K = ((seq.size() / L) - Q);	 		    /* BLOCKS TO TEST */
    std::vector<long> T(static_cast<unsigned long>(pow(2, L)), 0);

	/* COMPUTE THE EXPECTED:  Formula 16, in Marsaglia's Paper */
    double sum = 0.0;
	for (size_t i=1; i<=Q; i++ ) {		/* INITIALIZE TABLE */
		size_t decRep = 0;
		for (size_t j=0; j<L; j++ )
		    if(seq[(i-1)*L+j])
			    decRep += (long)pow(2, L-1-j);
		T[decRep] = i;
	}
	for (size_t i=Q+1; i<=Q+K; i++ ) { 	/* PROCESS BLOCKS */
		size_t decRep = 0;
		for (size_t j=0; j<L; j++ )
		    if(seq[(i-1)*L+j])
			    decRep += (long)pow(2, L-1-j);
		sum += log(i - T[decRep])/log(2);
		T[decRep] = i;
	}
	double c = 0.7 - 0.8/(double)L + (4 + 32/(double)L)*pow(K, -3/(double)L)/15;
    return erfc(fabs((sum/(double)K)-expected_value[L])/(sqrt(2) * (c * sqrt(variance[L]/(double)K))));
}