#include <vector>
#include <stdexcept>
#include "matrix.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
R A N K  A L G O R I T H M  R O U T I N E S
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#define	MATRIX_FORWARD_ELIMINATION	0
#define	MATRIX_BACKWARD_ELIMINATION	1

size_t
matrix::computeRank()
{
	size_t m=std::min(_M,_Q);
	
	/* FORWARD APPLICATION OF ELEMENTARY ROW OPERATIONS */ 
	for (size_t i=0; i<m-1; i++ ) {
		if ( _seq[i][i] )
			perform_elementary_row_operations(MATRIX_FORWARD_ELIMINATION, i);
		else { 	/* matrix[i][i] = 0 */
			find_unit_element_and_swap(MATRIX_FORWARD_ELIMINATION, i);
			perform_elementary_row_operations(MATRIX_FORWARD_ELIMINATION, i);
		}
	}

	/* BACKWARD APPLICATION OF ELEMENTARY ROW OPERATIONS */ 
	for (size_t i=m-1; i>0; i-- ) {
		if ( _seq[i][i] )
			perform_elementary_row_operations(MATRIX_BACKWARD_ELIMINATION, i);
		else { 	/* matrix[i][i] = 0 */
			find_unit_element_and_swap(MATRIX_BACKWARD_ELIMINATION, i);
			perform_elementary_row_operations(MATRIX_BACKWARD_ELIMINATION, i);
		}
	}

	return determine_rank(m);
}

void
matrix::perform_elementary_row_operations(int flag, size_t i)
{
	if ( flag == MATRIX_FORWARD_ELIMINATION ) {
		for (size_t j=i+1; j<_M;  j++ )
			if ( _seq[j][i] )
				for (size_t k=i; k<_Q; k++ )
					_seq[j][k] = ( _seq[j][k] != _seq[i][k] );
	}
	else {
		for (int j=i-1; j>=0;  j-- )
			if ( _seq[j][i] )
				for (int k=0; k<_Q; k++ )
					_seq[j][k] = (_seq[j][k] != _seq[i][k]);
	}
}

void
matrix::find_unit_element_and_swap(int flag, size_t i)
{ 
	int	index;
	
	if ( flag == MATRIX_FORWARD_ELIMINATION ) {
		index = i+1;
		while ( (index < _M) && ( !_seq[index][i] ) )
			index++;
		if ( index < _M )
			swap_rows(i, index);
	}
	else {
		index = i-1;
		while ( (index >= 0) && ( !_seq[index][i] ) )
			index--;
		if ( index >= 0 )
			swap_rows(i, index);
	}
}

void
matrix::swap_rows(size_t i, size_t index)
{
	for (size_t p=0; p<_Q; p++ ) {
	    std::swap(_seq[i][p], _seq[index][p]);
	}
}

size_t
matrix::determine_rank(size_t m)
{
	int		i, j, allZeroes;
	
	/* DETERMINE RANK, THAT IS, COUNT THE NUMBER OF NONZERO ROWS */
	
	size_t rank = m;
	for ( i=0; i<_M; i++ ) {
		allZeroes = 1; 
		for ( j=0; j<_Q; j++)  {
			if ( _seq[i][j] ) {
				allZeroes = 0;
				break;
			}
		}
		if ( allZeroes == 1 )
			rank--;
	} 
	
	return rank;
}

matrix::matrix (size_t M, size_t Q) : _seq(M, std::vector<bool>(Q)), _M(M), _Q(Q)
{}

void
matrix::def_matrix(const std::vector<bool> & seq)
{
	int		i,j;
	if(seq.size() < _M * _Q)
    {
	    throw std::logic_error("Sequence is too small to fill ( " + std::to_string(_M) + " x " + std::to_string(_Q)
	                           + " ) matrix");
    }
	for ( i=0; i<_M; i++ )
		for ( j=0; j<_Q; j++ )
			_seq[i][j] = seq[j+i*_M];
}
