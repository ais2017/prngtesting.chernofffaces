#include <cmath>

#include "cephes.h"
#include "stat_fncs.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                    B L O C K  F R E Q U E N C Y  T E S T
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

double
BlockFrequency(int M, const std::vector<bool>& sequence)
{
	double	sum = 0.0;
	
	auto N = sequence.size()/M;
	for (size_t i=0; i<N; i++ ) {
		int blockSum = 0;
		for (size_t j=0; j<M; j++ )
			blockSum += sequence[j+i*M] ? 1 : 0;
		sum += pow(((double)blockSum/(double)M) - 0.5, 2);
	}
	return cephes_igamc(N/2.0, (4.0 * M * sum)/2.0);
}
