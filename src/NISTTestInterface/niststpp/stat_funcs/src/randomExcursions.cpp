#include <cmath>

#include "cephes.h"
#include "stat_fncs.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                     R A N D O M  E X C U R S I O N S  T E S T
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

std::array<double, 8>
RandomExcursions(const std::vector<bool>& seq)
{
	int		b, J, x;
	size_t		cycleStart, cycleStop;
	int		stateX[8] = { -4, -3, -2, -1, 1, 2, 3, 4 };
	int		counter[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
	double	sum, nu[6][8];
	double	pi[5][6] = { {0.0000000000, 0.00000000000, 0.00000000000, 0.00000000000, 0.00000000000, 0.0000000000}, 
						 {0.5000000000, 0.25000000000, 0.12500000000, 0.06250000000, 0.03125000000, 0.0312500000},
						 {0.7500000000, 0.06250000000, 0.04687500000, 0.03515625000, 0.02636718750, 0.0791015625},
						 {0.8333333333, 0.02777777778, 0.02314814815, 0.01929012346, 0.01607510288, 0.0803755143},
						 {0.8750000000, 0.01562500000, 0.01367187500, 0.01196289063, 0.01046752930, 0.0732727051} };
	std::vector<int> S_k(seq.size(), 0);
    std::vector<size_t> cycle(std::max<size_t>(1000, seq.size()/100), 0);

	J = 0; 					/* DETERMINE CYCLES */
	S_k[0] = seq[0] ? 1 : -1;
	for(size_t i=1; i<seq.size(); i++ ) {
		S_k[i] = S_k[i-1] + (seq[i] ? 1 : - 1);
		if ( S_k[i] == 0 ) {
			J++;
			if ( J > std::max<size_t>(1000, seq.size()/100) ) {
			    throw std::logic_error("ERROR IN FUNCTION randomExcursions:  EXCEEDING THE MAX NUMBER OF CYCLES EXPECTED");
			}
			cycle[J] = i;
		}
	}
	if ( S_k[seq.size()-1] != 0 )
		J++;
	cycle[J] = seq.size();

    cycleStart = 0;
    cycleStop  = cycle[1];
    for (auto &k : nu)
        for (double &i : k)
            i = 0.;
    for (size_t j=1; j<=J; j++ ) {                           /* FOR EACH CYCLE */
        for (int &i : counter)
            i = 0;
        for (size_t i=cycleStart; i<cycleStop; i++ ) {
            if ( (S_k[i] >= 1 && S_k[i] <= 4) || (S_k[i] >= -4 && S_k[i] <= -1) ) {
                if ( S_k[i] < 0 )
                    b = 4;
                else
                    b = 3;
                counter[S_k[i]+b]++;
            }
        }
        cycleStart = cycle[j]+1;
        if ( j < J )
            cycleStop = cycle[j+1];

        for (size_t i=0; i<8; i++ ) {
            if ( (counter[i] >= 0) && (counter[i] <= 4) )
                nu[counter[i]][i]++;
            else if ( counter[i] >= 5 )
                nu[5][i]++;
        }
    }
    std::array<double, 8> ret{};
    for (size_t i=0; i<8; i++ ) {
        x = stateX[i];
        sum = 0.;
        for (size_t k=0; k<6; k++ )
            sum += pow(nu[k][i] - J*pi[(int)fabs(x)][k], 2) / (J*pi[(int)fabs(x)][k]);
        ret[i] = cephes_igamc(2.5, sum/2.0);
    }
    return ret;
}
