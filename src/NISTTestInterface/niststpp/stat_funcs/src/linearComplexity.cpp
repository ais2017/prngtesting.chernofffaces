#include <cmath>

#include "cephes.h"
#include "stat_fncs.h"

size_t linearComplexity(const std::vector<bool>& seq);

double
LinearComplexity(const std::vector<bool>& seq, size_t blockLength)
{
    static const size_t degreeOfFreedom = 6;
	int       i, sign;
	double    T_, chi2;
    std::array<double, degreeOfFreedom + 1> pi { 0.01047, 0.03125, 0.12500, 0.50000, 0.25000, 0.06250, 0.020833 };
    std::array<double, degreeOfFreedom + 1> nu {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

	size_t N = seq.size() / blockLength;

	for (size_t ii=0; ii<N; ii++ ) {
		size_t L = linearComplexity(std::vector<bool>(seq.begin() + ii * blockLength, seq.begin() + (ii + 1) *blockLength));

		if ( (blockLength + 1) % 2 == 0 )
			sign = -1;
		else 
			sign = 1;

		double mean = blockLength/2.0 + (9.0+sign)/36.0 - (blockLength/3.0 + 2.0/9.0)/pow(2, blockLength);
		T_ = (sign) * (L - mean) + 2.0/9.0;
		
		if ( T_ <= -2.5 )
			nu[0]++;
		else if ( T_ > -2.5 && T_ <= -1.5 )
			nu[1]++;
		else if ( T_ > -1.5 && T_ <= -0.5 )
			nu[2]++;
		else if ( T_ > -0.5 && T_ <= 0.5 )
			nu[3]++;
		else if ( T_ > 0.5 && T_ <= 1.5 )
			nu[4]++;
		else if ( T_ > 1.5 && T_ <= 2.5 )
			nu[5]++;
		else
			nu[6]++;
	}
	chi2 = 0.00;
	for ( i=0; i<degreeOfFreedom+1; i++ )
		chi2 += pow(nu[i]-N*pi[i], 2) / (N*pi[i]);
	return cephes_igamc(degreeOfFreedom/2.0, chi2/2.0);
}

size_t linearComplexity(const std::vector<bool>& seq)
{
    size_t L = 0;
    {
        std::vector<bool> T(seq.size(), false);
        std::vector<bool> P(seq.size(), false);
        std::vector<bool> B_(seq.size(), false);
        std::vector<bool> C(seq.size(), false);
        C[0] = true;
        B_[0] = true;
        long m = -1;
        for (size_t N_ = 0; N_ < seq.size(); ++N_)
        {

            int d = seq[N_] ? 1 : 0;
            for (size_t i = 1; i <= L; i++)
                if (C[i])
                    d += seq[N_ - i];
            d = d % 2;
            if (d == 1)
            {
                for (size_t i = 0; i < seq.size(); i++)
                {
                    T[i] = C[i];
                    P[i] = false;
                }
                for (size_t j = 0; j < seq.size(); j++)
                    if (B_[j])
                        P[j + N_ - m] = true;
                for (size_t i = 0; i < seq.size(); i++)
                    C[i] = (C[i] != P[i]);
                if (L <= N_ / 2)
                {
                    L = N_ + 1 - L;
                    m = N_;
                    for (size_t i = 0; i < seq.size(); i++)
                        B_[i] = T[i];
                }
            }
        }
    }
    return L;
}