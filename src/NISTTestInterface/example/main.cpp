//
// Created by svuatoslav on 12/16/18.
//

#include "NISTTests.h"

#include <iostream>
#include <memory>
#include <iomanip>

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cerr << "Usage:\n" << argv[0] << " configFile [sequenceFiles...]" << std::endl;
        return 1;
    }
    std::vector<std::pair<std::string, std::shared_ptr<TestInterface>>> tests;
    {
        std::string testName;
        {
            testName = "Approximate Entropy";
            try
            {
                tests.emplace_back(testName, std::make_shared<ApproximateEntropyTest>(argv[1]));
            }
            catch (std::exception &ex)
            {
                std::cerr << "Failed to create " << testName << " test:\n\t" << ex.what() << std::endl;
                return 1;
            }
        }
        {
            testName = "Discrete Fourier Transform";
            try
            {
                tests.emplace_back(testName, std::make_shared<DiscreteFourierTransformTest>());
            }
            catch (std::exception &ex)
            {
                std::cerr << "Failed to create " << testName << " test:\n\t" << ex.what() << std::endl;
                return 1;
            }
        }
        {
            testName = "Non-overlapping Template Matching";
            try
            {
                tests.emplace_back(testName, std::make_shared<NonoverlappingTemplateMatchingTest>(argv[1]));
            }
            catch (std::exception &ex)
            {
                std::cerr << "Failed to create " << testName << " test:\n\t" << ex.what() << std::endl;
                return 1;
            }
        }
        {
            testName = "Overlapping Template Matching";
            try
            {
                tests.emplace_back(testName, std::make_shared<OverlappingTemplateMatchingTest>(argv[1]));
            }
            catch (std::exception &ex)
            {
                std::cerr << "Failed to create " << testName << " test:\n\t" << ex.what() << std::endl;
                return 1;
            }
        }
        {
            testName = "Random Excursions Variant";
            try
            {
                tests.emplace_back(testName, std::make_shared<RandomExcursionVariantTest>());
            }
            catch (std::exception &ex)
            {
                std::cerr << "Failed to create " << testName << " test:\n\t" << ex.what() << std::endl;
                return 1;
            }
        }
        {
            testName = "Serial";
            try
            {
                tests.emplace_back(testName, std::make_shared<SerialTest>(argv[1]));
            }
            catch (std::exception &ex)
            {
                std::cerr << "Failed to create " << testName << " test:\n\t" << ex.what() << std::endl;
                return 1;
            }
        }
        {
            testName = "Block Frequency";
            try
            {
                tests.emplace_back(testName, std::make_shared<FrequencyBlockTest>(argv[1]));
            }
            catch (std::exception &ex)
            {
                std::cerr << "Failed to create " << testName << " test:\n\t" << ex.what() << std::endl;
                return 1;
            }
        }
        {
            testName = "Linear Complexity";
            try
            {
                tests.emplace_back(testName, std::make_shared<LinearComplexityTest>(argv[1]));
            }
            catch (std::exception &ex)
            {
                std::cerr << "Failed to create " << testName << " test:\n\t" << ex.what() << std::endl;
                return 1;
            }
        }
        {
            testName = "Rank";
            try
            {
                tests.emplace_back(testName, std::make_shared<RankTest>());
            }
            catch (std::exception &ex)
            {
                std::cerr << "Failed to create " << testName << " test:\n\t" << ex.what() << std::endl;
                return 1;
            }
        }
        {
            testName = "Universal";
            try
            {
                tests.emplace_back(testName, std::make_shared<UniversalTest>());
            }
            catch (std::exception &ex)
            {
                std::cerr << "Failed to create " << testName << " test:\n\t" << ex.what() << std::endl;
                return 1;
            }
        }
        {
            testName = "Cumulative Sums";
            try
            {
                tests.emplace_back(testName, std::make_shared<CumulativeSumsTest>());
            }
            catch (std::exception &ex)
            {
                std::cerr << "Failed to create " << testName << " test:\n\t" << ex.what() << std::endl;
                return 1;
            }
        }
        {
            testName = "Monobit Frequency";
            try
            {
                tests.emplace_back(testName, std::make_shared<FrequencyMonobitTest>());
            }
            catch (std::exception &ex)
            {
                std::cerr << "Failed to create " << testName << " test:\n\t" << ex.what() << std::endl;
                return 1;
            }
        }
        {
            testName = "Longest Run of Ones";
            try
            {
                tests.emplace_back(testName, std::make_shared<LongestRunTest>());
            }
            catch (std::exception &ex)
            {
                std::cerr << "Failed to create " << testName << " test:\n\t" << ex.what() << std::endl;
                return 1;
            }
        }
        {
            testName = "Random Excursions";
            try
            {
                tests.emplace_back(testName, std::make_shared<RandomExcursionTest>());
            }
            catch (std::exception &ex)
            {
                std::cerr << "Failed to create " << testName << " test:\n\t" << ex.what() << std::endl;
                return 1;
            }
        }
        {
            testName = "Runs";
            try
            {
                tests.emplace_back(testName, std::make_shared<RunsTest>());
            }
            catch (std::exception &ex)
            {
                std::cerr << "Failed to create " << testName << " test:\n\t" << ex.what() << std::endl;
                return 1;
            }
        }
    }
    std::cout.precision(3);
    for(int i = 2; i < argc; ++i)
    {
        std::cout << std::setw(130) << std::setfill('#') << '\n' << std::endl;
        std::cout << std::setfill(' ');
        std::cout << "\"" << argv[i] << "\":\n\n";
        for(auto & test : tests)
        {
            std::cout << "\t" << test.first << ":\n";
            std::cout << std::setw(10) << "min"    << std::setw(10) << "max"    << std::setw(10) << "avg"
                      << std::setw(10) << "mean"   << std::setw(10) << "random" << std::setw(10) << "first"
                      << std::setw(10) << "second" << std::setw(10) << "third"  << std::setw(10) << "fourth"
                      << std::setw(10) << "fifth"  << std::setw(10) << "sixth"  << std::setw(10) << "seventh"
                      << std::setw(10) << "eighth\n";
            bool success = true;
            try
            {
                test.second->test(argv[i]);
            }
            catch (...)
            {
                success = false;
            }
            if(success)
            {
                std::cout << std::setw(10) << test.second->getResult(TestInterface::modes::min);
                std::cout << std::setw(10) << test.second->getResult(TestInterface::modes::max);
                std::cout << std::setw(10) << test.second->getResult(TestInterface::modes::avg);
                std::cout << std::setw(10) << test.second->getResult(TestInterface::modes::mean);
                std::cout << std::setw(10) << test.second->getResult(TestInterface::modes::random);
                for(int j = 0; j < 8; ++j)
                {
                    std::cout << std::setw(10) << test.second->getResult(
                            static_cast<TestInterface::modes>(TestInterface::modes::val0 + j));
                }
            }
            else
            {
                for(int j = 0; j < 13; ++j)
                {
                    std::cout << std::setw(10) << "?!@#$";
                }
            }
            std::cout << "\n" << std::endl;
            test.second->dropResults();
        }
    }
    std::cout << "Tested " << argc - 2 << " sequences" << std::endl;
    return 0;
}