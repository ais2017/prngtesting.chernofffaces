//
// Created by svuatoslav on 12/15/18.
//

#include "TestInterface.h"
#include <fstream>
#include <cstring>
#include <algorithm>
#include <numeric>

std::vector<bool> TestInterface::readSequence(const std::string &sequenceFile)
{
    static const size_t bufSize = 4096;
    std::ifstream in;
    in.open(sequenceFile, std::ios::binary | std::ios::in);
    if(!in)
    {
        throw std::runtime_error("Failed to open sequence file \"" + sequenceFile + "\" : " + strerror(errno));
    }
    std::vector<bool> ret;
    char buf[bufSize];
    while(!in.eof())
    {
        in.read(buf, sizeof(buf));
        if(in.bad())
        {
            throw std::runtime_error("Error reading bit sequence: " + std::string(strerror(errno)));
        }
        auto num = in.gcount();
        ret.reserve(ret.size() + num * 8);
        for(size_t i = 0; i < num; ++i)
        {
            for(int j = 7; j >= 0; --j)
            {
                ret.push_back((buf[i] & (1 << j)) != 0);
            }
        }
    }
    return ret;
}

double TestInterface::getResult(TestInterface::modes mode) const
{
    if(_lastResult.empty())
    {
        throw std::logic_error("No results available");
    }
    if(mode >= val0)
    {
        return _lastResult[(mode - val0) % _lastResult.size()];
    }
    switch (mode)
    {
        case modes::min:
            return *std::min_element(_lastResult.cbegin(), _lastResult.cend());
        case modes::max:
            return *std::max_element(_lastResult.cbegin(), _lastResult.cend());
        case modes::avg:
            return std::accumulate(_lastResult.begin(), _lastResult.end(), 0.0)/_lastResult.size();
        case modes::mean:
        {
            auto pvals = _lastResult;
            std::sort(pvals.begin(), pvals.end());
            return pvals[pvals.size()/2];
        }
        case modes::random:
            return _lastResult[rand() % _lastResult.size()];
        default:
            throw std::logic_error("Unknown mode: " + std::to_string(mode));
    }
}

void TestInterface::test(const std::vector<bool> &sequence) const
{
    _lastResult = runTest(sequence);
}

void TestInterface::dropResults()
{
    _lastResult.clear();
    _lastResult.shrink_to_fit();
}
