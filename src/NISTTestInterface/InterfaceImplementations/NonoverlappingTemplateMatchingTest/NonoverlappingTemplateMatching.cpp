//
// Created by svuatoslav on 12/15/18.
//

#include "NonoverlappingTemplateMatching.h"
#include "stat_fncs.h"
#include "XMLParse.h"

#include <fstream>
#include <cstring>


const char* NonoverlappingTemplateMatchingTest::TestName = "NonoverlappingTemplateMatching";

std::vector<double> NonoverlappingTemplateMatchingTest::runTest(const std::vector<bool> &sequence) const
{
    if(sequence.size() < _blockSize)
    {
        throw std::runtime_error("Sequence size must be bigger than block size");
    }
    if(_templates.empty())
    {
        throw std::logic_error("Test is not loaded properly");
    }
    std::vector<double> ret;
    ret.reserve(_templates.size());
    for(auto & i : _templates)
    {
        auto vec = readSequence(i.first);
        vec.resize(i.second);
        ret.push_back(NonOverlappingTemplateMatchings(sequence, vec, _blockSize));
    }
    return ret;
}

void NonoverlappingTemplateMatchingTest::load(const std::string &config)
{
    XMLParse parser;
    parser.load(config, TestName);
    auto tmpsize = stoul(parser.attribute("BlockSize"));
    if(tmpsize == 0)
    {
        throw std::runtime_error("Block Size must be positive");
    }

    auto seqfile = parser.attribute("Templates");
    std::ifstream in(seqfile);
    if(!in)
    {
        throw std::runtime_error("Failed to open sequence listing file \"" + seqfile + "\" : " + strerror(errno));
    }
    decltype(_templates) vec;
    while(!in.eof())
    {
        std::string file;
        std::getline(in, file);
        if(in.bad())
        {
            throw std::runtime_error("Failed to read sequence listing file : " + std::string(strerror(errno)));
        }
        size_t bitnum = 0;
        size_t pos = file.find_last_of('*');
        if(pos != std::string::npos)
        {
            bitnum = std::stoul(file.substr(pos + 1));
            file = file.substr(0, pos);
        }
        if(!file.empty())
        {
            std::ifstream ifstream(file, std::ios::binary | std::ios::ate);
            if(ifstream)
            {
                auto size = ifstream.tellg();
                if(bitnum > size * 8 || bitnum == 0)
                {
                    bitnum = static_cast<size_t>(size * 8);
                }
                if(tmpsize >= bitnum)
                {
                    vec.emplace_back(file, bitnum == 0 ? size * 8 : bitnum);
                }
            }
        }
    }
    if(vec.empty())
    {
        throw std::runtime_error("No template files with sequence smaller or equal to block size found");
    }
    _blockSize = tmpsize;
    _templates = vec;
}

NonoverlappingTemplateMatchingTest::NonoverlappingTemplateMatchingTest(const std::string &config)
{
    load(config);
}
