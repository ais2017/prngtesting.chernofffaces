//
// Created by svuatoslav on 12/15/18.
//

#include "CumulativeSums.h"
#include "stat_fncs.h"

std::vector<double> CumulativeSumsTest::runTest(const std::vector<bool> &sequence) const
{
    return {CumulativeSums(sequence, false), CumulativeSums(sequence, true)};
}