cmake_minimum_required(VERSION 3.7)
project(niststiface)

set(CMAKE_CXX_STANDARD 17)

add_library(CumulativeSums ../include/CumulativeSums.h CumulativeSums.cpp)
target_link_libraries(CumulativeSums TestInterface niststpp)