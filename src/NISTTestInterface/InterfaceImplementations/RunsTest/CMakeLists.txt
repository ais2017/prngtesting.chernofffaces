cmake_minimum_required(VERSION 3.7)
project(niststiface)

set(CMAKE_CXX_STANDARD 17)

add_library(Runs ../include/Runs.h Runs.cpp)
target_link_libraries(Runs TestInterface niststpp)