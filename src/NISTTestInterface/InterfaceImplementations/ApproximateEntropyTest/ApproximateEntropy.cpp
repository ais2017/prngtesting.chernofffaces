//
// Created by svuatoslav on 12/15/18.
//

#include "ApproximateEntropy.h"
#include "stat_fncs.h"
#include "XMLParse.h"

const char* ApproximateEntropyTest::TestName = "ApproximateEntropy";

std::vector<double> ApproximateEntropyTest::runTest(const std::vector<bool> &sequence) const
{
    if(_blockSize == 0)
    {
        throw std::logic_error( std::string(TestName) + " test is not loaded");
    }
    return {ApproximateEntropy(sequence, _blockSize)};
}

void ApproximateEntropyTest::load(const std::string &config)
{
    XMLParse parser;
    parser.load(config, TestName);
    auto attr = parser.attribute("BlockSize");
    auto tmp = std::stoul(attr);
    if(tmp == 0)
    {
        throw std::logic_error("Block size must be positive");
    }
    _blockSize = tmp;
}

ApproximateEntropyTest::ApproximateEntropyTest(const std::string &config)
{
    load(config);
}
