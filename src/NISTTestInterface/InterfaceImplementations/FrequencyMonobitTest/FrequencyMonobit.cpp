//
// Created by svuatoslav on 12/15/18.
//

#include "FrequencyMonobit.h"
#include "stat_fncs.h"

std::vector<double> FrequencyMonobitTest::runTest(const std::vector<bool> &sequence) const
{
    return {Frequency(sequence)};
}