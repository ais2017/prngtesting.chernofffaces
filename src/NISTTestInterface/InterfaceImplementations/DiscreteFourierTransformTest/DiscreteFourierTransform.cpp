//
// Created by svuatoslav on 12/15/18.
//

#include "DiscreteFourierTransform.h"
#include "stat_fncs.h"

std::vector<double> DiscreteFourierTransformTest::runTest(const std::vector<bool> &sequence) const
{
    return {DiscreteFourierTransform(sequence)};
}