//
// Created by svuatoslav on 12/15/18.
//

#ifndef NISTSTIFACE_UNIVERSAL_H
#define NISTSTIFACE_UNIVERSAL_H

#include "TestInterface.h"

class UniversalTest : public TestInterface
{
public:
    UniversalTest() = default;
    ~UniversalTest() override = default;
    std::vector<double> runTest(const std::vector<bool> &sequence) const override;
};

#endif //NISTSTIFACE_UNIVERSAL_H
