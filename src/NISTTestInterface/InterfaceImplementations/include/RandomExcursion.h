//
// Created by svuatoslav on 12/15/18.
//

#ifndef NISTSTIFACE_RANDOMEXCURSION_H
#define NISTSTIFACE_RANDOMEXCURSION_H

#include "TestInterface.h"

class RandomExcursionTest : public TestInterface
{
public:
    RandomExcursionTest() = default;
    ~RandomExcursionTest() override = default;
    std::vector<double> runTest(const std::vector<bool> &sequence) const override;
};

#endif //NISTSTIFACE_RANDOMEXCURSION_H
