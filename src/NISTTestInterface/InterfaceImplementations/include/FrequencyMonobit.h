//
// Created by svuatoslav on 12/15/18.
//

#ifndef NISTSTIFACE_FREQUENCYMONOBIT_H
#define NISTSTIFACE_FREQUENCYMONOBIT_H

#include "TestInterface.h"

class FrequencyMonobitTest : public TestInterface
{
public:
    FrequencyMonobitTest() = default;
    ~FrequencyMonobitTest() override = default;
    std::vector<double> runTest(const std::vector<bool> &sequence) const override;
};

#endif //NISTSTIFACE_FREQUENCYMONOBIT_H
