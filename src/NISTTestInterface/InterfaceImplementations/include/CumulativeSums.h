//
// Created by svuatoslav on 12/15/18.
//

#ifndef NISTSTIFACE_CUMULATIVESUMS_H
#define NISTSTIFACE_CUMULATIVESUMS_H

#include "TestInterface.h"

class CumulativeSumsTest : public TestInterface
{
public:
    CumulativeSumsTest() = default;
    ~CumulativeSumsTest() override = default;
    std::vector<double> runTest(const std::vector<bool> &sequence) const override;
};

#endif //NISTSTIFACE_CUMULATIVESUMS_H
