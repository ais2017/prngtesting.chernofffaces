//
// Created by svuatoslav on 12/15/18.
//

#ifndef NISTSTIFACE_RANDOMEXCURSIONVARIANT_H
#define NISTSTIFACE_RANDOMEXCURSIONVARIANT_H

#include "TestInterface.h"

class RandomExcursionVariantTest : public TestInterface
{
public:
    RandomExcursionVariantTest() = default;
    ~RandomExcursionVariantTest() override = default;
    std::vector<double> runTest(const std::vector<bool> &sequence) const override;
};

#endif //NISTSTIFACE_RANDOMEXCURSIONVARIANT_H
