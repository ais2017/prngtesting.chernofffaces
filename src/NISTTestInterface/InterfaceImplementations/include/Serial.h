//
// Created by svuatoslav on 12/15/18.
//

#ifndef NISTSTIFACE_SERIAL_H
#define NISTSTIFACE_SERIAL_H

#include "TestInterface.h"
#include <string>

class SerialTest : public TestInterface
{
    unsigned long _blockSize = 0;
    static const char* TestName;
public:
    explicit SerialTest(const std::string & config);
    SerialTest() = default;
    ~SerialTest() override = default;
    void load(const std::string & config);
    std::vector<double> runTest(const std::vector<bool> &sequence) const override;
};

#endif //NISTSTIFACE_SERIAL_H
