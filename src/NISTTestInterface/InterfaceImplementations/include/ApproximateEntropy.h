//
// Created by svuatoslav on 12/15/18.
//

#ifndef NISTSTIFACE_APPROXIMATEENTROPY_H
#define NISTSTIFACE_APPROXIMATEENTROPY_H

#include "TestInterface.h"
#include <string>

class ApproximateEntropyTest : public TestInterface
{
    unsigned long _blockSize = 0;
    static const char* TestName;
public:
    ApproximateEntropyTest() = default;
    explicit ApproximateEntropyTest(const std::string & config);
    ~ApproximateEntropyTest() override = default;
    void load(const std::string & config);
    std::vector<double> runTest(const std::vector<bool> &sequence) const override;
};

#endif //NISTSTIFACE_APPROXIMATEENTROPY_H
