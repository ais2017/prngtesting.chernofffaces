//
// Created by svuatoslav on 12/15/18.
//

#ifndef NISTSTIFACE_RUNS_H
#define NISTSTIFACE_RUNS_H

#include "TestInterface.h"

class RunsTest : public TestInterface
{
public:
    RunsTest() = default;
    ~RunsTest() override = default;
    std::vector<double> runTest(const std::vector<bool> &sequence) const override;
};

#endif //NISTSTIFACE_RUNS_H
