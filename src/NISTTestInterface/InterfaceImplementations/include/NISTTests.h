//
// Created by svuatoslav on 12/16/18.
//

#ifndef NISTSTIFACE_NISTTESTS_H
#define NISTSTIFACE_NISTTESTS_H

#include "ApproximateEntropy.h"
#include "DiscreteFourierTransform.h"
#include "NonoverlappingTemplateMatching.h"
#include "RandomExcursionVariant.h"
#include "Serial.h"
#include "FrequencyBlock.h"
#include "LinearComplexity.h"
#include "OverlappingTemplateMatching.h"
#include "Rank.h"
#include "Universal.h"
#include "CumulativeSums.h"
#include "FrequencyMonobit.h"
#include "LongestRun.h"
#include "RandomExcursion.h"
#include "Runs.h"

#endif //NISTSTIFACE_NISTTESTS_H
