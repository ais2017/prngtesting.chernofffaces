//
// Created by svuatoslav on 12/15/18.
//

#ifndef NISTSTIFACE_DISCRETEFOURIERTRANSFORM_H
#define NISTSTIFACE_DISCRETEFOURIERTRANSFORM_H

#include "TestInterface.h"

class DiscreteFourierTransformTest : public TestInterface
{
public:
    DiscreteFourierTransformTest() = default;
    ~DiscreteFourierTransformTest() override = default;
    std::vector<double> runTest(const std::vector<bool> &sequence) const override;
};

#endif //NISTSTIFACE_DISCRETEFOURIERTRANSFORM_H
