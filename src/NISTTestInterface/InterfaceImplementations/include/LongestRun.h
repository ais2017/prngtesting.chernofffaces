//
// Created by svuatoslav on 12/15/18.
//

#ifndef NISTSTIFACE_LONGESTRUN_H
#define NISTSTIFACE_LONGESTRUN_H

#include "TestInterface.h"

class LongestRunTest : public TestInterface
{
public:
    LongestRunTest() = default;
    ~LongestRunTest() override = default;
    std::vector<double> runTest(const std::vector<bool> &sequence) const override;
};

#endif //NISTSTIFACE_LONGESTRUN_H
