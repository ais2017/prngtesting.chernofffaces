//
// Created by svuatoslav on 12/15/18.
//

#include "RandomExcursion.h"
#include "stat_fncs.h"

std::vector<double> RandomExcursionTest::runTest(const std::vector<bool> &sequence) const
{
    auto pvals = RandomExcursions(sequence);
    return std::vector<double>(pvals.cbegin(), pvals.cend());
}