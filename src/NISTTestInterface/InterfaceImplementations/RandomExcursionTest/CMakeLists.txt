cmake_minimum_required(VERSION 3.7)
project(niststiface)

set(CMAKE_CXX_STANDARD 17)

add_library(RandomExcursion ../include/RandomExcursion.h RandomExcursion.cpp)
target_link_libraries(RandomExcursion TestInterface niststpp)