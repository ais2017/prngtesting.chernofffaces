cmake_minimum_required(VERSION 3.7)
project(niststiface)

set(CMAKE_CXX_STANDARD 17)

add_library(LinearComplexity ../include/LinearComplexity.h LinearComplexity.cpp)
target_link_libraries(LinearComplexity TestInterface niststpp XMLParse)