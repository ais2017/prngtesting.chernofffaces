//
// Created by svuatoslav on 12/15/18.
//

#include "Serial.h"
#include "stat_fncs.h"
#include "XMLParse.h"

const char* SerialTest::TestName = "Serial";

std::vector<double> SerialTest::runTest(const std::vector<bool> &sequence) const
{
    if(_blockSize == 0)
    {
        throw std::logic_error( std::string(TestName) + " test is not loaded");
    }
    auto pvals = Serial(sequence, _blockSize);
    return {pvals.first, pvals.second};
}

void SerialTest::load(const std::string &config)
{
    XMLParse parser;
    parser.load(config, TestName);
    auto attr = parser.attribute("BlockSize");
    auto tmp = std::stoul(attr);
    if(tmp == 0)
    {
        throw std::logic_error("Block size must be positive");
    }
    _blockSize = tmp;
}

SerialTest::SerialTest(const std::string &config)
{
    load(config);
}
