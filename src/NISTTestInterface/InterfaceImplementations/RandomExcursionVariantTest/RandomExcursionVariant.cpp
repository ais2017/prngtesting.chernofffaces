//
// Created by svuatoslav on 12/15/18.
//

#include "RandomExcursionVariant.h"
#include "stat_fncs.h"

std::vector<double> RandomExcursionVariantTest::runTest(const std::vector<bool> &sequence) const
{
    auto pvals = RandomExcursionsVariant(sequence);
    return std::vector<double>(pvals.cbegin(), pvals.cend());
}