//
// Created by svuatoslav on 12/15/18.
//

#include "Rank.h"
#include "stat_fncs.h"

std::vector<double> RankTest::runTest(const std::vector<bool> &sequence) const
{
    return {Rank(sequence)};
}